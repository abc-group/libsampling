#include <stdio.h>
#include <check.h>
#include <math.h>

#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/nbenergy.h>
#include <mol2/prms.h>
#include <mol2/icharmm.h>

#include "../src/utils.h"
#include "../src/hybridization.h"
#include "../src/hbond.h"


struct mol_atom_group *test_ag, *ag_single, *ag_beta_sheet;
struct agsetup test_ags, ags_single, ags_beta_sheet;
struct mol_prms *params;

static const double weight = 0.441; // Default weight from MCM
static size_t atom_to_split_at; // Atom to split when testing hbondeng_split

/**
 * This function calculates numerical gradient of \p energy_func potential for single coordinate
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param en0 Energy in unperturbed state
 * \return Gradient
 */
double calc_gradient(
	size_t atom_num,
	int crd_id,
	double d,
	double en0,
	double (*energy_func)(void))
{
	double *crd = NULL;
	double en = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	en = energy_func();
	*crd = tmp;
	return (en0 - en) / d;
}

void test_hbond_grads(double d, double (*energy_func)(void))
{
	static const double tolerance = 1e-3;
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	double en = energy_func();

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = calc_gradient(i, 0, d, en, energy_func);
		fs[i].Y = calc_gradient(i, 1, d, en, energy_func);
		fs[i].Z = calc_gradient(i, 2, d, en, energy_func);
	}

	// Since energy function overwrites gradients in atomgroup each time, we must recalculate them.
	mol_zero_gradients(test_ag);
	energy_func();
	char msg[256];

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			test_ag->gradients[i].X, test_ag->gradients[i].Y, test_ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		struct mol_vector3 diff;
		MOL_VEC_SUB(diff, test_ag->gradients[i], fs[i]);
		ck_assert_msg(fabs(diff.X) < tolerance, msg);
		ck_assert_msg(fabs(diff.Y) < tolerance, msg);
		ck_assert_msg(fabs(diff.Z) < tolerance, msg);

	}
	free(fs);
}


// Below are wrapper functions around various hbond potentials. They are used to make checking gradients easy and copypaste-free.
static double _hbondeng()
{
	double energy = 0.0;
	smp_energy_hbond(test_ag, &energy, test_ags.nblst, 1.0, false, 0);
	return energy;
}

static double _hbondeng_weighted()
{
	double energy = 0.0;
	smp_energy_hbond(test_ag, &energy, test_ags.nblst, weight, false, 0);
	return energy;
}

static double _hbondeng_split()
{
	double energy = 0.0;
	smp_energy_hbond(test_ag, &energy, test_ags.nblst, weight, true, atom_to_split_at);
	return energy;
}

// Test cases
START_TEST(test_smp_hbond_init_bases)
{
	struct mol_atom_group *ag = mol_read_pdb("hsd_cut.pdb");
	mol_atom_group_read_geometry(ag, "hsd_cut.psf", "protein.prm", "protein.rtf");
	mol_atom_group_add_prms(ag, params);
	smp_hybridization_read(ag, "hsd_cut.mol2");
	bool ret = smp_hbond_init_bases(ag, NULL);
	ck_assert(ret);

	static const size_t have_base[] = {1, 5, 7, 11};
	static const size_t base_ref[] = {0, 4, 6, 10};
	static const size_t base2_ref[] = {2, 255, 8, 255};
	const size_t *base2 = mol_atom_group_fetch_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY);
	size_t cur = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (ag->hbond_prop[i] == HBOND_ACCEPTOR || ag->hbond_prop[i] == DONATABLE_HYDROGEN) {
			ck_assert_int_eq(i, have_base[cur]);
			ck_assert_int_eq(ag->hbond_base_atom_id[i], base_ref[cur]);
			if (base2_ref[cur] != 255)
				ck_assert_int_eq(base2[i], base2_ref[cur]);
			cur++;
		}
	}
}
END_TEST

START_TEST(test_smp_hbond_init_bases_fail)
{
	struct mol_atom_group *ag = mol_read_pdb("hsd_cut.pdb");
	mol_atom_group_read_geometry(ag, "hsd_cut.psf", "protein.prm", "protein.rtf");
	mol_atom_group_add_prms(ag, params);
	smp_hybridization_read(ag, "hsd_cut.mol2");
	// Break hydrogen base
	ag->hbond_base_atom_id[5] = 3; // was 4
	FILE *log = tmpfile();

	bool ret = smp_hbond_init_bases(ag, log);
	ck_assert(!ret);

	rewind(log);
	char buf[1025] = {0}; // fread does not write terminating \0, so we make sure it will already be there
	fread(buf, sizeof(char), 1024, log);
	fclose(log);

	/* The error string contains filename and line numbers. I don't think it's a good idea to hardcode them,
	 * so I check that both error messages are present, and that the buffer is not too long -- i.e. it does not
	 * contain any other messages. If C had built-in regex library, it could've been done in a more straightforward
	 * way, but oh, well. */
	ck_assert(strstr(buf, "Wrong base of hydrogen 5\n") != NULL);
	ck_assert(strstr(buf, "Base of hydrogen 5 - atom 3 - is not marked as donor\n") != NULL);
	ck_assert_int_lt(strlen(buf), 200);
}
END_TEST


START_TEST(test_smp_energy_hbond_beta_sheet)
{
	test_ag = ag_beta_sheet; test_ags = ags_beta_sheet;
	static const double energy_ref = -15.26045310801;
	double energy = _hbondeng();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng);
}
END_TEST


START_TEST(test_smp_energy_hbond_single)
{
	test_ag = ag_single; test_ags = ags_single;
	static const double energy_ref = -0.85496808984;
	double energy = _hbondeng();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng);
}
END_TEST


START_TEST(test_smp_energy_hbond_split_beta_sheet)
{
	test_ag = ag_beta_sheet; test_ags = ags_beta_sheet;
	static const double energy_ref = -2.66888747855;
	atom_to_split_at = 100; // Chosen arbitrary
	double energy = _hbondeng_split();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng_split);
}
END_TEST


START_TEST(test_smp_energy_hbond_split_single)
{
	test_ag = ag_single; test_ags = ags_single;
	const double energy_ref = -0.85496808984 * weight;
	atom_to_split_at = 6; // Chosen such that hbonds are not affected
	double energy = _hbondeng_split();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng_split);
}
END_TEST


START_TEST(test_smp_energy_hbond_split_single_none)
{
	test_ag = ag_single; test_ags = ags_single;
	static const double energy_ref = 0;
	atom_to_split_at = 0; // Chosen to cancel all hbonds
	double energy = _hbondeng_split();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng_split);
}
END_TEST


START_TEST(test_smp_energy_hbond_weighted_beta_sheet)
{
	test_ag = ag_beta_sheet; test_ags = ags_beta_sheet;
	static const double energy_ref = -6.72985982063;
	double energy = _hbondeng_weighted();
	ck_assert(fabs(energy - energy_ref) < 1e-9);
	test_hbond_grads(1e-6, _hbondeng_weighted);
}
END_TEST

void setup(void)
{
	params = mol_prms_read("libmol.params");

	ag_beta_sheet = mol_read_pdb("1bgs-beta.pdb");
	mol_atom_group_read_geometry(ag_beta_sheet, "1bgs-beta.psf", "protein.prm", "protein.rtf");
	mol_atom_group_add_prms(ag_beta_sheet, params);
	smp_hybridization_read(ag_beta_sheet, "1bgs-beta.mol2");
	ag_beta_sheet->gradients = calloc(ag_beta_sheet->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(ag_beta_sheet);
	mol_fixed_update(ag_beta_sheet, 0, NULL); // Make nothing fixed
	smp_hbond_init_bases(ag_beta_sheet, NULL);
	smp_atom_group_init_backbone_by_name(ag_beta_sheet);
	init_nblst(ag_beta_sheet, &ags_beta_sheet);
	update_nblst(ag_beta_sheet, &ags_beta_sheet);

	ag_single = mol_read_pdb("hsd_cut.pdb");
	mol_atom_group_read_geometry(ag_single, "hsd_cut.psf", "protein.prm", "protein.rtf");
	mol_atom_group_add_prms(ag_single, params);
	smp_hybridization_read(ag_single, "hsd_cut.mol2");
	ag_single->gradients = calloc(ag_single->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(ag_single);
	mol_fixed_update(ag_single, 0, NULL); // Make nothing fixed
	smp_hbond_init_bases(ag_single, NULL);
	smp_atom_group_init_backbone_by_name(ag_single);
	init_nblst(ag_single, &ags_single);
	update_nblst(ag_single, &ags_single);
}

void teardown(void)
{
	mol_atom_group_free(ag_beta_sheet);
	mol_atom_group_free(ag_single);
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("energy_hbond");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test_hbond");
	tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_set_timeout(tcase, 300);
	tcase_add_test(tcase, test_smp_hbond_init_bases);
	tcase_add_test(tcase, test_smp_hbond_init_bases_fail);
	tcase_add_test(tcase, test_smp_energy_hbond_single);
	tcase_add_test(tcase, test_smp_energy_hbond_beta_sheet);
	tcase_add_test(tcase, test_smp_energy_hbond_split_beta_sheet);
	tcase_add_test(tcase, test_smp_energy_hbond_split_single);
	tcase_add_test(tcase, test_smp_energy_hbond_split_single_none);
	tcase_add_test(tcase, test_smp_energy_hbond_weighted_beta_sheet);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
