#include <check.h>
#include <math.h>
#include <stdlib.h>
#include <transform_exp.h>
#include "utils.h"
#include <mol2/transform.h>

static const double tolerance_strict = 1e-9;

#define ck_assert_double_eq_tol(val, ref, tol) \
	do { \
		char msg[1024]; \
		snprintf(msg, 1023, "Error: %.15lf != %.15lf (tol = %e)", val, ref, tol); \
		ck_assert_msg(fabs(((val)) - ((ref))) < tol, msg); \
	} while(0)

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict)


static void test_eq_matrices(const struct mol_matrix3 m1, const struct mol_matrix3 m2, const double tolerance)
{
	ck_assert_double_eq_tol(m1.m11, m2.m11, tolerance);
	ck_assert_double_eq_tol(m1.m12, m2.m12, tolerance);
	ck_assert_double_eq_tol(m1.m13, m2.m13, tolerance);
	ck_assert_double_eq_tol(m1.m21, m2.m21, tolerance);
	ck_assert_double_eq_tol(m1.m22, m2.m22, tolerance);
	ck_assert_double_eq_tol(m1.m23, m2.m23, tolerance);
	ck_assert_double_eq_tol(m1.m31, m2.m31, tolerance);
	ck_assert_double_eq_tol(m1.m32, m2.m32, tolerance);
	ck_assert_double_eq_tol(m1.m33, m2.m33, tolerance);
}

static void test_eq_matrices3(const struct mol_matrix3* mm1, const struct mol_matrix3* mm2, const double tolerance)
{
	for (int i = 0; i < 3; ++i) {
		test_eq_matrices(mm1[i], mm2[i], tolerance);
	}
}

#define ck_assert_vector3_eq(val, refx, refy, refz) do \
	{                                              \
		ck_assert_double_eq((val).X, (refx));  \
		ck_assert_double_eq((val).Y, (refy));  \
		ck_assert_double_eq((val).Z, (refz));  \
	} while(0)

#define ck_assert_quaternion_eq(val, refw, refx, refy, refz) do \
	{                                                       \
		ck_assert_double_eq((val).W, (refw));           \
		ck_assert_double_eq((val).X, (refx));           \
		ck_assert_double_eq((val).Y, (refy));           \
		ck_assert_double_eq((val).Z, (refz));           \
	} while(0)


/**
 * Compare the numeric and analytical derivatives w.r.t. exponential map.
 */
static void _numerically_derive_partial_r_exp(const struct mol_vector3 v, const double delta, const double tolerance)
{
	struct mol_matrix3 m0, mx, my, mz;
	smp_rmatr_from_exp(&m0, v);
	smp_matrix_mult_scalar(&m0, -1);

	struct mol_vector3 vdx = v;
	vdx.X += delta;
	smp_rmatr_from_exp(&mx, vdx);
	smp_matrix_sum(&mx, mx, m0);
	smp_matrix_mult_scalar(&mx, 1/delta);

	struct mol_vector3 vdy = v;
	vdy.Y += delta;
	smp_rmatr_from_exp(&my, vdy);
	smp_matrix_sum(&my, my, m0);
	smp_matrix_mult_scalar(&my, 1/delta);

	struct mol_vector3 vdz = v;
	vdz.Z += delta;
	smp_rmatr_from_exp(&mz, vdz);
	smp_matrix_sum(&mz, mz, m0);
	smp_matrix_mult_scalar(&mz, 1/delta);

	struct mol_matrix3 d_analytic[3];
	smp_rmatr_derive_wrt_exp(d_analytic, v);

	test_eq_matrices(d_analytic[0], mx, tolerance);
	test_eq_matrices(d_analytic[1], my, tolerance);
	test_eq_matrices(d_analytic[2], mz, tolerance);
}

START_TEST(test_rmatr_derive_wrt_exp)
{
	static const double tolerance = 1e-8;
	static const double delta_num = 1e-7;
	static const double tolerance_num = 1e-5;

	struct mol_vector3 v;

	struct mol_matrix3 result[3];
	struct mol_matrix3 ref[3];

	// Test 1: zero vector. The only non-zero term is (dS(v)/dv * sin(theta) / theta) = (dS(v)/dv)
	v = (struct mol_vector3) { .X = 0, .Y = 0, .Z = 0 };

	ref[0].m11 = 0, ref[0].m12 = 0, ref[0].m13 = 0;
	ref[0].m21 = 0, ref[0].m22 = 0, ref[0].m23 = -1;
	ref[0].m31 = 0, ref[0].m32 = 1, ref[0].m33 = 0;

	ref[1].m11 = 0, ref[1].m12 = 0, ref[1].m13 = 1;
	ref[1].m21 = 0, ref[1].m22 = 0, ref[1].m23 = 0;
	ref[1].m31 = -1, ref[1].m32 = 0, ref[1].m33 = 0;

	ref[2].m11 = 0, ref[2].m12 = -1, ref[2].m13 = 0;
	ref[2].m21 = 1, ref[2].m22 = 0, ref[2].m23 = 0;
	ref[2].m31 = 0, ref[2].m32 = 0, ref[2].m33 = 0;

	smp_rmatr_derive_wrt_exp(result, v);
	test_eq_matrices3(ref, result, tolerance);
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);

	// Test 2: Very small rotation around (1,1,1)
	v = (struct mol_vector3) { .X = 1e-8, .Y = 1e-8, .Z = 1e-8 };

	ref[0].m11 = 0.000000000000, ref[0].m12 = 0.000000005000, ref[0].m13 = 0.000000005000;
	ref[0].m21 = 0.000000005000, ref[0].m22 = -0.000000010000, ref[0].m23 = -1.000000000000;
	ref[0].m31 = 0.000000005000, ref[0].m32 = 1.000000000000, ref[0].m33 = -0.000000010000;

	ref[1].m11 = -0.000000010000, ref[1].m12 = 0.000000005000, ref[1].m13 = 1.000000000000;
	ref[1].m21 = 0.000000005000, ref[1].m22 = 0.000000000000, ref[1].m23 = 0.000000005000;
	ref[1].m31 = -1.000000000000, ref[1].m32 = 0.000000005000, ref[1].m33 = -0.000000010000;

	ref[2].m11 = -0.000000010000, ref[2].m12 = -1.000000000000, ref[2].m13 = 0.000000005000;
	ref[2].m21 = 1.000000000000, ref[2].m22 = -0.000000010000, ref[2].m23 = 0.000000005000;
	ref[2].m31 = 0.000000005000, ref[2].m32 = 0.000000005000, ref[2].m33 = 0.000000000000;

	smp_rmatr_derive_wrt_exp(result, v);
	test_eq_matrices3(ref, result, tolerance);
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);

	// Test 3: Big rotation around (1,1,1). Reference value taken from old libmol.
	v = (struct mol_vector3) { .X = 1, .Y = 1, .Z = 1 };

	ref[0].m11 = 0.135896173245, ref[0].m12 = 0.562376305488, ref[0].m13 = 0.075431880317;
	ref[0].m21 = 0.075431880317, ref[0].m22 = -0.637808185805, ref[0].m23 = -0.394335973219;
	ref[0].m31 = 0.562376305488, ref[0].m32 = 0.258439799974, ref[0].m33 = -0.637808185805;

	ref[1].m11 = -0.637808185805, ref[1].m12 = 0.562376305488, ref[1].m13 = 0.258439799974;
	ref[1].m21 = 0.075431880317, ref[1].m22 = 0.135896173245, ref[1].m23 = 0.562376305488;
	ref[1].m31 = -0.394335973219, ref[1].m32 = 0.075431880317, ref[1].m33 = -0.637808185805;

	ref[2].m11 = -0.637808185805, ref[2].m12 = -0.394335973219, ref[2].m13 = 0.075431880317;
	ref[2].m21 = 0.258439799974, ref[2].m22 = -0.637808185805, ref[2].m23 = 0.562376305488;
	ref[2].m31 =  0.562376305488, ref[2].m32 = 0.075431880317, ref[2].m33 = 0.135896173245;

	smp_rmatr_derive_wrt_exp(result, v);
	test_eq_matrices3(ref, result, tolerance);
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);

	// Test 3: pi/6 rotation around (1,1,-1). Reference value taken from old libmol.
	v.X = (M_PI/6.0) * (1/sqrt(3));
	v.Y = (M_PI/6.0) * (1/sqrt(3));
	v.Z = -(M_PI/6.0) * (1/sqrt(3));

	ref[0].m11 = 0.004520753326, ref[0].m12 = 0.115833004040, ref[0].m13 = -0.175102507218;
	ref[0].m21 = 0.175102507218, ref[0].m22 = -0.290935511258, ref[0].m23 = -0.923034530299;
	ref[0].m31 = -0.115833004040, ref[0].m32 = 0.927555283626, ref[0].m33 = -0.290935511258;

	ref[1].m11 = -0.290935511258, ref[1].m12 = 0.115833004040, ref[1].m13 = 0.927555283626;
	ref[1].m21 = 0.175102507218, ref[1].m22 = 0.004520753326, ref[1].m23 = -0.115833004040;
	ref[1].m31 = -0.923034530299, ref[1].m32 =-0.175102507218, ref[1].m33 = -0.290935511258;

	ref[2].m11 = 0.290935511258, ref[2].m12 = -0.923034530299, ref[2].m13 = 0.175102507218;
	ref[2].m21 = 0.927555283626, ref[2].m22 = 0.290935511258, ref[2].m23 = 0.115833004040;
	ref[2].m31 = 0.115833004040, ref[2].m32 = 0.175102507218, ref[2].m33 = -0.004520753326;

	smp_rmatr_derive_wrt_exp(result, v);
	test_eq_matrices3(ref, result, tolerance);
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);

	// Test with small rotation around arbitrary axis. No reference value, just numerical vs. analytical check.
	v = (struct mol_vector3) { .X = 0.1, .Y = 0.2, .Z = 0.3 };
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);

	// Test with big rotation around arbitrary axis. No reference value, just numerical vs. analytical check.
	// Here, the behavior is different from old libmol, as we made the numerical and analytical derivatives
	//   agree for such case, instead of squeezing the angle value to [0;2pi] range.
	v = (struct mol_vector3) { .X = 3, .Y = 4, .Z = 5 };
	_numerically_derive_partial_r_exp(v, delta_num, tolerance_num);
}
END_TEST


static void _test_rmatr_from_exp_int(
		const struct mol_vector3 v,
		const struct mol_matrix3 m_ref,
		const double tolerance)
{
	// Compute the matrix of the exp. map v and compare it to the reference
	struct mol_matrix3 m;
	smp_rmatr_from_exp(&m, v);
	test_eq_matrices(m_ref, m, tolerance);

	// Create new exp. map, equal to the original but with angle += 2 Pi. The matrix should be the same.
	const double theta = sqrt(MOL_VEC_SQ_NORM(v));
	struct mol_vector3 v_2pi;
	if (theta > 1e-9) {
		MOL_VEC_MULT_SCALAR(v_2pi, v, (theta + 2 * M_PI) / theta);
	} else {
		v_2pi = (struct mol_vector3) {2*M_PI, 0, 0};
	}
	smp_rmatr_from_exp(&m, v_2pi);
	test_eq_matrices(m_ref, m, tolerance);
}


START_TEST(test_rmatr_from_exp)
{
	static const double tolerance = 1e-6;

	struct mol_vector3 v;
	struct mol_matrix3 ref;

	// Test 1: Zero rotation results in identity matrix
	v = (struct mol_vector3) { .X = 0, .Y = 0, .Z = 0 };

	ref.m11 = 1; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 1; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = 1;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 2: 1.5pi rotation around OX
	v = (struct mol_vector3) { .X = 1.5*M_PI, .Y = 0, .Z = 0 };

	ref.m11 = 1; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 0; ref.m23 = 1;
	ref.m31 = 0; ref.m32 = -1; ref.m33 = 0;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 3: -pi/2 rotation around OZ
	v = (struct mol_vector3) { .X = 0, .Y = 0, .Z = -0.5*M_PI };

	ref.m11 = 0; ref.m12 = 1; ref.m13 = 0;
	ref.m21 = -1; ref.m22 = 0; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = 1;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 4: 2.5pi rotation around OY
	v = (struct mol_vector3) { .X = 0, .Y = 2.5*M_PI, .Z = 0 };

	ref.m11 = 0; ref.m12 = 0; ref.m13 = 1;
	ref.m21 = 0; ref.m22 = 1; ref.m23 = 0;
	ref.m31 = -1; ref.m32 = 0; ref.m33 = 0;

	_test_rmatr_from_exp_int(v, ref, tolerance);


	// Test 5: pi/6 rotation around (1,1,1)
	v.X = v.Y = v.Z = (M_PI/6.0) * (1/sqrt(3));

	ref.m11 = 0.910684; ref.m12 = -0.244017; ref.m13 = 0.333333;
	ref.m21 = 0.333333; ref.m22 = 0.910684; ref.m23 = -0.244017;
	ref.m31 = -0.244017; ref.m32 = 0.333333; ref.m33 = 0.910684;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 6: -pi/6 rotation around (1,1,1)
	v.X = v.Y = v.Z = -(M_PI/6.0) * (1/sqrt(3));

	ref.m11 = 0.910684; ref.m12 = 0.333333; ref.m13 = -0.244017;
	ref.m21 = -0.244017; ref.m22 = 0.910684; ref.m23 = 0.333333;
	ref.m31 = 0.333333; ref.m32 = -0.244017; ref.m33 = 0.910684;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 7: pi/6 rotation around (1,1,-1)
	v.X = (M_PI/6.0) * (1/sqrt(3));
	v.Y = (M_PI/6.0) * (1/sqrt(3));
	v.Z = -(M_PI/6.0) * (1/sqrt(3));

	ref.m11 = 0.910684; ref.m12 = 0.333333; ref.m13 = 0.244017;
	ref.m21 = -0.244017; ref.m22 = 0.910684; ref.m23 = -0.333333;
	ref.m31 = -0.333333; ref.m32 = 0.244017; ref.m33 = 0.910684;

	_test_rmatr_from_exp_int(v, ref, tolerance);

	// Test 9: 20pi rotation around (1,1,1)
	v.X = v.Y = v.Z = (20*M_PI) * (1/sqrt(3));

	ref.m11 = 1; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 1; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = 1;

	_test_rmatr_from_exp_int(v, ref, tolerance);
}
END_TEST


/**
 * Compare the numeric and analytical derivatives w.r.t. rotation angle.
 */
static void _numerically_derive_partial_theta(const struct mol_vector3 axis, const double theta, const double delta, const double tolerance)
{
	const double l = sqrt(MOL_VEC_SQ_NORM(axis));

	struct mol_matrix3 m0;
	struct mol_vector3 v0;
	MOL_VEC_MULT_SCALAR(v0, axis, theta / l);

	struct mol_matrix3 m1;
	struct mol_vector3 v1;
	MOL_VEC_MULT_SCALAR(v1, axis, (theta + delta) / l);

	smp_rmatr_from_exp(&m0, v0);
	smp_matrix_mult_scalar(&m0, -1);

	smp_rmatr_from_exp(&m1, v1);
	smp_matrix_sum(&m1, m1, m0);

	smp_matrix_mult_scalar(&m1, 1/delta);

	struct mol_matrix3 d_analytic;
	smp_rmatr_derive_wrt_theta(&d_analytic, axis, theta);

	test_eq_matrices(d_analytic, m1, tolerance);
}

START_TEST(test_rmatr_derive_wrt_theta)
{
	static const double tolerance = 1e-6;
	static const double delta_num = 1e-7;
	static const double tolerance_num = 1e-5;

	struct mol_vector3 v;
	struct mol_matrix3 m, ref;
	double theta;

	// Test 1: Zero axis should result in zero output matrix
	theta = M_PI/2.0;
	v = (struct mol_vector3) { .X = 0, .Y = 0, .Z = 0 };

	ref.m11 = 0; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 0; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = 0;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	// Not comparing with numerical derivative for zero axis.

	// Test 2: pi/2 rotation around OX
	theta = M_PI/2.0;
	v = (struct mol_vector3) { .X = 1, .Y = 0, .Z = 0 };

	ref.m11 = 0; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = -1.; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = -1;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 3: -pi/2 rotation around OZ. Checking that axis normalization do not matter.
	theta = M_PI/2.0;
	v = (struct mol_vector3) { .X = 0, .Y = 0, .Z = -9 };

	ref.m11 = -1; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = -1; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = 0;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 4: 2.5pi rotation around OY.
	theta = 2.5 * M_PI;
	v = (struct mol_vector3) { .X = 0, .Y = 1, .Z = 0 };

	ref.m11 = -1; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 0; ref.m23 = 0;
	ref.m31 = 0; ref.m32 = 0; ref.m33 = -1;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 5: Rotation around (1,1,1) axis
	theta = M_PI/2.0;
	v = (struct mol_vector3) { .X = 1, .Y = 1, .Z = 1 };

	ref.m11 = -2./3; ref.m12 = 1./3; ref.m13 = 1./3;
	ref.m21 = 1./3; ref.m22 = -2./3; ref.m23 = 1./3;
	ref.m31 = 1./3; ref.m32 = 1./3; ref.m33 = -2./3;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 6: Rotation around (1, 1, -1) axis
	theta = M_PI/2.0;
	v = (struct mol_vector3) { .X = 1, .Y = 1, .Z = -1 };

	ref.m11 = -2./3; ref.m12 = 1./3; ref.m13 = -1./3;
	ref.m21 = 1./3; ref.m22 = -2./3; ref.m23 = -1./3;
	ref.m31 = -1./3; ref.m32 = -1./3; ref.m33 = -2./3;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 7: Rotation around OX axis
	theta = 0;
	v = (struct mol_vector3) { .X = 1, .Y = 0, .Z = 0 };

	ref.m11 = 0; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = 0; ref.m23 = -1;
	ref.m31 = 0; ref.m32 = 1; ref.m33 = 0;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);

	// Test 8: Rotation around OX axis
	theta = M_PI/6;
	v = (struct mol_vector3) { .X = 1, .Y = 0, .Z = 0 };

	ref.m11 = 0; ref.m12 = 0; ref.m13 = 0;
	ref.m21 = 0; ref.m22 = -1./2; ref.m23 = -sqrt(3)/2;
	ref.m31 = 0; ref.m32 = sqrt(3)/2; ref.m33 = -1./2;

	smp_rmatr_derive_wrt_theta(&m, v, theta);
	test_eq_matrices(ref, m, tolerance);
	_numerically_derive_partial_theta(v, theta, delta_num, tolerance_num);
}
END_TEST


START_TEST(test_quaternion_to_exp)
{
	struct mol_vector3 ret;

	const struct mol_quaternion q0 = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	ret = smp_quaternion_to_exp(q0);
	ck_assert_vector3_eq(ret, 0.0, 0.0, 0.0);

	const struct mol_quaternion q60 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};
	ret = smp_quaternion_to_exp(q60);
	ck_assert_vector3_eq(ret, 1 * M_PI / 3 / sqrt(14), 2 * M_PI / 3 / sqrt(14), 3 * M_PI / 3 / sqrt(14));

	const struct mol_quaternion q60i = {.W = -sqrt(3) / 2, .X = -0.5 / sqrt(14), .Y = -1 / sqrt(14), .Z = -1.5 / sqrt(14)};
	ret = smp_quaternion_to_exp(q60i);
	ck_assert_vector3_eq(ret, 1 * M_PI / 3 / sqrt(14), 2 * M_PI / 3 / sqrt(14), 3 * M_PI / 3 / sqrt(14));
}
END_TEST


START_TEST(test_quaternion_from_exp)
{
	const struct mol_vector3 v0 = {.X = 0, .Y = 0, .Z = 0};
	const struct mol_vector3 v60 = {
		.X = 1 * M_PI / 3 / sqrt(14),
		.Y = 2 * M_PI / 3 / sqrt(14),
		.Z = 3 * M_PI / 3 / sqrt(14)};
	const struct mol_vector3 v420 = {
		.X = 7 * M_PI / 3 / sqrt(14),
		.Y = 14 * M_PI / 3 / sqrt(14),
		.Z = 21 * M_PI / 3 / sqrt(14)};
	const struct mol_vector3 v60m = {
		.X = -1 * M_PI / 3 / sqrt(14),
		.Y = -2 * M_PI / 3 / sqrt(14),
		.Z = -3 * M_PI / 3 / sqrt(14)};
	struct mol_quaternion q;

	q = smp_quaternion_from_exp(v0);
	ck_assert_quaternion_eq(q, 1.0, 0.0, 0.0, 0.0);

	q = smp_quaternion_from_exp(v60);
	ck_assert_quaternion_eq(q, sqrt(3) / 2, 0.5 / sqrt(14), 1 / sqrt(14), 1.5 / sqrt(14));

	q = smp_quaternion_from_exp(v420);
	if (q.W < 0)
		q = mol_quaternion_scale(q, -1);
	ck_assert_quaternion_eq(q, sqrt(3) / 2, 0.5 / sqrt(14), 1 / sqrt(14), 1.5 / sqrt(14));

	q = smp_quaternion_from_exp(v60m);
	ck_assert_quaternion_eq(q, sqrt(3) / 2, -0.5 / sqrt(14), -1 / sqrt(14), -1.5 / sqrt(14));
}
END_TEST

Suite *rodrigues_suite(void)
{
	Suite *suite = suite_create("transform_exp");
	TCase *tcase = tcase_create("test_transform_exp");

	tcase_add_test(tcase, test_rmatr_derive_wrt_exp);
	tcase_add_test(tcase, test_rmatr_from_exp);
	tcase_add_test(tcase, test_rmatr_derive_wrt_theta);
	tcase_add_test(tcase, test_quaternion_to_exp);
	tcase_add_test(tcase, test_quaternion_from_exp);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = rodrigues_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

