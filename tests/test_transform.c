#include <check.h>
#include <math.h>

#include <mol2/vector.h>
#include <mol2/matrix.h>
#include <mol2/quaternion.h>
#include "src/transform.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < tol)

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict)

#define ck_assert_vector3_eq(val, refx, refy, refz) do \
	{                                              \
		ck_assert_double_eq((val).X, (refx));  \
		ck_assert_double_eq((val).Y, (refy));  \
		ck_assert_double_eq((val).Z, (refz));  \
	} while(0)



// Test cases
START_TEST(test_smp_vector3_rotate_by_quaternion)
{
	const struct mol_quaternion q_unit = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const double cos_a = cos(M_PI / 4), sin_a = sin(M_PI / 4); // Rotation by pi/2
	const struct mol_quaternion q_x = {.W = cos_a, .X = sin_a, .Y = 0, .Z = 0};
	const struct mol_quaternion q_x_2pi = {.W = -cos_a, .X = -sin_a, .Y = 0, .Z = 0};
	const struct mol_quaternion q_y = {.W = cos_a, .X = 0, .Y = sin_a, .Z = 0};
	const struct mol_quaternion q_z = {.W = cos_a, .X = 0, .Y = 0, .Z = sin_a};
	const struct mol_vector3 v0 = {.X = 1, .Y = 2, .Z = 3};
	struct mol_vector3 v;

	v = v0;
	smp_vector3_rotate_by_quaternion(&v, &q_unit);
	ck_assert_vector3_eq(v, v0.X, v0.Y, v0.Z);

	v = v0;
	smp_vector3_rotate_by_quaternion(&v, &q_x);
	ck_assert_vector3_eq(v, v0.X, -v0.Z, v0.Y);

	v = v0;
	smp_vector3_rotate_by_quaternion(&v, &q_y);
	ck_assert_vector3_eq(v, v0.Z, v0.Y, -v0.X);

	v = v0;
	smp_vector3_rotate_by_quaternion(&v, &q_z);
	ck_assert_vector3_eq(v, -v0.Y, v0.X, v0.Z);

	// Multiplying all elements of quaternion by (-1) should not change anything.
	v = v0;
	smp_vector3_rotate_by_quaternion(&v, &q_x_2pi);
	ck_assert_vector3_eq(v, v0.X, -v0.Z, v0.Y);

	// Trying to rotate zero vector
	v.X = v.Y = v.Z = 0;
	smp_vector3_rotate_by_quaternion(&v, &q_x);
	ck_assert_vector3_eq(v, 0, 0, 0);
}
END_TEST


START_TEST(test_smp_vector3_normalize)
{
	struct mol_vector3 v;
	double l;

	v.X = v.Y = v.Z = 0;
	l = smp_vector3_normalize(&v);
	ck_assert_double_eq(l, 0);
	ck_assert_vector3_eq(v, 0, 0, 0);

	v.X = v.Y = v.Z = 1;
	l = smp_vector3_normalize(&v);
	ck_assert_double_eq(l, sqrt(3));
	ck_assert_vector3_eq(v, 1 / sqrt(3), 1 / sqrt(3), 1 / sqrt(3));

	v.X = 1;
	v.Y = 2;
	v.Z = -3;
	l = smp_vector3_normalize(&v);
	ck_assert_double_eq(l, sqrt(14));
	ck_assert_vector3_eq(v, 1 / sqrt(14), 2 / sqrt(14), -3 / sqrt(14));
}
END_TEST


START_TEST(test_smp_matrix3_mul)
{
	// Nothing fancy, just two random matrices getting multiplied
	const struct mol_matrix3 m1 = {
		.m11 = 0.825406, .m12 = 0.535188, .m13 = 0.058269,
		.m21 = 0.100526, .m22 = 0.196159, .m23 = 0.584842,
		.m31 = 0.926538, .m32 = 0.860059, .m33 = 0.224466};
	const struct mol_matrix3 m2 = {
		.m11 = 0.836150, .m12 = 0.061400, .m13 = 0.142246,
		.m21 = 0.221859, .m22 = 0.795132, .m23 = 0.274921,
		.m31 = 0.606147, .m32 = 0.975627, .m33 = 0.228991};
	const struct mol_matrix3 mul = {
		.m11 = 0.844219080935, .m12 = 0.533073842879, .m13 = 0.277888198603,
		.m21 = 0.482074678255, .m22 = 0.732732240322, .m23 = 0.202151204257,
		.m31 = 1.101595970883, .m32 = 0.959744956170, .m33 = 0.419645298493};

	struct mol_matrix3 m = smp_matrix3_mul(&m1, &m2);
	ck_assert_double_eq(m.m11, mul.m11);
	ck_assert_double_eq(m.m12, mul.m12);
	ck_assert_double_eq(m.m13, mul.m13);
	ck_assert_double_eq(m.m21, mul.m21);
	ck_assert_double_eq(m.m22, mul.m22);
	ck_assert_double_eq(m.m23, mul.m23);
	ck_assert_double_eq(m.m31, mul.m31);
	ck_assert_double_eq(m.m32, mul.m32);
	ck_assert_double_eq(m.m33, mul.m33);
}
END_TEST

START_TEST(test_smp_matrix3_eye)
{
	struct mol_matrix3 m = {0};
	smp_matrix3_eye(&m);
	ck_assert_double_eq(m.m11, 1);
	ck_assert_double_eq(m.m12, 0);
	ck_assert_double_eq(m.m13, 0);
	ck_assert_double_eq(m.m21, 0);
	ck_assert_double_eq(m.m22, 1);
	ck_assert_double_eq(m.m23, 0);
	ck_assert_double_eq(m.m31, 0);
	ck_assert_double_eq(m.m32, 0);
	ck_assert_double_eq(m.m33, 1);
}
END_TEST

Suite *featurex_suite(void)
{
	Suite *suite = suite_create("transform");
	TCase *tcase = tcase_create("test_transform");
	tcase_add_test(tcase, test_smp_vector3_rotate_by_quaternion);
	tcase_add_test(tcase, test_smp_vector3_normalize);
	tcase_add_test(tcase, test_smp_matrix3_mul);
	tcase_add_test(tcase, test_smp_matrix3_eye);
	suite_add_tcase(suite, tcase);
	return suite;
}

int main(void)
{
	Suite *suite = featurex_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

