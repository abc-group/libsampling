#include <stdio.h>
#include <math.h>
#include <check.h>

#include <src/rigforest.h>
#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/icharmm.h>
#include <mol2/benergy.h>
#include <mol2/nbenergy.h>
#include <mol2/json.h>
#include <src/utils.h>
#include <src/transform.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

const double tolerance_pdb = 1e-3;
const double tolerance = 1e-5;
const double tolerance_strict = 1e-9;

#define MAX_ATOMS 50 // Magic constant for second dimension of various fixture arrays

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < tol);

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);

#define ck_assert_coordinate_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_pdb);


struct reference_rigtree {
	size_t num_vertices;
	size_t vertex_sizes[MAX_ATOMS];
	size_t vertex_atoms[MAX_ATOMS][MAX_ATOMS];
	size_t vertex_degree_out[MAX_ATOMS];
	size_t vertex_adjacent[MAX_ATOMS][MAX_ATOMS];
	size_t vertex_parents[MAX_ATOMS];
	size_t edges_atom[MAX_ATOMS][2];
	size_t total_natoms;
	double geometric_center[3];
};

// These tests use structures from nemo-with-9546-cl1/nemo_r_l_u_min.pdb
struct reference_rigtree reference_rigtree_nemo = {
	.num_vertices = 20,
	.vertex_sizes = {4, 1, 4, 1, 1, 10, 4, 1, 7, 1, 3, 1, 4, 1, 8, 1, 4, 7, 1, 3},
	.vertex_atoms =
		{{1344, 1345, 1346, 1347}, {1348}, {1353, 1354, 1355, 1356}, {1357}, {1358},
		 {1359, 1363, 1364, 1365, 1361, 1360, 1366, 1362, 1367, 1368}, {1369, 1370, 1371, 1380}, {1372},
		 {1373, 1379, 1378, 1377, 1376, 1374, 1375}, {1349}, {1350, 1351, 1352}, {1339},
		 {1337, 1338, 1314, 1315}, {1316}, {1317, 1318, 1319, 1320, 1330, 1321, 1331, 1322}, {1332},
		 {1333, 1336, 1335, 1334}, {1323, 1324, 1325, 1326, 1327, 1328, 1329}, {1340}, {1341, 1343, 1342}},
	.vertex_degree_out = {2, 2, 1, 2, 1, 0, 1, 1, 0, 1, 0, 2, 1, 1, 2, 1, 0, 0, 1, 0},
	.vertex_adjacent =
		{{1, 11}, {2, 9}, {3}, {4, 6}, {5}, {}, {7}, {8}, {}, {10}, {}, {12, 18}, {13}, {14}, {15, 17}, {16},
		 {}, {}, {19}, {}},
	.vertex_parents = {0xffffffffffffffff, 0, 1, 2, 3, 4, 3, 6, 7, 1, 9, 0, 11, 12, 13, 14, 15, 14, 11, 18},
	.edges_atom =
		{{1358, 1359}, {1357, 1358}, {1372, 1373}, {1371, 1372}, {1357, 1369}, {1355, 1357}, {1348, 1353},
		 {1349, 1350}, {1348, 1349}, {1346, 1348}, {1332, 1333}, {1330, 1332}, {1322, 1323}, {1316, 1317},
		 {1314, 1316}, {1339, 1337}, {1340, 1341}, {1339, 1340}, {1344, 1339}},
	.total_natoms = 67,
	.geometric_center =  {7.475761194029849, 7.669179104477611, -2.1305522388059703}
};

struct reference_rigtree reference_rigtree_gly = {
	.num_vertices = 2,
	.vertex_sizes = {3, 2},
	.vertex_atoms = {{2, 3, 4}, {1, 0}},
	.vertex_degree_out = {1, 0},
	.vertex_adjacent = {{1}},
	.vertex_parents = {0xffffffffffffffff, 0},
	.edges_atom = {{2, 1}},
	.total_natoms = 5,
	.geometric_center = {-45.1566, -18.3150, 29.389}
};

struct reference_rigtree reference_rigtree_bace_0 = {
	.num_vertices = 8,
	.vertex_sizes = {22, 1, 1, 3, 1, 6, 3, 2},
	.vertex_atoms = {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 33, 34, 35}, {19}, {20},
		  {21, 36, 37}, {22}, {23, 24, 25, 26, 27, 28}, {29, 30, 31}, {32, 38}},
	.vertex_degree_out = {1, 2, 1, 1, 1, 1, 0, 0},
	.vertex_adjacent = {{1}, {2, 7}, {3}, {4}, {5}, {6}, {}, {}},
	.vertex_parents = {0xffffffffffffffff, 0, 1, 2, 3, 4, 5, 1},
	.edges_atom = {{17, 19}, {19, 20}, {20, 21}, {21, 22}, {22, 23}, {27, 29}, {19, 32}},
	.total_natoms = 39,
	.geometric_center = {30.04625641025641869, 5.754666666666667041, 14.252435897435898227}
};

struct energy_prm {
	struct mol_atom_group *ag;
	struct agsetup *ag_setup;
	struct smp_rigforest *rf;
};


/**
 * This is an example energy function, used in rigtree minimization
 * \param[in] prm Pointer to structure containing arbitrary parameters. In this case it's energy_prm.
 * \param[in] array Input array of current coordinates of rigforest, i.e. rotation angles
 * \param[out] gradient Output array containing gradients for each coordinate. See description of \ref smp_rigforest_ndim for coordinate order.
 * \param[in] array_size Number of dimensions in our minimization problem, i.e. length of \p array and \p gradient.
 * \param[in] step Minimization step value. Usually unused.
 * \return The energy of rigforest configuration in configuration specified by \p array.
 */
static lbfgsfloatval_t energy_func_nemo(
	void *prm,
	const lbfgsfloatval_t *restrict array,
	lbfgsfloatval_t *restrict gradient,
	const int array_size,
	const lbfgsfloatval_t __attribute__((__unused__)) step)
{
	lbfgsfloatval_t energy;
	struct energy_prm* energy_prm = (struct energy_prm*) prm;
	struct mol_atom_group *ag = energy_prm->ag;
	struct agsetup *ags = energy_prm->ag_setup;
	struct smp_rigforest *rf = energy_prm->rf;


	if (array != NULL) {
		int ndim = smp_rigforest_ndim(rf);

		if (array_size != ndim) {
			fprintf(stderr, "energy_func_nemo error: mismatch in vector length\n");
			exit(EXIT_FAILURE);
		}
		smp_rigforest_update_from_coordinate_array(rf, ag, array);
	}

	check_clusterupdate(ag, ags);

	lbfgsfloatval_t en_vdweng = 0, en_bond = 0, en_ang = 0, en_dihed_i = 0, en_dihed_t = 0;

	mol_zero_gradients(ag);

	vdweng(ag, &en_vdweng, ags->nblst);
	vdwengs03(1.0, ags->nblst->nbcof, ag, &en_vdweng, ags->nf03, ags->listf03);
	beng(ag, &en_bond);
	aeng(ag, &en_ang);
	ieng(ag, &en_dihed_i);
	teng(ag, &en_dihed_t);

	energy = en_vdweng + en_bond + en_ang + en_dihed_i + en_dihed_t;

	smp_rigforest_derive(gradient, rf, ag);

	return energy;
}

static lbfgsfloatval_t energy_func_covalent_only (
		void* prm,
		const lbfgsfloatval_t* restrict array,
		lbfgsfloatval_t* restrict gradient,
		const int array_size,
		const lbfgsfloatval_t __attribute__((__unused__)) step)
{
	lbfgsfloatval_t energy;
	struct energy_prm* energy_prm = (struct energy_prm*) prm;
	struct mol_atom_group *ag = energy_prm->ag;
	struct smp_rigforest *rf = energy_prm->rf;

	if (array != NULL) {
		const int ndim = smp_rigforest_ndim(rf);
		if (array_size != ndim) {
			fprintf(stderr, "energy_func_covalent_only error: mismatch in vector length\n");
			exit(EXIT_FAILURE);
		}
		smp_rigforest_update_from_coordinate_array(rf, ag, array);
	}

	lbfgsfloatval_t en_bond = 0, en_ang = 0, en_dihed_i = 0, en_dihed_t = 0;

	mol_zero_gradients(ag);

	beng(ag, &en_bond);
	aeng(ag, &en_ang);
	ieng(ag, &en_dihed_i);
	teng(ag, &en_dihed_t);

	energy = en_bond + en_ang + en_dihed_i + en_dihed_t;

	smp_rigforest_derive(gradient, rf, ag);

	return energy;
}

/**
 * Check two arrays of integers for similarity, disregarding order.
 */
static bool lists_equal(const size_t *arr1, const size_t *arr2, size_t len) {
	bool *used = calloc(len, sizeof(bool));
	for (size_t i = 0; i < len; i++) {
		bool found = false;
		for (size_t j = 0; j < len; j++) {
			if (arr1[i] == arr2[j]) {
				if (used[j]) {
					free(used);
					return false;
				}
				used[j] = true;
				found = true;
				break;
			}
		}
		if (!found) {
			free(used);
			return false;
		}
	}
	free(used);
	return true;
}


/**
 * Check simple rigforest (or, probably, riggrove? it only has one tree) for correctness.
 */
void test_rigtree(
		const struct smp_rigforest *rf,
		const struct mol_atom_group *ag,
		const struct reference_rigtree *ref,
		const bool anchored)
{
	struct smp_rigtree *tr;
	ck_assert_int_eq(rf->nfree_atoms, 0);
	if (!anchored) {
		ck_assert_int_eq(rf->nfree_trees, 1);
		ck_assert_int_eq(rf->nanch_trees, 0);
		tr = rf->free_trees[0];
	} else {
		ck_assert_int_eq(rf->nfree_trees, 0);
		ck_assert_int_eq(rf->nanch_trees, 1);
		tr = rf->anch_trees[0];
	}

	ck_assert(rf->original_coordinates != NULL);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(rf->original_coordinates[i].X, ag->coords[i].X);
		ck_assert_coordinate_eq(rf->original_coordinates[i].Y, ag->coords[i].Y);
		ck_assert_coordinate_eq(rf->original_coordinates[i].Z, ag->coords[i].Z);
	}

	ck_assert(tr->nodes != NULL);
	ck_assert(tr->node_label_for_atom != NULL);
	ck_assert_double_eq(tr->tree_rotation.W, 1);
	ck_assert_double_eq(tr->tree_rotation.X, 0);
	ck_assert_double_eq(tr->tree_rotation.Y, 0);
	ck_assert_double_eq(tr->tree_rotation.Z, 0);
	ck_assert_double_eq(tr->tree_translation.X, 0);
	ck_assert_double_eq(tr->tree_translation.Y, 0);
	ck_assert_double_eq(tr->tree_translation.Z, 0);
	ck_assert(tr->edge_grads != NULL);

	// Check atoms in nodes
	ck_assert_int_eq(tr->nnodes, ref->num_vertices);
	for (size_t i = 0; i < tr->nnodes; i++) {
		ck_assert_int_eq(tr->nodes[i].label, i);
		ck_assert_int_eq(tr->nodes[i].natoms, ref->vertex_sizes[i]);
		ck_assert(tr->nodes[i].atoms != NULL);
		for (size_t j = 0; j < tr->nodes[i].natoms; j++) {
			ck_assert_int_eq(tr->nodes[i].atoms[j], ref->vertex_atoms[i][j]);
		}
	}
	ck_assert_int_eq(tr->natoms, ref->total_natoms);
	ck_assert(tr->atoms != NULL);
	// Check fixed atoms and auxiliary arrays in smp_rigtree
	int atom_count = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		int found = 0;
		size_t vertex = ref->num_vertices;
		for (size_t j = 0; j < ref->num_vertices; j++) {
			for (size_t k = 0; k < ref->vertex_sizes[j]; k++) {
				if (i == ref->vertex_atoms[j][k]) {
					found = 1;
					vertex = j;
					break;
				}
			}
		}
		int fixed = !found;
		ck_assert_int_eq(ag->fixed[i], fixed);
		if (found) {
			ck_assert_int_eq(tr->atoms[atom_count++], i);
			ck_assert_int_eq(tr->node_label_for_atom[i], vertex);
		} else {
			ck_assert_int_eq(tr->node_label_for_atom[i], -1);
		}
	}
	ck_assert_int_eq(atom_count, ref->total_natoms);
	// Check edges
	// Number of edges is always the same: number_of_vertices-1.
	// We check that all edges in returned structure are correct, and also that each edge is met only once.
	// This ensures that both lists are identical.
	int *edge_was_found = calloc(tr->nnodes - 1, sizeof(int));
	for (size_t i = 0; i < tr->nnodes - 1; i++) {
		size_t v1 = tr->edges[i].begin, v2 = tr->edges[i].end;
		bool found = 0;
		for (size_t j = 0; j < tr->nnodes - 1; j++) {
			if (ref->edges_atom[j][0] == v1 && ref->edges_atom[j][1] == v2) {
				// In case tr->edges has duplicate edges, we check that we haven't yet seen same edge
				ck_assert(edge_was_found[j] == 0);
				edge_was_found[j] = 1;
				found = 1;
				break;
			}
		}
		ck_assert(found == 1);
	}
	free(edge_was_found);
	// Check center-of-mass
	ck_assert_double_eq(tr->tree_center.X, ref->geometric_center[0]);
	ck_assert_double_eq(tr->tree_center.Y, ref->geometric_center[1]);
	ck_assert_double_eq(tr->tree_center.Z, ref->geometric_center[2]);
	// Check adjacency matrix
	// For each vertex, we check the list of its children, like we did with edges several lines earlier
	for (size_t i = 0; i < tr->nnodes; i++) {
		ck_assert_int_eq(tr->node_degrees[i], ref->vertex_degree_out[i]);
		ck_assert(lists_equal(tr->adjacency_matrix[i], ref->vertex_adjacent[i], ref->vertex_degree_out[i]) == 1);
	}
	// Check bond angles
	for (size_t i = 0; i < tr->nnodes-1; i++) {
		ck_assert_double_eq(tr->edge_thetas[i], 0.0);
	}
	// Check vertex parents and indices of edges to them
	for (size_t i = 0; i < tr->nnodes; i++) {
		ck_assert_int_eq(tr->node_parents[i], ref->vertex_parents[i]);
		size_t edge_index = tr->_edge_index[i];
		if (i == 0) {
			ck_assert_int_eq(edge_index, -1);
		} else {
			size_t atom1 = tr->edges[edge_index].begin;
			size_t atom2 = tr->edges[edge_index].end;
			ck_assert_int_eq(tr->node_label_for_atom[atom1], tr->node_parents[i]);
			ck_assert_int_eq(tr->node_label_for_atom[atom2], i);
		}
	}
}


START_TEST(test_smp_rigforest_create_nemo)
{
	// This tests only reads rigid tree, not concerning itself with free atoms or rigid sidechains
	struct mol_atom_group *ag = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	ck_assert(ag != NULL);
	mol_atom_group_read_geometry(ag, "nemo-with-9546-cl1/nemo-with-9546-cl1.psf", "charmm22_extended.prm",
		"charmm22_extended.rtf");
	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag);
	ck_assert(rf != NULL);
	test_rigtree(rf, ag, &reference_rigtree_nemo, false);
}
END_TEST


START_TEST(test_smp_rigforest_create_gly)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	ck_assert(ag != NULL);
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	ck_assert(rf != NULL);
	test_rigtree(rf, ag, &reference_rigtree_gly, false);
}
END_TEST


START_TEST(test_smp_rigforest_init_gly)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	ck_assert(ag != NULL);
	struct smp_rigforest rf;
	smp_rigforest_init(&rf, "gly/gly.txt", ag);
	test_rigtree(&rf, ag, &reference_rigtree_gly, false);
}
END_TEST


START_TEST(test_smp_rigforest_create_bace_0)
{
	// Unlike gly, is anchored tree, not free
	struct mol_atom_group *ag = mol_read_json("bace_0.json");
	ck_assert(ag != NULL);
	struct smp_rigforest *rf = smp_rigforest_create("bace_0.txt", ag);
	ck_assert(rf != NULL);
	test_rigtree(rf, ag, &reference_rigtree_bace_0, true);
}
END_TEST


START_TEST(test_smp_rigforest_reload_nemo)
{
	struct mol_atom_group *ag_orig = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct mol_atom_group *ag_zero = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1_zeros.pdb");
	struct reference_rigtree reference_rigtree_nemo_zero = reference_rigtree_nemo;
	reference_rigtree_nemo_zero.geometric_center[0] = 0;
	reference_rigtree_nemo_zero.geometric_center[1] = 0;
	reference_rigtree_nemo_zero.geometric_center[2] = 0;

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_zero);

	smp_rigforest_reload(rf, "nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_orig);
	test_rigtree(rf, ag_orig, &reference_rigtree_nemo, false);

	smp_rigforest_reload(rf, "nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_zero);
	test_rigtree(rf, ag_zero, &reference_rigtree_nemo_zero, false);
}
END_TEST


START_TEST(test_smp_rigforest_destroy_gly)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	smp_rigforest_destroy(rf);
	free(rf);
}
END_TEST


START_TEST(test_smp_rigforest_free_gly)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	smp_rigforest_free(rf);
}
END_TEST


START_TEST(test_smp_rigforest_free_null)
{
	smp_rigforest_free(NULL);
}
END_TEST


START_TEST(test_smp_rigforest_ndim_free)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	size_t ndim = smp_rigforest_ndim(rf);
	ck_assert_int_eq(ndim, 7); // 1 bond plus 6 rigid body DoF
}
END_TEST

START_TEST(test_smp_rigforest_ndim_anchored)
{
	struct mol_atom_group *ag = mol_read_json("bace_0.json");
	struct smp_rigforest *rf = smp_rigforest_create("bace_0.txt", ag);
	size_t ndim = smp_rigforest_ndim(rf);
	ck_assert_int_eq(ndim, 7); // 7 bonds plus 0 rigid body DoF
}
END_TEST

START_TEST(test_smp_rigforest_copy_coords_to_atomgroup_nemo)
{
	struct mol_atom_group *ag = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag);

	for (size_t i = 0; i < ag->natoms; i++)
		MOL_VEC_SET_SCALAR(rf->original_coordinates[i], 0.0);

	smp_rigforest_copy_coords_to_atomgroup(ag, rf);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_double_eq(ag->coords[i].X, 0);
		ck_assert_double_eq(ag->coords[i].Y, 0);
		ck_assert_double_eq(ag->coords[i].Z, 0);
	}
}
END_TEST


START_TEST(test_smp_rigforest_copy_coords_from_atomgroup_nemo)
{
	struct mol_atom_group *ag_orig = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct mol_atom_group *ag_zero = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1_zeros.pdb");

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_orig);

	ag_zero->fixed = calloc(ag_zero->natoms, sizeof(bool));
	memcpy(ag_zero->fixed, ag_orig->fixed, ag_orig->natoms * sizeof(bool));

	smp_rigforest_copy_coords_from_atomgroup(rf, ag_zero);
	// smp_rigforest_copy_coords_from_atomgroup changes coordinates of atoms, but not center-of-mass.
	test_rigtree(rf, ag_zero, &reference_rigtree_nemo, false);
}
END_TEST


START_TEST(test_smp_rigforest_update_from_coordinate_array_theta_free)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct mol_atom_group *ag_rot = mol_read_pdb("gly/gly_rot_0.5.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);

	ag_rot->fixed = calloc(ag->natoms, sizeof(bool));
	memcpy(ag_rot->fixed, ag->fixed, ag->natoms * sizeof(bool));

	const size_t ndim = smp_rigforest_ndim(rf);
	double *coords = calloc(ndim, sizeof(double));
	coords[0] = -0.5;
	for (size_t i = 1; i < ndim; i++)
		coords[i] = 0.0;

	smp_rigforest_update_from_coordinate_array(rf, ag, coords);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_rot->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_rot->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_rot->coords[i].Z);
	}

	smp_rigforest_copy_coords_from_atomgroup(rf, ag);
	ck_assert_double_eq(rf->free_trees[0]->edge_thetas[0], -0.5);
	rf->free_trees[0]->edge_thetas[0] = 0; // Reset to zero so following test does not fail.
	test_rigtree(rf, ag_rot, &reference_rigtree_gly, false);
}
END_TEST

START_TEST(test_smp_rigforest_update_from_coordinate_array_theta_anchored)
{
	struct mol_atom_group *ag = mol_read_json("bace_0.json");
	struct mol_atom_group *ag_rot = mol_read_pdb("bace_0_rot_0.5.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("bace_0.txt", ag);

	ag_rot->fixed = calloc(ag->natoms, sizeof(bool));
	memcpy(ag_rot->fixed, ag->fixed, ag->natoms * sizeof(bool));

	const size_t ndim = smp_rigforest_ndim(rf);
	double *coords = calloc(ndim, sizeof(double));
	for (size_t i = 0; i < ndim; i++) coords[i] = 0.0;
	coords[0] = 0.5; // Rotate dihedral between atoms 28 and 30

	smp_rigforest_update_from_coordinate_array(rf, ag, coords);

	mol_write_pdb("out.pdb", ag);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_rot->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_rot->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_rot->coords[i].Z);
	}

	smp_rigforest_copy_coords_from_atomgroup(rf, ag);
	ck_assert_double_eq(rf->anch_trees[0]->edge_thetas[0], 0.5);
	rf->anch_trees[0]->edge_thetas[0] = 0; // Reset to zero so following test does not fail.
	test_rigtree(rf, ag_rot, &reference_rigtree_bace_0, true);
}
END_TEST


START_TEST(test_smp_rigforest_update_from_coordinate_array_translate)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct mol_atom_group *ag_trans = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);

	ag_trans->fixed = calloc(ag->natoms, sizeof(bool));
	memcpy(ag_trans->fixed, ag->fixed, ag->natoms * sizeof(bool));
	const struct mol_vector3 delta = {.X = 1, .Y = -2, .Z = 3};
	for (size_t i = 0; i < ag_trans->natoms; ++i) {
		MOL_VEC_ADD(ag_trans->coords[i], ag_trans->coords[i], delta);
	}

	const size_t ndim = smp_rigforest_ndim(rf);
	double *coords = calloc(ndim, sizeof(double));
	for (size_t i = 0; i < ndim; i++)
		coords[i] = 0.0;
	coords[4] = delta.X; coords[5] = delta.Y; coords[6] = delta.Z;

	smp_rigforest_update_from_coordinate_array(rf, ag, coords);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_trans->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_trans->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_trans->coords[i].Z);
	}

	smp_rigforest_copy_coords_from_atomgroup(rf, ag);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation.X, delta.X);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation.Y, delta.Y);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation.Z, delta.Z);
	// Reset to zero so following test does not fail
	rf->free_trees[0]->tree_translation.X = rf->free_trees[0]->tree_translation.Y = rf->free_trees[0]->tree_translation.Z = 0.0;
	test_rigtree(rf, ag_trans, &reference_rigtree_gly, false);
}
END_TEST


START_TEST(test_smp_rigforest_update_from_coordinate_array_rotate)
{
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct mol_atom_group *ag_rot = mol_read_pdb("gly/gly_rot_rb.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);

	ag_rot->fixed = calloc(ag->natoms, sizeof(bool));
	memcpy(ag_rot->fixed, ag->fixed, ag->natoms * sizeof(bool));
	// Rotation by 60 degrees in exp and quaternion form
	const struct mol_vector3 v60 = {
		.X = 1 * M_PI / 3 / sqrt(14),
		.Y = 2 * M_PI / 3 / sqrt(14),
		.Z = 3 * M_PI / 3 / sqrt(14)};
	const struct mol_quaternion q60 = {
		.W = sqrt(3) / 2,
		.X = 0.5 / sqrt(14),
		.Y = 1 / sqrt(14),
		.Z = 1.5 / sqrt(14)};

	const size_t ndim = smp_rigforest_ndim(rf);
	double *coords = calloc(ndim, sizeof(double));
	for (size_t i = 0; i < ndim; i++)
		coords[i] = 0.0;
	coords[1] = v60.X; coords[2] = v60.Y; coords[3] = v60.Z;

	smp_rigforest_update_from_coordinate_array(rf, ag, coords);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_rot->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_rot->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_rot->coords[i].Z);
	}

	smp_rigforest_copy_coords_from_atomgroup(rf, ag);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation.W, q60.W);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation.X, q60.X);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation.Y, q60.Y);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation.Z, q60.Z);
	// Reset to zero so following test does not fail
	rf->free_trees[0]->tree_rotation.W = 1;
	rf->free_trees[0]->tree_rotation.X = rf->free_trees[0]->tree_rotation.Y = rf->free_trees[0]->tree_rotation.Z = 0.0;
	test_rigtree(rf, ag_rot, &reference_rigtree_gly, false);
}
END_TEST


START_TEST(test_smp_rigforest_update_center_nemo)
{
	struct mol_atom_group *ag_orig = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct mol_atom_group *ag_zero = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1_zeros.pdb");
	struct reference_rigtree reference_rigtree_nemo_zero = reference_rigtree_nemo;
	reference_rigtree_nemo_zero.geometric_center[0] = 0;
	reference_rigtree_nemo_zero.geometric_center[1] = 0;
	reference_rigtree_nemo_zero.geometric_center[2] = 0;

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_orig);

	smp_rigforest_update_center(rf, ag_zero);
	test_rigtree(rf, ag_orig, &reference_rigtree_nemo_zero, false);
}
END_TEST


START_TEST(test_smp_rigforest_move_nemo)
{
	struct mol_atom_group *ag_orig = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct mol_atom_group *ag_rot = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1_rot.pdb");

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_orig);
	test_rigtree(rf, ag_orig, &reference_rigtree_nemo, false);

	// Unfortunately, smp_rigforest_init also changes atom group, so we have to artificially call it here
	struct smp_rigforest *rf_tmp = calloc(1, sizeof(struct smp_rigforest));
	smp_rigforest_init(rf_tmp, "nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag_rot);

	struct mol_vector3 *orig_old = calloc(ag_orig->natoms, sizeof(struct mol_vector3));
	memcpy(orig_old, rf->original_coordinates, 3*ag_orig->natoms*sizeof(double));

	// Rotate dihedral 1358-1359-1360-1361 by 1 radian relative to original angle
	rf->free_trees[0]->edge_thetas[0] = 1;
	smp_rigforest_move(ag_orig, rf);

	// Check that necessary values were not affected
	ck_assert_double_eq(1, rf->free_trees[0]->edge_thetas[0]);
	for (size_t i = 0; i < ag_orig->natoms; i++) {
		ck_assert_double_eq(rf->original_coordinates[i].X, orig_old[i].X);
		ck_assert_double_eq(rf->original_coordinates[i].Y, orig_old[i].Y);
		ck_assert_double_eq(rf->original_coordinates[i].Z, orig_old[i].Z);
	}

	// Update coordinates and angles in smp_rigforest, so it looks brand-new
	smp_rigforest_copy_coords_from_atomgroup(rf, ag_orig);
	rf->free_trees[0]->edge_thetas[0] = 0;

	// Compare with reference file
	test_rigtree(rf, ag_rot, &reference_rigtree_nemo, false);

	// Rotate back
	rf->free_trees[0]->edge_thetas[0] = -1;
	smp_rigforest_move(ag_orig, rf);

	// Check that atom coordinates reverted to initial values
	smp_rigforest_copy_coords_from_atomgroup(rf, ag_orig);
	for (size_t i = 0; i < ag_orig->natoms; i++) {
		ck_assert_double_eq(rf->original_coordinates[i].X, orig_old[i].X);
		ck_assert_double_eq(rf->original_coordinates[i].Y, orig_old[i].Y);
		ck_assert_double_eq(rf->original_coordinates[i].Z, orig_old[i].Z);
	}
}
END_TEST


START_TEST(test_smp_rigforest_move_gly)
{
	struct mol_atom_group *ag_orig = mol_read_pdb("gly/gly.pdb");
	struct mol_atom_group *ag_rot_1 = mol_read_pdb("gly/gly_rot_0.5.pdb");
	struct mol_atom_group *ag_rot_2 = mol_read_pdb("gly/gly_rot_0.6.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag_orig);

	// We need to initialize .fixed fields in atomgroups
	ag_rot_1->fixed = calloc(ag_orig->natoms, sizeof(bool));
	ag_rot_2->fixed = calloc(ag_orig->natoms, sizeof(bool));
	memcpy(ag_rot_1->fixed, ag_orig->fixed, ag_orig->natoms * sizeof(bool));
	memcpy(ag_rot_2->fixed, ag_orig->fixed, ag_orig->natoms * sizeof(bool));

	// Rotate first edge by 0.5 radian
	rf->free_trees[0]->edge_thetas[0] = -0.5;
	smp_rigforest_move(ag_orig, rf);
	smp_rigforest_copy_coords_from_atomgroup(rf, ag_orig);
	rf->free_trees[0]->edge_thetas[0] = 0;
	test_rigtree(rf, ag_rot_1, &reference_rigtree_gly, false);

	// Further rotate first edge by 0.1 radian
	rf->free_trees[0]->edge_thetas[0] = -0.1;
	smp_rigforest_move(ag_orig, rf);
	smp_rigforest_copy_coords_from_atomgroup(rf, ag_orig);
	rf->free_trees[0]->edge_thetas[0] = 0;
	test_rigtree(rf, ag_rot_2, &reference_rigtree_gly, false);
}
END_TEST


START_TEST(test_smp_rigforest_move_bace_0)
{
	struct mol_atom_group *ag_orig = mol_read_json("bace_0.json");
	struct mol_atom_group *ag_rot_1 = mol_read_pdb("bace_0_rot_0.5.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("bace_0.txt", ag_orig);

	// We need to initialize .fixed fields in atomgroups
	ag_rot_1->fixed = calloc(ag_orig->natoms, sizeof(bool));
	memcpy(ag_rot_1->fixed, ag_orig->fixed, ag_orig->natoms * sizeof(bool));

	// Rotate first edge by 0.5 radian
	rf->anch_trees[0]->edge_thetas[0] = 0.5;
	smp_rigforest_move(ag_orig, rf);
	smp_rigforest_copy_coords_from_atomgroup(rf, ag_orig);
	rf->anch_trees[0]->edge_thetas[0] = 0;
	test_rigtree(rf, ag_rot_1, &reference_rigtree_bace_0, true);
}
END_TEST


START_TEST(test_smp_rigforest_derive_nemo)
{
	// This is regression test that compares energy values and gradients to precomputed ones
	// smp_rigforest_derive is called within energy_func_nemo
	// NOTE: the reference values were obtained using the old libmol that did not properly treat parameters of the
	//       improper dihedrals. charmm22_extended.prm from test data was changed to restore the original (slightly
	//       incorrect) energy and gradient values and make this test pass.
	struct mol_atom_group *ag = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	ag->gradients = calloc(ag->natoms, sizeof(struct mol_vector3));
	mol_atom_group_read_geometry(ag, "nemo-with-9546-cl1/nemo-with-9546-cl1.psf", "charmm22_extended.prm",
		"charmm22_extended.rtf");
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL); // Make nothing fixed

	struct agsetup ag_setup;
	init_nblst(ag, &ag_setup);
	update_nblst(ag, &ag_setup);
	smp_scale_vdw_radius(ag, 0.8);

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag);

	struct energy_prm prms = { .rf = rf, .ag = ag, .ag_setup = &ag_setup};

	const size_t ndim = smp_rigforest_ndim(rf);
	double *grad = calloc(ndim, sizeof(double));
	double energy = energy_func_nemo(&prms, NULL, grad, 0, 0);

	ck_assert_double_eq_tol(energy, 436.00336822091003, 1e-4);
	// Check tr->edge_grads and grad
	const double grad_ref[] = {0.515145227455, -5.001619445835, -0.642930547265, -0.382748575545, -0.813918415862,
	                           -1.345883005392, 2.612629709899, -0.310730483376, 1.861209791909, 2.171982878208,
	                           -0.223222502714, 0.908437401651, -0.113987989630, -7.949250994517, 0.905402951341,
	                           1.708790654747, -0.315140666077, 0.114759073109, -15.816830655713, -3.629563444700,
	                           3.339899714226, -3.440878165091, -4.495088348685, 4.865087244451, -1.778455487138};

	// Check gradients of rotatable bonds
	for (size_t i = 0; i < rf->free_trees[0]->nnodes - 1; i++) {
		ck_assert_double_eq(rf->free_trees[0]->edge_grads[i], grad[i]); // These two should be strictly identical
		ck_assert_double_eq_tol(grad[i], grad_ref[i], 1e-4); // But they can differ from reference because floating point
	}

	// Check gradients of rigid body movement
	for (size_t i = rf->free_trees[0]->nnodes - 1; i < ndim; i++) {
		ck_assert_double_eq_tol(grad[i], grad_ref[i], 1e-4);
	}
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation_grads.X, grad[ndim-6]);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation_grads.Y, grad[ndim-5]);
	ck_assert_double_eq(rf->free_trees[0]->tree_rotation_grads.Z, grad[ndim-4]);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation_grads.X, grad[ndim-3]);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation_grads.Y, grad[ndim-2]);
	ck_assert_double_eq(rf->free_trees[0]->tree_translation_grads.Z, grad[ndim-1]);
}
END_TEST


START_TEST(test_smp_rigforest_derive_gly)
{
	// And in here we compare energy and derivatives with analytical ones
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	ag->gradients = calloc(ag->natoms, sizeof(struct mol_vector3));
	mol_atom_group_read_geometry(ag, "gly/gly.psf", "charmm22_extended.prm", "charmm22_extended.rtf");
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL); // Make nothing fixed

	struct agsetup ag_setup;
	init_nblst(ag, &ag_setup);
	update_nblst(ag, &ag_setup);

	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);

	struct energy_prm prms = { .rf = rf, .ag = ag, .ag_setup = &ag_setup};

	double *grad = calloc(smp_rigforest_ndim(rf), sizeof(double));
	double energy0 = energy_func_covalent_only(&prms, NULL, grad, 0, 0);

	// Our tree does not interact with anything, so DoF for rigid-body should be zero.
	for (size_t i = rf->free_trees[0]->nnodes - 1; i < rf->free_trees[0]->nnodes+5; i++) {
		ck_assert_double_eq_tol(grad[i], 0, 1e-9);
	}

	// We only have one bond in our structure. Let's try rotating it!
	const double delta = 1e-3;
	rf->free_trees[0]->edge_thetas[0] = delta;
	smp_rigforest_move(ag, rf);
	double energy1 = energy_func_covalent_only(&prms, NULL, NULL, 0, 0);
	ck_assert_double_eq_tol((energy1 - energy0) / delta, grad[0], 1e-9);
}
END_TEST


START_TEST(test_smp_rigforest_derive_bace_0)
{
	// And in here we compare energy and derivatives with analytical ones
	struct mol_atom_group *ag = mol_read_json("bace_0.json");
	ag->gradients = calloc(ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL); // Make nothing fixed

	struct agsetup ag_setup;
	init_nblst(ag, &ag_setup);
	update_nblst(ag, &ag_setup);

	struct smp_rigforest *rf = smp_rigforest_create("bace_0.txt", ag);

	struct energy_prm prms = { .rf = rf, .ag = ag, .ag_setup = &ag_setup };

	double *grad = calloc(smp_rigforest_ndim(rf), sizeof(double));
	double energy0 = energy_func_covalent_only(&prms, NULL, grad, 0, 0);

	mol_write_pdb("out1.pdb", ag);

	// Rotate every dihedral one-by-one
	for (int bond = 0; bond < 7; bond++) {
		const double delta = 1e-5;
		rf->anch_trees[0]->edge_thetas[bond] = delta;
		smp_rigforest_move(ag, rf);
		double energy1 = energy_func_covalent_only(&prms, NULL, NULL, 0, 0);
		const double grad_num = (energy1 - energy0) / delta;
		ck_assert_double_eq_tol(grad_num, grad[bond], 1e-4);
		rf->anch_trees[0]->edge_thetas[bond] = 0.0;
	}
}
END_TEST


START_TEST(test_smp_rigforest_minimize)
{
	// NOTE: the reference values were obtained using the old libmol that did not properly treat parameters of the
	//       improper dihedrals. charmm22_extended.prm from test data was changed to restore the original (slightly
	//       incorrect) energy and gradient values and make this test pass.
	struct mol_atom_group *ag = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1.pdb");
	struct mol_atom_group *ag_min = mol_read_pdb("nemo-with-9546-cl1/nemo-with-9546-cl1_min.pdb");
	ag->gradients = calloc(ag->natoms, sizeof(struct mol_vector3));
	mol_atom_group_read_geometry(ag, "nemo-with-9546-cl1/nemo-with-9546-cl1.psf", "charmm22_extended.prm",
		"charmm22_extended.rtf");
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL); // Make nothing fixed

	struct agsetup ag_setup;
	init_nblst(ag, &ag_setup);
	update_nblst(ag, &ag_setup);
	smp_scale_vdw_radius(ag, 0.8);

	struct smp_rigforest *rf = smp_rigforest_create("nemo-with-9546-cl1/nemo-with-9546-cl1.txt", ag);

	struct energy_prm prms = { .rf = rf, .ag = ag, .ag_setup = &ag_setup};
	const size_t ndim = smp_rigforest_ndim(rf);

	double energy = energy_func_nemo(&prms, NULL, NULL, 0, 0);
	ck_assert_double_eq_tol(energy, 436.00336822091003, 1e-4);

	mol_fixed_update(ag, 0, NULL); // unfreeze all atoms
	smp_rigforest_minimize(500, 1.0e-3, rf, &prms, energy_func_nemo);
	// Minimization is complicated, so we lower tolerance to 0.5 Angstrom
	const double tolerance_after_min = 0.5;
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_double_eq_tol(ag->coords[i].X, ag_min->coords[i].X, tolerance_after_min);
		ck_assert_double_eq_tol(ag->coords[i].Y, ag_min->coords[i].Y, tolerance_after_min);
		ck_assert_double_eq_tol(ag->coords[i].Z, ag_min->coords[i].Z, tolerance_after_min);
	}

	// Check that we have low gradients after minimization
	double *grad = calloc(ndim, sizeof(double));
	energy_func_nemo(&prms, NULL, grad, 0, 0);
	for (size_t i = 0; i < ndim; i++) {
		ck_assert_double_eq_tol(grad[i], 0, 1e-2);
	}
}
END_TEST


Suite *rigtree_suite(void)
{
	Suite *suite = suite_create("rigforest");

	TCase *tcase = tcase_create("test_rigforest");
	tcase_set_timeout(tcase, 30); // test_smp_rigforest_minimize is long
	tcase_add_test(tcase, test_smp_rigforest_create_gly);
	tcase_add_test(tcase, test_smp_rigforest_create_bace_0);
	tcase_add_test(tcase, test_smp_rigforest_create_nemo);
	tcase_add_test(tcase, test_smp_rigforest_init_gly);
	tcase_add_test(tcase, test_smp_rigforest_reload_nemo);
	tcase_add_test(tcase, test_smp_rigforest_destroy_gly);
	tcase_add_test(tcase, test_smp_rigforest_free_gly);
	tcase_add_test(tcase, test_smp_rigforest_free_null);
	tcase_add_test(tcase, test_smp_rigforest_ndim_free);
	tcase_add_test(tcase, test_smp_rigforest_ndim_anchored);
	tcase_add_test(tcase, test_smp_rigforest_copy_coords_to_atomgroup_nemo);
	tcase_add_test(tcase, test_smp_rigforest_copy_coords_from_atomgroup_nemo);
	tcase_add_test(tcase, test_smp_rigforest_update_from_coordinate_array_theta_free);
	tcase_add_test(tcase, test_smp_rigforest_update_from_coordinate_array_theta_anchored);
	tcase_add_test(tcase, test_smp_rigforest_update_from_coordinate_array_translate);
	tcase_add_test(tcase, test_smp_rigforest_update_from_coordinate_array_rotate);
	tcase_add_test(tcase, test_smp_rigforest_update_center_nemo);
	tcase_add_test(tcase, test_smp_rigforest_move_gly);
	tcase_add_test(tcase, test_smp_rigforest_move_nemo);
	tcase_add_test(tcase, test_smp_rigforest_move_bace_0);
	tcase_add_test(tcase, test_smp_rigforest_derive_nemo);
	tcase_add_test(tcase, test_smp_rigforest_derive_gly);
	tcase_add_test(tcase, test_smp_rigforest_derive_bace_0);
	tcase_add_test(tcase, test_smp_rigforest_minimize);
	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = rigtree_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
