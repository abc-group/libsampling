#include <check.h>
#include <unistd.h>

#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <stdio.h>
#include <check.h>
#include <math.h>
#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/icharmm.h>
#include <mol2/gbsa.h>

#include "movers.h"
#include "utils.h"
#include "energy.h"
#include "prng.h"


const double tolerance_pdb = 1e-3;
const double tolerance = 1e-5;
const double tolerance_strict = 1e-9;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);

#define ck_assert_coordinate_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_pdb);

struct mol_prms *params;

static void _compare_ags(const struct mol_atom_group *ag1, const struct mol_atom_group *ag2)
{
	ck_assert(ag1->natoms == ag2->natoms);
	for (size_t i = 0; i < ag1->natoms; i++) {
		ck_assert_coordinate_eq(ag1->coords[i].X, ag2->coords[i].X);
		ck_assert_coordinate_eq(ag1->coords[i].Y, ag2->coords[i].Y);
		ck_assert_coordinate_eq(ag1->coords[i].Z, ag2->coords[i].Z);
	}

}

static void _check_vdw(const struct mol_atom_group *ag)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		if (strcmp(ag->atom_name[i], " H  ") != 0) // Don't check hydrogens
			ck_assert(ag->vdw_radius[i] > 0);
	}
}


static void _setup_system(struct mol_atom_group **ag, struct agsetup *ags, struct acesetup *acs,
                          const char *pdb, const char *psf, const char *prm, const char *rtf)
{
	*ag = mol_read_pdb(pdb);
	mol_atom_group_read_geometry(*ag, psf, prm, rtf);
	mol_atom_group_add_prms(*ag, params);
	(*ag)->gradients = calloc((*ag)->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(*ag);
	mol_fixed_update(*ag, 0, NULL); // Make nothing fixed
	init_nblst(*ag, ags);
	update_nblst(*ag, ags);
	ace_ini(*ag, acs);
	ace_fixedupdate(*ag, ags, acs);
	ace_updatenblst(ags, acs);
	smp_atom_group_init_backbone_by_name(*ag);
}

static struct mol_index_list _generate_atom_list_for_second_peptide_in_ala_pdb()
{
	struct mol_index_list lig_atom_list;
	lig_atom_list.size = 15;
	lig_atom_list.members = calloc(lig_atom_list.size, sizeof(size_t));
	for (size_t i = 0; i < lig_atom_list.size; i++)
		lig_atom_list.members[i] = i + 15; // Atoms 16-30 in PDB, 15-29 in zero-based arrays
	return lig_atom_list;
}

START_TEST(test_smp_slide_into_contact_backbone_empty)
{
	struct mol_atom_group *test_ag;
	struct agsetup ags;
	struct acesetup acs;
	_setup_system(&test_ag, &ags, &acs, "ala.pdb", "ala.psf", "protein.prm", "protein.rtf");
	struct mol_atom_group *ref_ag = mol_read_pdb("ala.pdb");
	struct mol_vector3 translation;
	struct mol_index_list lig_atom_list, interface_residue_list;
	lig_atom_list.size = interface_residue_list.size = 0;

	unsigned int nsteps = smp_movers_slide_into_contact_backbone(test_ag, &ags, &acs, &translation,
                                                             lig_atom_list, interface_residue_list, 0.1, 100.0,
                                                             1.);

	ck_assert_int_eq(nsteps, 0);
	ck_assert_double_eq(translation.X, 0);
	ck_assert_double_eq(translation.Y, 0);
	ck_assert_double_eq(translation.Z, 0);
	_compare_ags(ref_ag, test_ag);
}
END_TEST

START_TEST(test_smp_slide_into_contact_backbone_push)
{
	struct mol_atom_group *test_ag;
	struct agsetup ags;
	struct acesetup acs;
	// Original backbone-backbone distance is ~20A
	_setup_system(&test_ag, &ags, &acs, "ala_far.pdb", "ala.psf", "protein.prm", "protein.rtf");
	const struct mol_atom_group *ref_ag = mol_read_pdb("ala_far_pushed.pdb");
	struct mol_vector3 translation;
	struct mol_index_list lig_atom_list, interface_residue_list;

	interface_residue_list.size = 4; // All residues
	interface_residue_list.members = calloc(interface_residue_list.size, sizeof(size_t));
	for (size_t i = 0; i < interface_residue_list.size; i++)
		interface_residue_list.members[i] = i;

	lig_atom_list = _generate_atom_list_for_second_peptide_in_ala_pdb();

	unsigned int nsteps = smp_movers_slide_into_contact_backbone(test_ag, &ags, &acs, &translation,
	                                                             lig_atom_list, interface_residue_list, 0.1, 100.0,
	                                                             1.);

	ck_assert_int_eq(nsteps, 173);
	ck_assert_coordinate_eq(translation.X, -15.6981752);
	ck_assert_coordinate_eq(translation.Y, -3.56824919);
	ck_assert_coordinate_eq(translation.Z,  6.33442118);

	_compare_ags(ref_ag, test_ag);
	_check_vdw(test_ag);
}
END_TEST

START_TEST(test_smp_slide_into_contact_backbone_pull)
{
	struct mol_atom_group *test_ag;
	struct agsetup ags;
	struct acesetup acs;
	_setup_system(&test_ag, &ags, &acs, "ala_overlap.pdb", "ala.psf", "protein.prm", "protein.rtf");
	const struct mol_atom_group *ref_ag = mol_read_pdb("ala_overlap_pulled.pdb");
	struct mol_vector3 translation;
	struct mol_index_list lig_atom_list, interface_residue_list;

	interface_residue_list.size = 4; // All residues
	interface_residue_list.members = calloc(interface_residue_list.size, sizeof(size_t));
	for (size_t i = 0; i < interface_residue_list.size; i++)
		interface_residue_list.members[i] = i;

	lig_atom_list = _generate_atom_list_for_second_peptide_in_ala_pdb();

	unsigned int nsteps = smp_movers_slide_into_contact_backbone(test_ag, &ags, &acs, &translation,
		lig_atom_list, interface_residue_list, 0.1, 100.0,1.);

	ck_assert_int_eq(nsteps, 41);
	ck_assert_coordinate_eq(translation.X,  3.61972708);
	ck_assert_coordinate_eq(translation.Y,  1.43441881);
	ck_assert_coordinate_eq(translation.Z, -1.28453049);

	_compare_ags(ref_ag, test_ag);
	_check_vdw(test_ag);
}
END_TEST

START_TEST(test_smp_slide_into_contact_backbone_pull_real_all)
{
	struct mol_atom_group *test_ag;
	struct agsetup ags;
	struct acesetup acs;
	_setup_system(&test_ag, &ags, &acs, "1bgs-beta-hsd.pdb", "1bgs-beta-hsd.psf",
	              "charmm22_extended.prm", "charmm22_extended.rtf");
	const struct mol_atom_group *ref_ag = mol_read_pdb("1bgs-beta-hsd-pulled-all.pdb");
	struct mol_vector3 translation;
	struct mol_index_list lig_atom_list, interface_residue_list;

	interface_residue_list.size = 25;  // All residues
	interface_residue_list.members = calloc(interface_residue_list.size, sizeof(size_t));
	for (size_t i = 0; i < interface_residue_list.size; i++)
		interface_residue_list.members[i] = i;

	lig_atom_list.size = 15;
	lig_atom_list.members = calloc(lig_atom_list.size, sizeof(size_t));
	for (size_t i = 0; i < lig_atom_list.size; i++)
		lig_atom_list.members[i] = i + 264; // Atoms 265-279 in PDB, 264-278 in zero-based arrays

	unsigned int nsteps = smp_movers_slide_into_contact_backbone(test_ag, &ags, &acs, &translation,
		lig_atom_list, interface_residue_list, 0.1, 100.0, 1.);

	ck_assert_int_eq(nsteps, 43);
	ck_assert_coordinate_eq(translation.X, -1.5819815);
	ck_assert_coordinate_eq(translation.Y, -3.7145692);
	ck_assert_coordinate_eq(translation.Z, -1.4796319);
	mol_write_pdb("1bgs-beta-hsd-pulled-all.pdb", test_ag);
	_compare_ags(ref_ag, test_ag);
}
END_TEST

START_TEST(test_smp_slide_into_contact_backbone_pull_real_interface)
{
	struct mol_atom_group *test_ag;
	struct agsetup ags;
	struct acesetup acs;
	_setup_system(&test_ag, &ags, &acs, "1bgs-beta-hsd.pdb", "1bgs-beta-hsd.psf",
	              "charmm22_extended.prm", "charmm22_extended.rtf");
	const struct mol_atom_group *ref_ag = mol_read_pdb("1bgs-beta-hsd-pulled-interface.pdb");
	struct mol_vector3 translation;
	struct mol_index_list lig_atom_list, interface_residue_list;

	// Interface: ligand and residues 100-102 of receptor
	interface_residue_list.size = 4;
	interface_residue_list.members = calloc(interface_residue_list.size, sizeof(size_t));
	interface_residue_list.members[0] = 14;
	interface_residue_list.members[1] = 15;
	interface_residue_list.members[2] = 16;
	interface_residue_list.members[3] = 24;

	lig_atom_list.size = 15;
	lig_atom_list.members = calloc(lig_atom_list.size, sizeof(size_t));
	for (size_t i = 0; i < lig_atom_list.size; i++)
		lig_atom_list.members[i] = i + 264; // Atoms 265-279 in PDB, 264-278 in zero-based arrays

	unsigned int nsteps = smp_movers_slide_into_contact_backbone(test_ag, &ags, &acs, &translation,
		lig_atom_list, interface_residue_list, 0.1, 100.0, 1.);

	ck_assert_int_eq(nsteps, 63);
	ck_assert_coordinate_eq(translation.X,  2.9605300);
	ck_assert_coordinate_eq(translation.Y, -5.1206208);
	ck_assert_coordinate_eq(translation.Z, -2.1689871);
	_compare_ags(ref_ag, test_ag);
}
END_TEST

START_TEST(test_smp_movers_randomly_rotate_translate_selected_zero)
{
	// First -- sanity check. If the max. rotation/translation is zero, nothing should be done.
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ref_ag = mol_read_pdb("ala.pdb");
	struct smp_prng *prng = smp_prng_create(42, PRNG_MT19937);
	struct mol_index_list lig_atom_list = _generate_atom_list_for_second_peptide_in_ala_pdb();

	smp_movers_randomly_rotate_translate_selected(ag, prng, lig_atom_list, 0, 0);
	_compare_ags(ag, ref_ag);
}
END_TEST

START_TEST(test_smp_movers_randomly_rotate_translate_selected_sanity)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ref_ag = mol_read_pdb("ala.pdb");
	struct mol_index_list lig_atom_list = _generate_atom_list_for_second_peptide_in_ala_pdb();

	static const double delta_translation = 0.2; // Angstrom
	static const double delta_rotation = 0.3; // Radians
	// The rough estimation of the diameter of the bounding sphere of the ligand
	static const double ligand_diameter = 8.0; // Angstrom
	// Maximum displacement of a single atom per smp_movers_randomly_rotate_translate_selected invocation
	const double max_possible_displacement = delta_translation + 0.5*ligand_diameter * delta_rotation;
	double max_achieved_displacement = 0;

	struct smp_prng *prng = smp_prng_create(42, PRNG_MT19937);
	for (size_t iter = 0; iter < 100; iter++) {
		// Restore coordinates
		for (size_t i = 0; i < ag->natoms; i++)
			MOL_VEC_COPY(ag->coords[i], ref_ag->coords[i]);
		smp_movers_randomly_rotate_translate_selected(ag, prng, lig_atom_list,
			delta_translation, delta_rotation);
		// First fifteen atoms should not be touched
		for (size_t i = 0; i < 15; i++) {
			ck_assert_double_eq(ag->coords[i].X, ref_ag->coords[i].X);
			ck_assert_double_eq(ag->coords[i].Y, ref_ag->coords[i].Y);
			ck_assert_double_eq(ag->coords[i].Z, ref_ag->coords[i].Z);
		}
		// The next fifteen atoms must be displaced, but not too much
		for (size_t i = 15; i < 30; i++) {
			struct mol_vector3 displacement;
			MOL_VEC_SUB(displacement, ag->coords[i], ref_ag->coords[i]);
			const double displacement_l = sqrt(MOL_VEC_SQ_NORM(displacement));

			// The chances of random displacement being epsilon-equal to zero are negligible
			ck_assert(displacement_l > tolerance_strict);
			// And check that the displacement is not too large
			ck_assert(displacement_l < max_possible_displacement);
			// Check that each coordinate separately is non-zero
			MOL_VEC_APPLY(displacement, fabs);
			ck_assert(displacement.X > tolerance_strict);
			ck_assert(displacement.Y > tolerance_strict);
			ck_assert(displacement.Z > tolerance_strict);
			// Update max. displacement, if necessary
			if (displacement_l > max_achieved_displacement)
				max_achieved_displacement = displacement_l;
		}
	}
	// We do 100 iterations with 15 atoms each. We should reach at least 70% of maximum.
	ck_assert(max_achieved_displacement > 0.7 * max_possible_displacement);
}
END_TEST

START_TEST(test_smp_movers_randomly_rotate_dihedrals_zero)
{
	// First -- sanity check. If the max. rotation is zero, nothing should be done.
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	struct mol_atom_group *ref_ag = mol_read_pdb("gly/gly.pdb");

	// We need to initialize .fixed fields in atomgroups
	mol_fixed_init(ag);

	struct smp_prng *prng = smp_prng_create(42, PRNG_MT19937);

	smp_movers_randomly_rotate_dihedrals(ag, rf, prng, 0);
	_compare_ags(ag, ref_ag);
}
END_TEST

START_TEST(test_smp_movers_randomly_rotate_dihedrals_sanity)
{
	// First -- sanity check. If the max. rotation is zero, nothing should be done.
	struct mol_atom_group *ag = mol_read_pdb("gly/gly.pdb");
	struct smp_rigforest *rf = smp_rigforest_create("gly/gly.txt", ag);
	struct mol_atom_group *ref_ag = mol_read_pdb("gly/gly.pdb");

	// We need to initialize .fixed fields in atomgroups
	mol_fixed_init(ag);

	static const double delta = 0.2; // Radians
	// Here, only N atom is being moved around. So, we just estimate the distance from it to dihedral axis
	static const double rotation_radius = 0.8; // 1.47 Angstrom * sin(60)
	// Maximum displacement of a single atom per smp_movers_randomly_rotate_translate_selected invocation
	const double max_possible_displacement = delta * rotation_radius;
	double max_achieved_displacement = 0;

	struct smp_prng *prng = smp_prng_create(42, PRNG_MT19937);
	for (size_t iter = 0; iter < 100; iter++) {
		// Restore coordinates
		for (size_t i = 0; i < ag->natoms; i++)
			MOL_VEC_COPY(ag->coords[i], ref_ag->coords[i]);
		smp_rigforest_copy_coords_from_atomgroup(rf, ag);
		for (size_t i = 0; i < rf->free_trees[0]->nnodes - 1; i++)
			rf->free_trees[0]->edge_thetas[i] = 0;

		smp_movers_randomly_rotate_dihedrals(ag, rf, prng, delta);
		// The first atom must be displaced, but not too much
		for (size_t i = 0; i < 1; i++) {
			struct mol_vector3 displacement;
			MOL_VEC_SUB(displacement, ag->coords[i], ref_ag->coords[i]);
			const double displacement_l = sqrt(MOL_VEC_SQ_NORM(displacement));

			// The chances of random displacement being epsilon-equal to zero are negligible
			ck_assert(displacement_l > tolerance_strict);
			// And check that the displacement is not too large
			ck_assert(displacement_l < max_possible_displacement);
			// Check that each coordinate separately is non-zero
			MOL_VEC_APPLY(displacement, fabs);
			ck_assert(displacement.X > tolerance_strict);
			ck_assert(displacement.Y > tolerance_strict);
			ck_assert(displacement.Z > tolerance_strict);
			// Update max. displacement, if necessary
			if (displacement_l > max_achieved_displacement)
				max_achieved_displacement = displacement_l;
		}
		// The remaining four atoms should not be touched
		for (size_t i = 1; i < 5; i++) {
			ck_assert_double_eq(ag->coords[i].X, ref_ag->coords[i].X);
			ck_assert_double_eq(ag->coords[i].Y, ref_ag->coords[i].Y);
			ck_assert_double_eq(ag->coords[i].Z, ref_ag->coords[i].Z);
		}

	}
	// We do 100 iterations with 1 atoms each. We should reach at least 70% of maximum.
	ck_assert(max_achieved_displacement > 0.7 * max_possible_displacement);
}
END_TEST


void setup_prms(void)
{
	params = mol_prms_read("libmol.params");
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("movers");
	TCase *tcase;

	tcase = tcase_create("test_smp_movers_slide_into_contact_backbone");
	tcase_add_checked_fixture(tcase, setup_prms, NULL);
	// Test when called with empty list
	tcase_add_test(tcase, test_smp_slide_into_contact_backbone_empty);
	// These two tests don't have any sidechains
	tcase_add_test(tcase, test_smp_slide_into_contact_backbone_push);
	tcase_add_test(tcase, test_smp_slide_into_contact_backbone_pull);
	// Test on real system with sidechains
	tcase_add_test(tcase, test_smp_slide_into_contact_backbone_pull_real_all);
	tcase_add_test(tcase, test_smp_slide_into_contact_backbone_pull_real_interface);
	suite_add_tcase(suite, tcase);

	tcase = tcase_create("test_smp_movers_randomly_rotate_translate_selected");
	tcase_add_test(tcase, test_smp_movers_randomly_rotate_translate_selected_zero);
	tcase_add_test(tcase, test_smp_movers_randomly_rotate_translate_selected_sanity);
	suite_add_tcase(suite, tcase);

	tcase = tcase_create("test_smp_movers_randomly_rotate_dihedrals");
	tcase_add_test(tcase, test_smp_movers_randomly_rotate_dihedrals_zero);
	tcase_add_test(tcase, test_smp_movers_randomly_rotate_dihedrals_sanity);
	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
