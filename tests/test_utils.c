#include <string.h>
#include <check.h>
#include <math.h>

#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/quaternion.h>
#include <src/transform.h>
#include <mol2/icharmm.h>
#include "../src/utils.h"

const double tolerance_strict = 1e-9;
const double tolerance_pdb = 1e-3;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert_msg(fabs(((val))-((ref))) < (tol), "%.12f != %.12f", val, ref);

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);

#define ck_assert_coordinate_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_pdb);


START_TEST(test_smp_str_trim_and_copy)
{
	char buf[10];
	memset(buf, 0xff, 10);
	smp_str_trim_and_copy(buf, " cat ", 10);
	ck_assert(!strcmp(buf, "cat"));
	memset(buf, 0xff, 10);
	smp_str_trim_and_copy(buf, "      cat    dog   ", 10);
	ck_assert(!strcmp(buf, "cat"));
	memset(buf, 0xff, 10);
	smp_str_trim_and_copy(buf, "      cat    dog   ", 3);
	ck_assert(!strcmp(buf, "ca"));
}
END_TEST


START_TEST(test_smp_scale_vdw_radius)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	mol_atom_group_read_geometry(ag, "ala.psf", "protein.prm", "protein.rtf");
	ck_assert(fabs(ag->vdw_radius[0] - 1.65) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[0] - 1.65) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius[22] - 1.55) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[22] - 1.36) < __DBL_EPSILON__);
	smp_scale_vdw_radius(ag, 0.1);
	ck_assert(fabs(ag->vdw_radius[0] - 0.165) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[0] - 1.65) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius[22] - 0.155) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[22] - 1.36) < __DBL_EPSILON__);
	smp_scale_vdw_radius(ag, 2);
	ck_assert(fabs(ag->vdw_radius[0] - 0.33) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[0] - 1.65) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius[22] - 0.31) < __DBL_EPSILON__);
	ck_assert(fabs(ag->vdw_radius03[22] - 1.36) < __DBL_EPSILON__);
}
END_TEST


START_TEST(test_smp_atom_group_init_backbone_by_name)
{
	struct mol_atom_group *ag = mol_read_pdb("1bgs-beta.pdb");
	smp_atom_group_init_backbone_by_name(ag);
	size_t sum = 0;
	static const size_t sum_ref = 122;
	static const bool first_ref[] = {1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0};
	for (size_t i  = 0; i < ag->natoms; i++) {
		if (i < sizeof(first_ref)/sizeof(bool)) {
			ck_assert_int_eq(ag->backbone[i], first_ref[i]);
		}
		sum += ag->backbone[i];
	}
	ck_assert_int_eq(sum, sum_ref);
}
END_TEST


static struct mol_index_list _gen_index_list()
{
	struct mol_index_list atom_list;
	atom_list.size = 5;
	atom_list.members = calloc(atom_list.size, sizeof(size_t));
	atom_list.members[0] = 0;
	atom_list.members[1] = 4;
	atom_list.members[2] = 8;
	atom_list.members[3] = 9;
	atom_list.members[4] = 29;
	return atom_list;
}


START_TEST(test_smp_atom_list_translate)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ag_ref = mol_read_pdb("ala_smp_translate_selected_atoms.pdb");
	struct mol_index_list atom_list = _gen_index_list();
	const struct mol_vector3 v = {.X = 1, .Y = 2, .Z = -3};
	smp_atom_list_translate(ag, &v, atom_list);

	ck_assert_int_eq(ag->natoms, ag_ref->natoms);
	ck_assert_int_eq(ag->natoms, 30);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_ref->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_ref->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_ref->coords[i].Z);
	}
}
END_TEST


START_TEST(test_smp_atom_list_rotate_1)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ag_ref = mol_read_pdb("ala_smp_rotate_selected_atoms_1.pdb");
	struct mol_index_list atom_list = _gen_index_list();

	const struct mol_vector3 center = {.X = 0, .Y = 0, .Z = 0};
	// Rotate around OZ by 30 degrees counter-clockwise
	const struct mol_quaternion q = {.W = 0.9659258262890683, .X = 0, .Y = 0, .Z = 0.25881904510252074};
	smp_atom_list_rotate(ag, &q, &center, atom_list);

	ck_assert_int_eq(ag->natoms, ag_ref->natoms);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_ref->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_ref->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_ref->coords[i].Z);
	}
}
END_TEST


START_TEST(test_smp_atom_list_rotate_2)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ag_ref = mol_read_pdb("ala_smp_rotate_selected_atoms_2.pdb");
	struct mol_index_list atom_list = _gen_index_list();

	const struct mol_vector3 center = {.X = 1, .Y = 2, .Z = 3};
	// Rotate around {2/3; 1/3; -2/3} by 30 degrees counter-clockwise
	const struct mol_quaternion q = {.W = 0.9659258262890683, .X = 0.17254603006834715, .Y = 0.08627301503417358, .Z = -0.17254603006834715};
	smp_atom_list_rotate(ag, &q, &center, atom_list);

	ck_assert_int_eq(ag->natoms, ag_ref->natoms);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_ref->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_ref->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_ref->coords[i].Z);
	}
}
END_TEST


START_TEST(test_smp_atom_list_centroid)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_index_list atom_list = _gen_index_list();

	struct mol_vector3 c = smp_atom_list_centroid(ag, atom_list);
	ck_assert_double_eq(c.X,  3.3148);
	ck_assert_double_eq(c.Y,  1.0648);
	ck_assert_double_eq(c.Z, -1.8816);
}
END_TEST


START_TEST(test_smp_create_interface_residues_list)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	const size_t first_ligand_atom = 15, last_ligand_atom = 29;

	struct mol_index_list interface_residues;

	// All residues in cutoff
	smp_create_interface_residues_list(&interface_residues, ag,
		first_ligand_atom, last_ligand_atom,
		99999, true);

	ck_assert_int_eq(interface_residues.size, 4);
	ck_assert_int_eq(interface_residues.members[0], 0);
	ck_assert_int_eq(interface_residues.members[1], 1);
	ck_assert_int_eq(interface_residues.members[2], 2);
	ck_assert_int_eq(interface_residues.members[3], 3);

	// The list is already allocated, no need to allocate it again
	smp_create_interface_residues_list(&interface_residues, ag,
	        first_ligand_atom, last_ligand_atom,
	        4, false);
	ck_assert_int_eq(interface_residues.size, 2);
	ck_assert_int_eq(interface_residues.members[0], 1);
	ck_assert_int_eq(interface_residues.members[1], 2);

	// Set the cutoff so small nothing falls in it
	smp_create_interface_residues_list(&interface_residues, ag,
	        first_ligand_atom, last_ligand_atom,
	        0.0001, false);
	ck_assert_int_eq(interface_residues.size, 0);

	// Should also include all residues. Also, check that the previous calls with alloc=false hadn't shrunk the array
	smp_create_interface_residues_list(&interface_residues, ag,
	        first_ligand_atom, last_ligand_atom,
	        15, false);
	ck_assert_int_eq(interface_residues.size, 4);
	ck_assert_int_eq(interface_residues.members[0], 0);
	ck_assert_int_eq(interface_residues.members[1], 1);
	ck_assert_int_eq(interface_residues.members[2], 2);
	ck_assert_int_eq(interface_residues.members[3], 3);
}
END_TEST


START_TEST(test_smp_get_atom_list_by_element)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_index_list ret;

	smp_get_atom_list_by_element(&ret, ag, "H", true);
	ck_assert_int_eq(ret.size, 8);
	ck_assert_int_eq(ret.members[0], 1);
	ck_assert_int_eq(ret.members[1], 2);
	ck_assert_int_eq(ret.members[2], 3);
	ck_assert_int_eq(ret.members[3], 9);
	ck_assert_int_eq(ret.members[4], 16);
	ck_assert_int_eq(ret.members[5], 17);
	ck_assert_int_eq(ret.members[6], 18);
	ck_assert_int_eq(ret.members[7], 24);

	smp_get_atom_list_by_element(&ret, ag, "N", false);
	ck_assert_int_eq(ret.size, 4);
	ck_assert_int_eq(ret.members[0], 0);
	ck_assert_int_eq(ret.members[1], 8);
	ck_assert_int_eq(ret.members[2], 15);
	ck_assert_int_eq(ret.members[3], 23);

}
END_TEST


START_TEST(test_smp_freeze_all_unfreeze_from_list)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	mol_fixed_init(ag);

	struct mol_index_list atom_list = _gen_index_list();

	smp_freeze_all_unfreeze_from_list(ag, atom_list);
	for (size_t i = 0; i < 30; i++) {
		bool should_be_unfrozen = (i == 0) || (i == 4) || (i == 8) || (i == 9) || (i == 29);
		ck_assert(ag->fixed[i] == !should_be_unfrozen);
	}
}
END_TEST


START_TEST(test_coords_to_array)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");
	struct mol_vector3 *v = calloc(ag->natoms, sizeof(struct mol_vector3));
	smp_coords_to_array(v, ag, NULL);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(v[i].X, ag->coords[i].X);
		ck_assert_coordinate_eq(v[i].Y, ag->coords[i].Y);
		ck_assert_coordinate_eq(v[i].Z, ag->coords[i].Z);
	}

	memset(v, 0, ag->natoms * sizeof(struct mol_vector3));
	struct mol_index_list idx;
	idx.size = 2;
	idx.members = calloc(2, sizeof(size_t));
	idx.members[0] = 2;
	idx.members[1] = 0;
	smp_coords_to_array(v, ag, &idx);
	ck_assert_coordinate_eq(v[0].X, ag->coords[2].X);
	ck_assert_coordinate_eq(v[0].Y, ag->coords[2].Y);
	ck_assert_coordinate_eq(v[0].Z, ag->coords[2].Z);
	ck_assert_coordinate_eq(v[1].X, ag->coords[0].X);
	ck_assert_coordinate_eq(v[1].Y, ag->coords[0].Y);
	ck_assert_coordinate_eq(v[1].Z, ag->coords[0].Z);
	for (size_t i = 2; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(v[i].X, 0);
		ck_assert_coordinate_eq(v[i].Y, 0);
		ck_assert_coordinate_eq(v[i].Z, 0);
	}

	memset(v, 0, ag->natoms * sizeof(struct mol_vector3));
	idx.size = 0;
	smp_coords_to_array(v, ag, &idx);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(v[i].X, 0);
		ck_assert_coordinate_eq(v[i].Y, 0);
		ck_assert_coordinate_eq(v[i].Z, 0);
	}
}
END_TEST


START_TEST(test_coords_from_array)
{
	const struct mol_atom_group *ag_ref = mol_read_pdb("ala.pdb");
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");

	struct mol_vector3 *v = calloc(ag->natoms, sizeof(struct mol_vector3));
	smp_coords_to_array(v, ag_ref, NULL);

	memset(ag->coords, 0, ag->natoms * sizeof(struct mol_vector3));
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, 0);
		ck_assert_coordinate_eq(ag->coords[i].Y, 0);
		ck_assert_coordinate_eq(ag->coords[i].Z, 0);
	}

	smp_coords_from_array(ag, v, NULL);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, ag_ref->coords[i].X);
		ck_assert_coordinate_eq(ag->coords[i].Y, ag_ref->coords[i].Y);
		ck_assert_coordinate_eq(ag->coords[i].Z, ag_ref->coords[i].Z);
	}

	memset(ag->coords, 0, ag->natoms * sizeof(struct mol_vector3));
	struct mol_index_list idx;
	idx.size = 2;
	idx.members = calloc(2, sizeof(size_t));
	idx.members[0] = 2;
	idx.members[1] = 0;
	smp_coords_from_array(ag, v, &idx);
	ck_assert_coordinate_eq(ag->coords[0].X, ag_ref->coords[1].X);
	ck_assert_coordinate_eq(ag->coords[0].Y, ag_ref->coords[1].Y);
	ck_assert_coordinate_eq(ag->coords[0].Z, ag_ref->coords[1].Z);
	ck_assert_coordinate_eq(ag->coords[2].X, ag_ref->coords[0].X);
	ck_assert_coordinate_eq(ag->coords[2].Y, ag_ref->coords[0].Y);
	ck_assert_coordinate_eq(ag->coords[2].Z, ag_ref->coords[0].Z);
	for (size_t i = 1; i < ag->natoms; i++) {
		if (i == 2) continue;
		ck_assert_coordinate_eq(ag->coords[i].X, 0);
		ck_assert_coordinate_eq(ag->coords[i].Y, 0);
		ck_assert_coordinate_eq(ag->coords[i].Z, 0);
	}

	memset(ag->coords, 0, ag->natoms * sizeof(struct mol_vector3));
	idx.size = 0;
	smp_coords_from_array(ag, v, &idx);
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_coordinate_eq(ag->coords[i].X, 0);
		ck_assert_coordinate_eq(ag->coords[i].Y, 0);
		ck_assert_coordinate_eq(ag->coords[i].Z, 0);
	}
}
END_TEST


START_TEST(test_ag_has_nan_coordinates)
{
	struct mol_atom_group *ag = mol_read_pdb("ala.pdb");

	ck_assert(!smp_atom_group_has_nan_coordinates(ag));

	ag->coords[4].X = sqrt(-1);
	ck_assert(smp_atom_group_has_nan_coordinates(ag));

	ag->coords[4].X = log(0);
	ck_assert(smp_atom_group_has_nan_coordinates(ag));

	ag->coords[4].X = 1;
	ck_assert(!smp_atom_group_has_nan_coordinates(ag));
}
END_TEST

Suite *featurex_suite(void)
{
	Suite *suite = suite_create("utils");
	TCase *tcase = tcase_create("test_utils");
	tcase_add_test(tcase, test_smp_str_trim_and_copy);
	tcase_add_test(tcase, test_smp_scale_vdw_radius);
	tcase_add_test(tcase, test_smp_atom_group_init_backbone_by_name);
	tcase_add_test(tcase, test_smp_atom_list_translate);
	tcase_add_test(tcase, test_smp_atom_list_rotate_1);
	tcase_add_test(tcase, test_smp_atom_list_rotate_2);
	tcase_add_test(tcase, test_smp_atom_list_centroid);
	tcase_add_test(tcase, test_smp_create_interface_residues_list);
	tcase_add_test(tcase, test_smp_get_atom_list_by_element);
	tcase_add_test(tcase, test_smp_freeze_all_unfreeze_from_list);
	tcase_add_test(tcase, test_coords_to_array);
	tcase_add_test(tcase, test_coords_from_array);
	tcase_add_test(tcase, test_ag_has_nan_coordinates);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = featurex_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
