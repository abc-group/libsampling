#include <math.h>

#include "hbond.h"
#include "utils.h"
#include "hybridization.h"
#include "hbond_polynomials.h"
#include "hbond_splines.h"

static const double HB_ENG_MAX = 0.0; ///< Maximal allowed energy of h-bond. Larger values will be set to zero.

static const double MIN_AH = 1.4; ///< Min. distance between acceptor and hydrogen (Angstroms).
static const double MAX_AH = 3.0; ///< Max. distance between acceptor and hydrogen (Angstroms).

static const double MIN_HD = 0.8; ///< Min. distance between donor and hydrogen (Angstroms).
static const double MAX_HD = 1.25; ///< Max. distance between donor and hydrogen (Angstroms).

static const double MIN_XH = 0.0; ///< Min. cosine of angle Psi (acceptor base - acceptor - hydrogen).
static const double MAX_XH = 1.0; ///< Max. cosine of angle Psi (acceptor base - acceptor - hydrogen).

static const double MIN_XD = 0.0; ///< Min. cosine of angle Theta (acceptor - hydrogen - donor).
static const double MAX_XD = 1.0; ///< Min. cosine of angle Theta (acceptor - hydrogen - donor).

/** Types of donor atoms, depending on their place in molecule structure */
enum _donor_type {
	hbdon_NO = 0,  ///< Unknown type of donor atom.
	hbdon_BB,      ///< Part of backbone.
	hbdon_SC       ///< Part of sidechain.
};

/** Types of acceptor atoms, depending on their place in molecule structure */
enum _acceptor_type {
	hbacc_NO = 0,  ///< Unknown type of acceptor.
	hbacc_BB,      ///< Part of backbone.
	hbacc_SP2,     ///< Part of sidechain, with sp2 hybridization.
	hbacc_SP3,     ///< Part of sidechain, with sp3 hybridization.
	hbacc_RING     ///< Part of aromatic ring in sidechain.
};

/** Types of hydroben bond */
enum _bond_type {
	hbe_NONE = 0,
	hbe_BBTURN,    ///< Between two backbone atoms, turn-like secondary structure.
	hbe_BBHELIX,   ///< Between two backbone atoms, helix-like secondary structure.
	hbe_BBOTHER,   ///< Between two backbone atoms, other secondary structure.
	hbe_SP2B,      ///< Donor from backbone, acceptor from sidechain and has sp2 hybridization.
	hbe_SP3B,      ///< Donor from backbone, acceptor from sidechain and has sp3 hybridization.
	hbe_RINGB,     ///< Donor from backbone, acceptor from aromatic ring from sidechain.
	hbe_BSC,       ///< Donor from sidechain, acceptor from backbone.
	hbe_SP2SC,     ///< Donor from sidechain, acceptor from sidechain and has sp2 hybridization.
	hbe_SP3SC,     ///< Donor from sidechain, acceptor from sidechain and has sp3 hybridization.
	hbe_RINGSC     ///< Donor from sidechain, acceptor from aromatic ring from sidechain.
};

/**
 * Initialize helper metadata for storing second acceptor base (if necessary), and update acceptor bases.
 */
static void _set_hbond_acceptor_bases(struct mol_atom_group *ag)
{
	size_t *base2 = calloc(ag->natoms, sizeof(size_t));

	if (mol_atom_group_has_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY)) {
		free(mol_atom_group_fetch_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY));
		mol_atom_group_delete_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY);
	}
	mol_atom_group_add_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY, base2);

	for (size_t i = 0; i < ag->natoms; i++) {
		base2[i] = SMP_HBOND_BASE2_MISSING;
		if (MOL_ATOM_HBOND_PROP_IS(ag, i, HBOND_ACCEPTOR)) {
			int heavy_acceptor_base_count = 0;
			for (size_t j = 0; j < ag->bond_lists[i].size; j++) {
				const size_t acc_bond_idx = ag->bond_lists[i].members[j];
				const struct mol_bond acc_bond = ag->bonds[acc_bond_idx];
				size_t base_atomi = acc_bond.ai;
				if (base_atomi == i)
					base_atomi = acc_bond.aj;

				if (strcmp(ag->element[i], "H") == 0)
					continue;

				heavy_acceptor_base_count++;
				if (heavy_acceptor_base_count == 1) // take the first one as the acceptor base
					ag->hbond_base_atom_id[i] = base_atomi;
				else {
					base2[i] = base_atomi;
				}
			}
		}
	}
}

static bool _are_covalently_linked(const struct mol_atom_group *ag, const size_t ai, const size_t aj)
{
	for (size_t j = 0; j < ag->bond_lists[ai].size; j++) {
		const size_t bond_idx = ag->bond_lists[ai].members[j];
		const struct mol_bond bond = ag->bonds[bond_idx];
		size_t neighbour = bond.ai;
		if (neighbour == ai)
			neighbour = bond.aj;
		if (neighbour == aj)
			return true;
	}
	return false;
}

/**
 * Check that all atoms have all necessary base atoms assigned, and these base atoms are covalently linked to corresponding H or donor.
 * \return True if all bases are assigned properly, false on any inconsistency.
 */
static bool _check_bases(const struct mol_atom_group *ag, FILE* log)
{
	const size_t *base2_arr = mol_atom_group_fetch_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY);
	const enum smp_hybridization_state *hybridization_states = smp_hybridization_states_fetch(ag);
	bool have_errors = false;

	for (size_t i = 0; i < ag->natoms; i++) {
		if (MOL_ATOM_HBOND_PROP_IS(ag, i, HBOND_ACCEPTOR)) {
			const size_t base1 = ag->hbond_base_atom_id[i];
			const size_t base2 = base2_arr[i];
			const enum smp_hybridization_state hyb = hybridization_states[i];
			bool should_have_base2 = (hyb == RING_HYBRID || hyb == SP3_HYBRID);
			bool base_1_ok = _are_covalently_linked(ag, i, base1);
			// If base2 should not exist and is missing it's okay, no need to check it
			bool base_2_ok = false;
			if (should_have_base2) {
				if (base2 == SMP_HBOND_BASE2_MISSING) {
					if (log)
						FPRINTF_ERROR(log, "Base2 not set (but should be) for acceptor %zu", i);
					have_errors = true;
				} else {
					base_2_ok = _are_covalently_linked(ag, i, base2);
				}
			} else { // !should_have_base2
				if (base2 == SMP_HBOND_BASE2_MISSING) {
					base_2_ok = true;
				} else {
					if (log)
						FPRINTF_ERROR(log, "Base2 is set (but should not be) for acceptor %zu", i);
					have_errors = true;
				}
			}

			if (!base_1_ok) {
				if (log)
					FPRINTF_ERROR(log, "Wrong base of acceptor %zd", i);
				have_errors = true;
			}
			if (!base_2_ok) {
				if (log)
					FPRINTF_ERROR(log, "Wrong base2 of acceptor %zd", i);
				have_errors = true;
			}
		}
		if (MOL_ATOM_HBOND_PROP_IS(ag, i, DONATABLE_HYDROGEN)) {
			const size_t base = ag->hbond_base_atom_id[i];
			bool base_ok = _are_covalently_linked(ag, i, base);
			if (!base_ok) {
				if (log)
					FPRINTF_ERROR(log, "Wrong base of hydrogen %zd", i);
				have_errors = true;
			}
			if (! MOL_ATOM_HBOND_PROP_IS(ag, base, HBOND_DONOR)) {
				if (log)
					FPRINTF_ERROR(log, "Base of hydrogen %zd - atom %zd - is not marked as donor", i, base);
				have_errors = true;
			}
		}
	}
	return !have_errors;
}

/**
 * When both donor and acceptor are in backbone, return the type of their bond based on likely secondary structure.
 * \param ag Atomgroup.
 * \param don Donor atom number.
 * \param acc Acceptor atom number.
 * \return Type of h-bond.
 */
static inline enum _bond_type _get_hbe_type_BB(const struct mol_atom_group *ag, const size_t don, const size_t acc)
{
	if (ag->residue_id[don].chain != ag->residue_id[acc].chain) {
		return hbe_BBOTHER;
	} else {
		const int delta = abs(ag->residue_id[don].residue_seq - ag->residue_id[acc].residue_seq);
		switch (delta) {
			case 0:
				return hbe_NONE;
			case 1:
			case 2:
			case 3:
				return hbe_BBTURN;
			case 4:
				return hbe_BBHELIX;
			default:
				return hbe_BBOTHER;
		}
	}
}

/**
 * Determine type (\ref _donor_type) of donor atom.
 */
static inline enum _donor_type _get_donor_chem_type(const struct mol_atom_group *ag, const size_t don)
{
	if (ag->backbone[don])
		return hbdon_BB;
	else
		return hbdon_SC;
}

/**
 * Determine type (\ref _acceptor_type) of acceptor atom.
 */
static inline enum _acceptor_type _get_acceptor_chem_type(
		const struct mol_atom_group *ag,
		const size_t acc,
		const enum smp_hybridization_state *hybridization_states)
{
	if (ag->backbone[acc])
		return hbacc_BB;
	else {
		switch (hybridization_states[acc]) {
			case SP2_HYBRID:
				return hbacc_SP2;
			case SP3_HYBRID:
				return hbacc_SP3;
			case RING_HYBRID:
				return hbacc_RING;
			default:
				return hbacc_NO;
		}
	}
}

/**
 * Get type of hbond between hydrogen and acceptor.
 * \param ag Atomgroup.
 * \param hydro Hydrogen atom number.
 * \param acc Acceptor atom number.
 * \return Type of h-bond.
 */
static enum _bond_type _get_hbe_type(
		const struct mol_atom_group *ag,
		const size_t hydro,
		const size_t acc,
		const enum smp_hybridization_state *hybridization_states)
{
	if (!MOL_ATOM_HBOND_PROP_IS(ag, hydro, DONATABLE_HYDROGEN) || !MOL_ATOM_HBOND_PROP_IS(ag, acc, HBOND_ACCEPTOR))
		return hbe_NONE;

	size_t don = ag->hbond_base_atom_id[hydro];

	enum _donor_type don_type = _get_donor_chem_type(ag, don);
	enum _acceptor_type acc_type = _get_acceptor_chem_type(ag, acc, hybridization_states);

	if ((don_type == hbdon_BB) && (acc_type == hbacc_BB))
		return _get_hbe_type_BB(ag, don, acc);
	else if (acc_type == hbacc_BB)
		return hbe_BSC;
	else if (don_type == hbdon_BB) {
		switch (acc_type) {
			case hbacc_SP2:
				return hbe_SP2B;
			case hbacc_SP3:
				return hbe_SP3B;
			case hbacc_RING:
				return hbe_RINGB;
			default:
				return hbe_NONE;
		}
	} else {
		switch (acc_type) {
			case hbacc_SP2:
				return hbe_SP2SC;
			case hbacc_SP3:
				return hbe_SP3SC;
			case hbacc_RING:
				return hbe_RINGSC;
			default:
				return hbe_NONE;
		}
	}
}

/**
 * Return true if the hydrogen bond of type \p hbe should have base2.
 */
static bool _hbond_has_base2(const enum _bond_type hbe)
{
	return (hbe == hbe_RINGSC) || (hbe == hbe_RINGB) || (hbe == hbe_SP3SC) || (hbe == hbe_SP3B);
}

/**
 * Create a unit vector pointing from the hydrogen towards the donor.
 * \param[in] ag Atomgroup.
 * \param[in] hydro Hydrogen atom id.
 * \param[out] hd_unit_vector Unit vector from hydrogen towards donor.
 * \param[out] hd_distance Distance between hydrogen and donor.
 * \return True on success, false on any failure.
 */
static bool _create_hydrogen_to_donor_unit_vector(
		struct mol_vector3 *hd_unit_vector,
		double *hd_distance,
		const struct mol_atom_group *ag,
		const size_t hydro)
{
	size_t donor = ag->hbond_base_atom_id[hydro];
	MOL_VEC_SUB(*hd_unit_vector, ag->coords[donor], ag->coords[hydro]);

	*hd_distance = MOL_VEC_SQ_NORM(*hd_unit_vector);

	if (*hd_distance <= 0) {
		PRINTF_ERROR("Failed to normalize the hydrogen to donor vector!");
		return false;
	}

	*hd_distance = sqrt(*hd_distance);

	MOL_VEC_DIV_SCALAR(*hd_unit_vector, *hd_unit_vector, *hd_distance);
	return true;
}

/**
 * Create a unit vector pointing from the acceptor base atom (or virtual atom) towards the acceptor.
 * \param[out] ba_unit_vector Unit vector from base to acceptor.
 * \param[out] ba_distance Distance between hydrogen and donor.
 * \param[in] ag Atomgroup.
 * \param[in] acc Acceptor atom id.
 * \param[in] hbe_type Type of hbond. Necessary to determine which base atom to use.
 * \param[in] base2_arr Pointer to the arrays of the base2 indexes.
 * \return True on success, false on any failure.
 */
static bool _create_base_to_acceptor_unit_vector(
		struct mol_vector3 *ba_unit_vector,
		double *ba_distance,
		const struct mol_atom_group *ag,
		const size_t acc,
		const enum _bond_type hbe_type,
		const size_t *base2_arr)
{
	size_t base = ag->hbond_base_atom_id[acc];
	size_t base2;
	struct mol_vector3 virtual_base;

	if (_hbond_has_base2(hbe_type)) {
		base2 = base2_arr[acc];
		if (base2 == SMP_HBOND_BASE2_MISSING) {
			return false;
		}
	}

	switch (hbe_type) {
		case hbe_RINGSC:
		case hbe_RINGB: // RING: virtual_base is the midpoint between base and base2
			MOL_VEC_ADD(virtual_base, ag->coords[base], ag->coords[base2]);
			MOL_VEC_MULT_SCALAR(virtual_base, virtual_base, 0.5);
			break;
		case hbe_SP3SC:
		case hbe_SP3B: // SP3: virtual_base is base2
			MOL_VEC_COPY(virtual_base, ag->coords[base2]);
			break;
		default: // SP2: virtual_base is base
			MOL_VEC_COPY(virtual_base, ag->coords[base]);
			break;
	}

	MOL_VEC_SUB(*ba_unit_vector, ag->coords[acc], virtual_base);

	const double ba_distance2 = MOL_VEC_SQ_NORM(*ba_unit_vector);

	if (ba_distance2 <= 0) {
		PRINTF_ERROR("Failed to normalize the base to acceptor vector!");
		return false;
	}

	*ba_distance = sqrt(ba_distance2);
	MOL_VEC_DIV_SCALAR(*ba_unit_vector, *ba_unit_vector, *ba_distance);

	return true;
}

/**
 * Return \c true if \p hbe is any backbone-to-backbone type.
 */
static inline bool _hbond_is_bb(const enum _bond_type hbe)
{
	return ((hbe == hbe_BBTURN) || (hbe == hbe_BBHELIX) || (hbe == hbe_BBOTHER));
}

/**
 * Compute energies and gradients of h-bond based on its parameters.
 *
 * \param[in] hbe Type of hbond.
 * \param[in] ah_distance Distance between acceptor and hydrogen.
 * \param[in] xd_cosine Cosine of A-H-D angle.
 * \param[in] xh_cosine Cosine of B-A-H angle.
 * \param[out] energy Energy of h-bond.
 * \param[out] de_dr Derivative of energy w.r.t. A-H distance.
 * \param[out] de_dxd Derivative of energy w.r.t. A-H-D angle.
 * \param[out] de_dxh Derivative of energy w.r.t. B-A-H angle.
 * \return True on success, false on failure.
 */
static bool _hbond_energy_computation(
		double *energy,
		double *de_dr,
		double *de_dxd,
		double *de_dxh,
		const enum _bond_type hbe,
		const double ah_distance,
		const double xd_cosine,
		const double xh_cosine)
{
	*energy = HB_ENG_MAX + 1.0f;
	*de_dr = 0.0;
	*de_dxd = 0.0;
	*de_dxh = 0.0;

	if ((fabs(xd_cosine) > 1.0) || (fabs(xh_cosine) > 1.0)) {
		PRINTF_ERROR("Invalid angle value in hbond_energy_computation: xh_cosine = %lf, xd_cosine = %lf", xh_cosine, xd_cosine);
		return false;
	}

	if ((ah_distance > MAX_AH) || (ah_distance < MIN_AH) ||
		(xh_cosine < MIN_XH) || (xh_cosine > MAX_XH) ||
		(xd_cosine < MIN_XD) || (xd_cosine > MAX_XD))
		return false;

	double dAHdis = ah_distance;
	double dxD = xd_cosine;
	double dxH = xh_cosine;

	// F - fade interval, dF - its derivative, P - polynomial, dP - its derivative
	// S - short-range, L - long range, if absent - we don't differentiate short- and long-range.
	// r - AH distance, xd_cosine - cosine of A-H-D angle, xh_cosine - cosine of B-A-H angle.
	// Fade interval values
	double  FSr = 0.0,  FLr = 0.0,  FxD = 0.0,  FxH = 0.0;
	// Fade interval derivatives
	double dFSr = 0.0, dFLr = 0.0, dFxD = 0.0, dFxH = 0.0;
	// Polynomial values
	double  Pr = 0.0,  PSxD = 0.0,  PSxH = 0.0,  PLxD = 0.0,  PLxH = 0.0;
	// Polynomial derivatives
	double dPr = 0.0, dPSxD = 0.0, dPSxH = 0.0, dPLxD = 0.0, dPLxH = 0.0;

	// Calculate fade functions
	_bspline_fade_xD(xd_cosine, &FxD, &dFxD);
	_bspline_fade_xH(xh_cosine, &FxH, &dFxH);

	if (_hbond_is_bb(hbe)) {
		_bspline_fade_rBB(ah_distance, &FSr, &dFSr);
	} else {
		_bspline_fade_rshort(ah_distance, &FSr, &dFSr);
		_bspline_fade_rlong(ah_distance, &FLr, &dFLr);
	}

	switch (hbe) {
		case hbe_NONE: break;

		case hbe_BBHELIX: 	 // hbstat = 1, polynomials 1,5,11
			_poly_AH_BBHelix(dAHdis, &Pr, &dPr);
			_poly_xD_BBHelix(dxD, &PSxD, &dPSxD);
			_poly_xH_BBHelix(dxH, &PSxH, &dPSxH);
			break;


		case hbe_BBOTHER:	 // hbstat = 2, polynomials 2,6,11
			_poly_AH_BBOther(dAHdis, &Pr, &dPr);
			_poly_xD_BBOther(dxD, &PSxD, &dPSxD);
			_poly_xH_BBOther(dxH, &PSxH, &dPSxH);
			break;

		case hbe_BBTURN:	 // hbstat = 3, polynomials 3,7,13,8,14
		case hbe_BSC:
		case hbe_SP2B:
		case hbe_SP2SC:
			_poly_AH_SP2(dAHdis, &Pr, &dPr);
			_poly_xD_SP2short(dxD, &PSxD, &dPSxD);
			_poly_xH_SP2short(dxH, &PSxH, &dPSxH);
			_poly_xD_SP2long(dxD, &PLxD, &dPLxD);
			_poly_xH_SP2long(dxH, &PLxH, &dPLxH);
			break;

		case hbe_SP3B: 		// hbstat = 4, polynomials 4,9,15,10,15
		case hbe_SP3SC:
			_poly_AH_SP3(dAHdis, &Pr, &dPr);
			_poly_xD_SP3short(dxD, &PSxD, &dPSxD);
			_poly_xH_SP3(dxH, &PSxH, &dPSxH);
			_poly_xD_SP3long(dxD, &PLxD, &dPLxD);
			PLxH = PSxH; dPLxH = dPSxH;
			break;

		case hbe_RINGB: 	// hbstat = 5, polynomials 3,7,16,8,16
		case hbe_RINGSC:
			_poly_AH_SP2(dAHdis, &Pr, &dPr);
			_poly_xD_SP2short(dxD, &PSxD, &dPSxD);
			_poly_xH_Ring(dxH, &PSxH, &dPSxH);
			_poly_xD_SP2long(dxD, &PLxD, &dPLxD);
			PLxH = PSxH; dPLxH = dPSxH;
			break;
	}


	*energy = Pr * FxD * FxH +  FSr * ( PSxD * FxH + FxD * PSxH ) + FLr * ( PLxD * FxH + FxD * PLxH );
	*de_dr =  dPr * FxD * FxH + dFSr * ( PSxD * FxH + FxD * PSxH ) + dFLr * ( PLxD * FxH + FxD * PLxH );
	*de_dxd = dFxD * ( Pr * FxH + FLr * PLxH + FSr * PSxH ) + FxH * ( FSr * dPSxD + FLr * dPLxD );
	*de_dxh = dFxH * ( Pr * FxD + FLr * PLxD + FSr * PSxD ) + FxD * ( FSr * dPSxH + FLr * dPLxH );

	return true;
}

/**
 * Calculate h-bond energy and gradient between pair of atoms given its type
 * \param[in] hydro Index of hydrogen atom in \p ag.
 * \param[in] acc Index of acceptor atom in \p ag.
 * \param[in] hbe Type of h-bond.
 * \param[in] base2_arr Array of secondary base atoms, typically retrieved from \c hbond_base2 metadata.
 * \param[in] weight Energy weighting term.
 * \param[in,out] ag Atomgroup. Gradients are updated.
 * \param[out] energy Calculated h-bond energy.
 * \return True on success, false on failure.
 */
static bool _hbond_energy_and_gradient_computation(
		struct mol_atom_group *ag,
		double *energy,
		const size_t hydro,
		const size_t acc,
		const enum _bond_type hbe,
		const size_t *base2_arr,
		const double weight)
{
	struct mol_vector3 ah_unit_vector, hd_unit_vector, ba_unit_vector;
	double hd_distance, ba_distance;

	size_t don = ag->hbond_base_atom_id[hydro];

	if (!_create_hydrogen_to_donor_unit_vector(&hd_unit_vector, &hd_distance, ag, hydro))
		return false;

	if ((hd_distance < MIN_HD) || (hd_distance > MAX_HD))
	{
		*energy = 0;
		return true;
	}

	if (!_create_base_to_acceptor_unit_vector(&ba_unit_vector, &ba_distance, ag, acc, hbe, base2_arr))
		return false;

	MOL_VEC_SUB(ah_unit_vector, ag->coords[hydro], ag->coords[acc]);

	const double ah_distance2 = MOL_VEC_SQ_NORM(ah_unit_vector);

	if (ah_distance2 <= 0) {
		PRINTF_ERROR("Overlapping acceptor and hydrogen!");
		return false;
	}
	const double ah_distance = sqrt(ah_distance2);

	MOL_VEC_DIV_SCALAR(ah_unit_vector, ah_unit_vector, ah_distance);

	const double xd_cosine = MOL_VEC_DOT_PROD(ah_unit_vector, hd_unit_vector);
	const double xh_cosine = MOL_VEC_DOT_PROD(ba_unit_vector, ah_unit_vector);
	double de_dr, de_dxd, de_dxh;
	double energy_unweighted;

	if (!_hbond_energy_computation(&energy_unweighted, &de_dr, &de_dxd, &de_dxh, hbe, ah_distance, xd_cosine,
		xh_cosine)) {
		return false;
	}

	*energy = weight * energy_unweighted;

	if (*energy >= HB_ENG_MAX) {
		*energy = 0;
		return true;
	}


	// Computing gradients

	size_t base = ag->hbond_base_atom_id[acc];
	size_t base2;

	if (_hbond_has_base2(hbe)) {
		base2 = base2_arr[acc];
		if (base2 == SMP_HBOND_BASE2_MISSING) {
			PRINTF_ERROR("Acceptor base 2 missing!");
			return false;
		}
	}

	struct mol_vector3 u, v;
	MOL_VEC_MULT_SCALAR(u, ah_unit_vector, de_dr);
	MOL_VEC_MULT_SCALAR(u, u, weight);

	MOL_VEC_ADD(ag->gradients[acc], ag->gradients[acc], u);
	MOL_VEC_SUB(ag->gradients[hydro], ag->gradients[hydro], u);

	MOL_VEC_MULT_SCALAR(u, ah_unit_vector, xd_cosine);
	MOL_VEC_SUB(u, u, hd_unit_vector);
	MOL_VEC_MULT_SCALAR(u, u, -de_dxd * weight / ah_distance);
	MOL_VEC_MULT_SCALAR(v, hd_unit_vector, xd_cosine);
	MOL_VEC_SUB(v, ah_unit_vector, v);
	MOL_VEC_MULT_SCALAR(v, v, -de_dxd * weight / hd_distance);

	MOL_VEC_SUB(ag->gradients[hydro], ag->gradients[hydro], u);
	MOL_VEC_ADD(ag->gradients[acc], ag->gradients[acc], u);
	MOL_VEC_SUB(ag->gradients[hydro], ag->gradients[hydro], v);
	MOL_VEC_ADD(ag->gradients[don], ag->gradients[don], v);


	MOL_VEC_MULT_SCALAR(u, ah_unit_vector, xh_cosine);
	MOL_VEC_SUB(u, ba_unit_vector, u);
	MOL_VEC_MULT_SCALAR(u, u, -de_dxh * weight / ah_distance);
	MOL_VEC_MULT_SCALAR(v, ba_unit_vector, xh_cosine);
	MOL_VEC_SUB(v, v, ah_unit_vector);
	MOL_VEC_MULT_SCALAR(v, v, -de_dxh * weight / ba_distance);

	MOL_VEC_ADD(ag->gradients[hydro], ag->gradients[hydro], u);
	MOL_VEC_SUB(ag->gradients[acc], ag->gradients[acc], u);
	MOL_VEC_SUB(ag->gradients[acc], ag->gradients[acc], v);

	if ((hbe == hbe_RINGSC) || (hbe == hbe_RINGB)) {
		MOL_VEC_MULT_SCALAR(v, v, 0.5);
		MOL_VEC_ADD(ag->gradients[base], ag->gradients[base], v);
		MOL_VEC_ADD(ag->gradients[base2], ag->gradients[base2], v);
	} else if ((hbe == hbe_SP3SC) || (hbe == hbe_SP3B)) {
		MOL_VEC_ADD(ag->gradients[base2], ag->gradients[base2], v);
	} else {
		MOL_VEC_ADD(ag->gradients[base], ag->gradients[base], v);
	}

	return true;
}


/**
 * Calculate pairwise energy and gradient.
 *
 * \param ag Atomgroup.
 * \param hydro Hydrogen atom id in \p ag.
 * \param acc Acceptor atom id in \p ag.
 * \param base2_arr Array of secondary base atoms, typically retrieved from \c hbond_base2 metadata.
 * \param hybridization_states Array of secondary base atoms, typically retrieved from \c atom_hybridization_state metadata.
 * \param rc2 Squared cutoff distance for A-H separation (Angstrom^2).
 * \param weight Energy weighting term.
 * \return
 */
double _get_pairwise_hbondeng(
		struct mol_atom_group *ag,
		const size_t hydro,           // index of the hydrogen atom in atoms_hydro
		const size_t acc,             // index of the acceptor atom in atoms_acc,
		const size_t *base2_arr,
		const enum smp_hybridization_state *hybridization_states,
		const double rc2,
		const double weight)
{
	struct mol_vector3 d;
	MOL_VEC_SUB(d, ag->coords[hydro], ag->coords[acc]);
	double d2 = MOL_VEC_SQ_NORM(d);

	if (d2 > rc2)
		return 0.0;

	enum _bond_type hbe = _get_hbe_type(ag, hydro, acc, hybridization_states);
	if (hbe == hbe_NONE)
		return 0.0;

	double energy;
	if (_hbond_energy_and_gradient_computation(ag, &energy, hydro, acc, hbe, base2_arr, weight))
		return energy;
	else
		return 0.0;
}


void smp_energy_hbond(
		struct mol_atom_group *ag,
		double *energy,
		const struct nblist *nblst,
		const double weight,
		const bool use_split,
		const int atom_split)
{
	const double rc = nblst->nbcof;
	const double rc2 = rc * rc;
	const size_t *base2_arr = mol_atom_group_fetch_metadata(ag, SMP_HBOND_BASE2_METADATA_KEY);
	const enum smp_hybridization_state *hybridization_states = smp_hybridization_states_fetch(ag);

	for (int i = 0; i < nblst->nfat; i++) {
		const int ai = nblst->ifat[i];

		if (MOL_ATOM_HBOND_PROP_IS(ag, ai, DONATABLE_HYDROGEN) || MOL_ATOM_HBOND_PROP_IS(ag, ai, HBOND_ACCEPTOR)) {
			const int n2 = nblst->nsat[i];
			const int *p = nblst->isat[i];


			for (int j = 0; j < n2; j++) {
				const int aj = p[j];

				if (use_split && (
						((ai < atom_split) && (aj < atom_split)) ||
						((ai >= atom_split) && (aj >= atom_split))))
					continue;

				if (MOL_ATOM_HBOND_PROP_IS(ag, ai, DONATABLE_HYDROGEN)) {
					if (MOL_ATOM_HBOND_PROP_IS(ag, aj, HBOND_ACCEPTOR))
						*energy += _get_pairwise_hbondeng(ag, ai, aj, base2_arr,
							hybridization_states, rc2, weight);
				} else {
					if (MOL_ATOM_HBOND_PROP_IS(ag, aj, DONATABLE_HYDROGEN))
						*energy += _get_pairwise_hbondeng(ag, aj, ai, base2_arr,
							hybridization_states, rc2, weight);
				}
			}
		}
	}
}


bool smp_hbond_init_bases(struct mol_atom_group *ag, FILE *log)
{
	_set_hbond_acceptor_bases(ag);
	return _check_bases(ag, log);
}
