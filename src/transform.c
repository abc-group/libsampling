#include "transform.h"
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

void smp_vector3_rotate_by_quaternion(struct mol_vector3 *v, const struct mol_quaternion *q)
{
	struct mol_vector3 tmp1, tmp2, tmp3; // Sorry for non-descriptive names
	MOL_VEC_MULT_SCALAR(tmp1, *v, q->W);
	MOL_VEC_CROSS_PROD(tmp2, *q, *v);
	MOL_VEC_ADD(tmp2, tmp1, tmp2);
	MOL_VEC_CROSS_PROD(tmp3, *q, tmp2);
	MOL_VEC_MULT_SCALAR(tmp3, tmp3, 2);
	MOL_VEC_ADD(*v, *v, tmp3);
}


#define _MUL_ROW(i,j) r.m##i##j = ma->m##i##1 * mb->m1##j + ma->m##i##2 * mb->m2##j + ma->m##i##3 * mb->m3##j
struct mol_matrix3 smp_matrix3_mul(const struct mol_matrix3 *ma, const struct mol_matrix3 *mb)
{
	struct mol_matrix3 r;
	_MUL_ROW(1,1);
	_MUL_ROW(1,2);
	_MUL_ROW(1,3);
	_MUL_ROW(2,1);
	_MUL_ROW(2,2);
	_MUL_ROW(2,3);
	_MUL_ROW(3,1);
	_MUL_ROW(3,2);
	_MUL_ROW(3,3);
	return r;
}
#undef _MUL_ROW

void smp_matrix3_eye(struct mol_matrix3 *m)
{
	m->m11 = 1; m->m12 = 0; m->m13 = 0;
	m->m21 = 0; m->m22 = 1; m->m23 = 0;
	m->m31 = 0; m->m32 = 0; m->m33 = 1;
}

double smp_vector3_normalize(struct mol_vector3 *v)
{
	double vnorm = sqrt(MOL_VEC_SQ_NORM(*v));
	if (vnorm != 0)
		MOL_VEC_DIV_SCALAR(*v, *v, vnorm);
	return vnorm;
}
