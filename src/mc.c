#include "mc.h"
#include <math.h>

bool smp_mc_metropolis(
		struct smp_prng *prng,
		const double e_old,
		const double e_new,
		const double kt)
{
	if (e_new <= e_old) {
		return true;
	} else {
		const double p = exp(-(e_new-e_old)/kt);
		const double r = smp_prng_uniform(prng);
		return (r < p);
	}
}
