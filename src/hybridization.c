#include "hybridization.h"

#include <stdio.h>
#include <string.h>
#include <jansson.h>
#include "utils.h"

#define BUF_SIZE 1024

/**
  Routine for reading hybridization states from CHIMERA MOL2 file
*/
static enum smp_hybridization_state _hydridization_from_sybyl(const char *sybyl_type)
{
	if (sybyl_type == NULL)
		return UNKNOWN_HYBRID;

	/* Carbons */
	if (strcmp("C.3", sybyl_type) == 0) /* carbon sp3 */
		return SP3_HYBRID;
	if (strcmp("C.2", sybyl_type) == 0) /* carbon sp2 */
		return SP2_HYBRID;
	if (strcmp("C.cat", sybyl_type) == 0) /* carbocation (C+) used only in a guadinium group */
		return SP2_HYBRID;
	if (strcmp("C.1", sybyl_type) == 0) /* carbon sp */
		return SP1_HYBRID;
	if (strcmp("C.ar", sybyl_type) == 0) /* carbon aromatic */
		return RING_HYBRID;

	/* Nitrogens */
	if (strcmp("N.3", sybyl_type) == 0) /* nitrogen sp3 */
		return SP3_HYBRID;
	if (strcmp("N.4", sybyl_type) == 0) /* nitrogen sp3 positively charged */
		return SP3_HYBRID;
	if (strcmp("N.2", sybyl_type) == 0) /* nitrogen sp2 */
		return SP2_HYBRID;
	if (strcmp("N.am", sybyl_type) == 0) /* nitrogen amide */
		return SP2_HYBRID;
	if (strcmp("N.pl3", sybyl_type) == 0) /* nitrogen trigonal planar */
		return SP2_HYBRID;
	if (strcmp("N.1", sybyl_type) == 0) /* nitrogen sp */
		return SP1_HYBRID;
	if (strcmp("N.ar", sybyl_type) == 0) /* nitrogen aromatic */
		return RING_HYBRID;

	/* Oxygens */
	if (strcmp("O.3", sybyl_type) == 0) /* oxygen sp3 */
		return SP3_HYBRID;
	if (strcmp("O.spc", sybyl_type) == 0) /* oxygen in single point charge (SPC) water model */
		return SP3_HYBRID;
	if (strcmp("O.t3p", sybyl_type) == 0) /* oxygen in Transferable Intermolecular Potential (TIP3P) water model */
		return SP3_HYBRID;
	if (strcmp("O.2", sybyl_type) == 0) /* oxygen sp2 */
		return SP2_HYBRID;
	if (strcmp("O.co2", sybyl_type) == 0) /* oxygen in carboxylate and phosphate groups */
		return SP2_HYBRID;

	/* Sulfurs */
	if (strcmp("S.3", sybyl_type) == 0) /* sulfur sp3 */
		return SP3_HYBRID;
	if (strcmp("S.O", sybyl_type) == 0) /* sulfoxide sulfur */
		return SP3_HYBRID;
	if (strcmp("S.O2", sybyl_type) == 0) /* sulfone sulfur */
		return SP3_HYBRID;
	if (strcmp("S.2", sybyl_type) == 0) /* sulfur sp2 */
		return SP2_HYBRID;

	/* Phosphorous */
	if (strcmp("P.3", sybyl_type) == 0) /* phosphorous sp3 */
		return SP3_HYBRID;

	/* everything else */
	return UNKNOWN_HYBRID;
}


bool smp_hybridization_read(struct mol_atom_group *ag, const char *filename)
{
	char buffer[BUF_SIZE];
	bool mol_info_found = false;

	FILE* fp = fopen(filename, "r");

	if (!fp) {
		PRINTF_ERROR("Failed to open %s!\n", filename);
		return false;
	}

	enum smp_hybridization_state *hybridization_state = calloc(ag->natoms, sizeof(enum smp_hybridization_state));
	mol_atom_group_add_metadata(ag, SMP_HYBRIDIZATION_METADATA_KEY, hybridization_state);

	while (fgets(buffer, BUF_SIZE, fp) != NULL) {
		if (strstr(buffer, "<TRIPOS>MOLECULE") != NULL) {
			if (fgets(buffer, BUF_SIZE, fp) == NULL) {
				PRINTF_ERROR("Failed to read %s!", filename);
				fclose(fp);
				return false;
			}

			if (fgets(buffer, BUF_SIZE, fp) == NULL) {
				PRINTF_ERROR("Failed to read %s!", filename);
				fclose(fp);
				return false;
			}

			size_t nat = strtoul(buffer, NULL, 10);

			if (nat != ag->natoms) {
				PRINTF_ERROR("%s has %zu atoms instead of %zu atoms!", filename, nat, ag->natoms);
				fclose(fp);
				return false;
			}

			mol_info_found = true;
		}

		if (strstr(buffer, "<TRIPOS>ATOM") != NULL) {
			if (!mol_info_found) {
				PRINTF_ERROR("MOLECULE Record missing in %s!", filename);
				fclose(fp);
				return false;
			}

			for (size_t i = 0; i < ag->natoms; i++) {
				if (fgets(buffer, BUF_SIZE, fp) == NULL) {
					PRINTF_ERROR("Failed to read %s!", filename);
					fclose(fp);
					return false;
				}

				size_t atom_id;
				char atom_name[20], atom_type[20];
				double x, y, z;

				if (sscanf(buffer, "%zu %s %lf %lf %lf %s", &atom_id, atom_name, &x, &y, &z, atom_type ) != 6) {
					PRINTF_ERROR("Failed to read ATOM record from %s!", filename);
					fclose(fp);
					return false;
				}

				hybridization_state[i] = _hydridization_from_sybyl(atom_type);
			}
		}
	}
	fclose(fp);
	return true;
}


static void _set_donors(struct mol_atom_group *ag)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		if (MOL_ATOM_HBOND_PROP_IS(ag, i, DONATABLE_HYDROGEN)) {
			for (size_t j = 0; j < ag->bond_lists[i].size; j++) {
				const size_t acc_bond_idx = ag->bond_lists[i].members[j];
				const struct mol_bond acc_bond = ag->bonds[acc_bond_idx];
				size_t base_atomi = acc_bond.ai;
				if (base_atomi == i)
					base_atomi = acc_bond.aj;

				ag->hbond_base_atom_id[i] = base_atomi;
				MOL_ATOM_HBOND_PROP_ADD(ag, base_atomi, HBOND_DONOR);
				break;
			}
		}
	}
}

bool smp_hybridization_read_json(struct mol_atom_group *ag, const char *filename, bool read_hprop)
{
	json_error_t json_file_error;
	json_t *base = json_load_file(filename, 0, &json_file_error);

	if (!base) {
		PRINTF_ERROR("error reading json file %s on line %d column %d: %s",
		        filename, json_file_error.line, json_file_error.column, json_file_error.text);
		return false;
	}

	if (!json_is_object(base)) {
		PRINTF_ERROR("json file not an object: %s", filename);
		return false;
	}

	json_t *version = json_object_get(base, "version");
	if (version == NULL || !json_is_number(version)) {
		PRINTF_ERROR("no version or version is not a number: %s", filename);
		return false;
	}
	int version_int = json_integer_value(version);
	if (version_int != 1) {
		PRINTF_ERROR("only version 1 is supported; file %s is version %d", filename, version_int);
		return false;
	}

	json_t *atoms;
	atoms = json_object_get(base, "atoms");
	if (!json_is_array(atoms)) {
		PRINTF_ERROR("json atoms are not an array");
		return false;
	}

	size_t natoms = json_array_size(atoms);
	if (natoms != ag->natoms) {
		PRINTF_ERROR("Different number of atoms in atomgroup (%zd) and %s (%zd)", ag->natoms, filename, natoms);
	}

	enum smp_hybridization_state *hybridization_state = calloc(ag->natoms, sizeof(enum smp_hybridization_state));
	mol_atom_group_add_metadata(ag, SMP_HYBRIDIZATION_METADATA_KEY, hybridization_state);
	if (read_hprop) {
		mol_atom_group_init_atomic_field(ag, hbond_prop);
		mol_atom_group_init_atomic_field(ag, hbond_base_atom_id);
	}

	for (size_t i = 0; i < natoms; i++) {
		json_t *atom = json_array_get(atoms, i);
		if (!json_is_object(atom)) {
			fprintf(stderr, "atom %zd not an object\n", i);
			continue;
		}

		json_t *sybyl_type = json_object_get(atom, "sybyl_type");
		if (sybyl_type != NULL) {
			const char *sybyl_type_string;
			if (!json_is_string(sybyl_type)) {
				PRINTF_ERROR("json sybyl_type is not string for atom %zd\n", i);
				hybridization_state[i] = UNKNOWN_HYBRID;
			} else {
				sybyl_type_string = json_string_value(sybyl_type);
				hybridization_state[i] = _hydridization_from_sybyl(sybyl_type_string);
			}
		} else {
			hybridization_state[i] = UNKNOWN_HYBRID;
		}

		if (read_hprop) {
			ag->hbond_prop[i] = UNKNOWN_HPROP;
			json_t *hb_acceptor = json_object_get(atom, "hb_acceptor");
			if (hb_acceptor != NULL) {
				if (!json_is_boolean(hb_acceptor)) {
					PRINTF_ERROR("json hb_acceptor is not boolean for atom %zd", i);
				}
				if (json_is_true(hb_acceptor)) {
					MOL_ATOM_HBOND_PROP_ADD(ag, i, HBOND_ACCEPTOR);
				}
			}
			json_t *hb_donor = json_object_get(atom, "hb_donor");
			if (hb_donor != NULL) {
				if (!json_is_boolean(hb_donor)) {
					PRINTF_ERROR("json hb_donor is not boolean for atom %zd", i);
				}
				if (json_is_true(hb_donor)) {
					MOL_ATOM_HBOND_PROP_ADD(ag, i, DONATABLE_HYDROGEN);
				}
			}
		}
	}

	json_decref(base);

	if (read_hprop)
		_set_donors(ag);

	return true;
}


enum smp_hybridization_state *smp_hybridization_states_fetch(const struct mol_atom_group *ag)
{
	return (enum smp_hybridization_state*) mol_atom_group_fetch_metadata(ag, SMP_HYBRIDIZATION_METADATA_KEY);
}


bool smp_hybridization_states_present(const struct mol_atom_group *ag)
{
	return mol_atom_group_has_metadata(ag, SMP_HYBRIDIZATION_METADATA_KEY);
}


void smp_hybridization_states_delete(struct mol_atom_group *ag)
{
	if (smp_hybridization_states_present(ag)) {
		enum smp_hybridization_state *h = smp_hybridization_states_fetch(ag);
		free(h);
		mol_atom_group_delete_metadata(ag, SMP_HYBRIDIZATION_METADATA_KEY);
	}
}


void smp_hybridization_atom_group_join(
		struct mol_atom_group *dst,
		const struct mol_atom_group *src_a,
		const struct mol_atom_group *src_b)
{
	mol_atom_group_init_atomic_field(dst, hbond_prop);
	mol_atom_group_init_atomic_field(dst, hbond_base_atom_id);
	enum smp_hybridization_state *h_state = calloc(dst->natoms, sizeof(enum smp_hybridization_state));

	const enum smp_hybridization_state *h_state_a = smp_hybridization_states_fetch(src_a);
	const enum smp_hybridization_state *h_state_b = smp_hybridization_states_fetch(src_b);

	for (size_t i = 0; i < src_a->natoms; i++) {
		dst->hbond_prop[i] = src_a->hbond_prop[i];
		dst->hbond_base_atom_id[i] = src_a->hbond_base_atom_id[i];
		h_state[i] = h_state_a[i];
	}

	const size_t d = src_a->natoms;
	for (size_t i = 0; i < src_b->natoms; i++) {
		dst->hbond_prop[d+i] = src_b->hbond_prop[i];
		dst->hbond_base_atom_id[d+i] = src_b->hbond_base_atom_id[i] + d;
		h_state[d+i] = h_state_b[i];
	}

	mol_atom_group_add_metadata(dst, SMP_HYBRIDIZATION_METADATA_KEY, h_state);
}
