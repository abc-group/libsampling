/*
   Based on a C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.keio.ac.jp/matumoto/emt.html
   email: matumoto@math.keio.ac.jp
*/

#include "prng.h"
#include "utils.h"
#include "transform.h"
#include <limits.h>
#include <math.h>

//The inverse of (0xffffffff + 1 = 2^32 = UINT_MAX+1)
static const double uint_max_pp_inv = 2.32830643653869628906e-010;

// Prng parameters. There are taken from original code. I do not have idea what is that
const int SMP_PRNG_MT_M = 397;
#define MATRIX_A 0x9908b0dfUL   // constant vector a
const unsigned long UPPER_MASK = 0x80000000UL; // most significant w-r bits
const unsigned long LOWER_MASK = 0x7fffffffUL; // least significant r bits

// Prng tempering parameters. There are taken from original code. I do not have idea what is that
const unsigned long TEMPERING_MASK_B = 0x9d2c5680UL;
const unsigned long TEMPERING_MASK_C = 0xefc60000UL;
#define TEMPERING_SHIFT_U(y)  ((y) >> 11)
#define TEMPERING_SHIFT_S(y)  ((y) << 7)
#define TEMPERING_SHIFT_T(y)  ((y) << 15)
#define TEMPERING_SHIFT_L(y)  ((y) >> 18)


/* Internal initialization function for Mersenne twister implementation (MT19937)
 * initializes mti and mt[SMP_PRNG_MT_N] with a seed
 */
void smp_prng_mt_init(struct smp_prng *prng, unsigned long seed)
{
	prng->mti = SMP_PRNG_MT_N + 1;
	prng->mt[0] = seed & 0xffffffffUL;
	for (prng->mti = 1; prng->mti < SMP_PRNG_MT_N; prng->mti++) {
		prng->mt[prng->mti] =
			(1812433253UL * (prng->mt[prng->mti - 1] ^ (prng->mt[prng->mti - 1] >> 30)) + prng->mti);
		prng->mt[prng->mti] &= 0xffffffffUL;
	}
}

void smp_prng_init(struct smp_prng *prng, const unsigned long seed, const enum smp_prng_type type)
{
	if (type == PRNG_MT19937) {
		smp_prng_mt_init(prng, seed);
	}
}

struct smp_prng *smp_prng_create(const unsigned long seed, const enum smp_prng_type type)
{
	struct smp_prng *prng = NULL;
	if (type == PRNG_MT19937) {
		prng = malloc(sizeof(struct smp_prng));
		if (prng != NULL) {
			smp_prng_init(prng, seed, type);
		}
	}
	return prng;
}

void smp_prng_destroy(__attribute__((unused)) struct smp_prng *smp_prng)
{

}

void smp_prng_free(struct smp_prng *smp_prng)
{
	if (smp_prng != NULL) {
		smp_prng_destroy(smp_prng);
		free(smp_prng);
	}
}

/* Internal initialization function for Mersenne twister implementation (MT19937)
 * generates a random number on [0,0xffffffff]-interval
 * returns unsigned long according to original implementation of MT19937
 */
unsigned long smp_prng_uint32(struct smp_prng *prng)
{
	unsigned long y;
	static const unsigned long mag01[2] = {0x0UL, MATRIX_A};

	if (prng->mti >= SMP_PRNG_MT_N) { /* generate SMP_PRNG_MT_N words at one time */
		int kk;

		for (kk = 0; kk < SMP_PRNG_MT_N - SMP_PRNG_MT_M; kk++) {
			y = (prng->mt[kk] & UPPER_MASK) | (prng->mt[kk + 1] & LOWER_MASK);
			prng->mt[kk] = prng->mt[kk + SMP_PRNG_MT_M] ^ (y >> 1) ^ mag01[y & 0x1UL];
		}
		for (; kk < SMP_PRNG_MT_N - 1; kk++) {
			y = (prng->mt[kk] & UPPER_MASK) | (prng->mt[kk + 1] & LOWER_MASK);
			prng->mt[kk] = prng->mt[kk + (SMP_PRNG_MT_M - SMP_PRNG_MT_N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
		}
		y = (prng->mt[SMP_PRNG_MT_N - 1] & UPPER_MASK) | (prng->mt[0] & LOWER_MASK);
		prng->mt[SMP_PRNG_MT_N - 1] = prng->mt[SMP_PRNG_MT_M - 1] ^ (y >> 1) ^ mag01[y & 0x1UL];

		prng->mti = 0;
	}

	y = prng->mt[prng->mti++];

	// Tempering phase
	y ^= TEMPERING_SHIFT_U(y);
	y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
	y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
	y ^= TEMPERING_SHIFT_L(y);

	return y;
}

double smp_prng_uniform(struct smp_prng *prng)
{
	return smp_prng_uint32(prng) * uint_max_pp_inv;
}

float smp_prng_uniformf(struct smp_prng *prng)
{
	return (float) (smp_prng_uint32(prng) * uint_max_pp_inv);
}

double smp_prng_gaussian(struct smp_prng *prng)
{
	double r1, r2;
	smp_prng_gaussian2(prng, &r1, &r2);
	return r1;
}

void smp_prng_gaussian2(struct smp_prng *prng, double *r1, double *r2)
{
	double fac, rsq, v1, v2;
	do {
		v1 = 2.0 * smp_prng_uniform(prng) - 1.0;
		v2 = 2.0 * smp_prng_uniform(prng) - 1.0;
		rsq = v1 * v1 + v2 * v2;
	} while (rsq >= 1.0 || rsq == 0.0);
	fac = sqrt(-2.0 * log(rsq) / rsq);
	*r1 = v1 * fac;
	*r2 = v2 * fac;
}

float smp_prng_gaussianf(struct smp_prng *prng)
{
	float r1, r2;
	smp_prng_gaussian2f(prng, &r1, &r2);
	return r1;
}

void smp_prng_gaussian2f(struct smp_prng *prng, float *r1, float *r2)
{
	float fac, rsq, v1, v2;
	do {
		v1 = 2.0f * smp_prng_uniformf(prng) - 1.0f;
		v2 = 2.0f * smp_prng_uniformf(prng) - 1.0f;
		rsq = v1 * v1 + v2 * v2;
	} while (rsq >= 1.0 || rsq == 0.0);
	fac = sqrtf(-2.0f * logf(rsq) / rsq);
	*r1 = v1 * fac;
	*r2 = v2 * fac;
}

void smp_prng_quaternion(struct smp_prng *smp_prng, struct mol_quaternion *q)
{
	double s;
	double sigma1, sigma2;
	double theta1, theta2;

	s = smp_prng_uniform(smp_prng);
	sigma1 = sqrt(1 - s);
	sigma2 = sqrt(s);
	theta1 = 2 * M_PI * smp_prng_uniform(smp_prng);
	theta2 = 2 * M_PI * smp_prng_uniform(smp_prng);

	q->W = cos(theta2) * sigma2;
	q->X = sin(theta1) * sigma1;
	q->Y = cos(theta1) * sigma1;
	q->Z = sin(theta2) * sigma2;
}

void smp_prng_quaternion_small(struct smp_prng *smp_prng, struct mol_quaternion *q, const double delta)
{
	/* The methods works as follows: first, we generate random quaternion; then we use Slerp interpolation between
	 * it and "identity" quaternion to obtain smaller quaternion. And that's it */
	struct mol_quaternion qa, qb;
	// Set up quaternion corresponding to identity matrix
	qa.W = 1;
	qa.X = 0;
	qa.Y = 0;
	qa.Z = 0;

	// Generate random uniform quaternion
	smp_prng_quaternion(smp_prng, &qb);

	// Take dot product between two quaternions
	double dot = qb.W;  // As qa is unit quaternion, the dot product is very simple
	// Wrap around qb for angles larger than 90 deg because q=-q
	if (dot < 0) {
		SMP_QUATERNION_MULT_SCALAR(qb, qb, -1);
		dot = -dot;
	}

	// Use "slerp" algorithm (spherical interpolation) between qa and qb
	double r, s;
	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD) {
		// If the random quaternion is close to unity,
		// take the first element of Taylor series.
		r = 1.0 - delta;
		s = delta;
	} else {
		double sigma = acos(dot);
		double gamma = 1 / sin(sigma);
		r = sin((1 - delta) * sigma) * gamma;
		s = sin(delta * sigma) * gamma;
	}
	// Reuse qa as spherical interpolation between qa and qb
	SMP_QUATERNION_MULT_SCALAR(qa, qa, r);
	SMP_QUATERNION_MULT_SCALAR(qb, qb, s);
	SMP_QUATERNION_ADD(*q, qa, qb);

	// Normalize qa and write to q
	*q = mol_quaternion_normalize(*q);
}
