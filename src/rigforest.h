#ifndef _SMP_RIGFOREST_H_
#define _SMP_RIGFOREST_H_

#include <mol2/atom_group.h>
#include <mol2/quaternion.h>
#include <mol2/vector.h>
#include <mol2/lbfgs.h>

/**
 * \addtogroup l_rigforest Rigforest
 * \brief Module to work with rigid trees.
 * @{
 */

struct smp_rigtree_node {
	size_t label; //!< Label for this rigid cluster.
	size_t natoms; //!< Number of atoms in this rigid cluster.
	size_t *atoms; //!< Atoms belonging to this rigid cluster.
};

struct smp_rigtree_edge {
	size_t begin; //!< Index of node from which this edge starts.
	size_t end; //!< Index of node where this edge terminates.
};

struct smp_rigtree {
	size_t nnodes; //!< Number of nodes in this tree. Since it is a tree, it has \c nnodes-1 edges.
	struct smp_rigtree_node *nodes; //!< Array of nodes of the tree.
	size_t *node_degrees; //!< Outdegree for each node.
	size_t *node_parents; //!< Parent ID of each node.

	size_t **adjacency_matrix; //!< Adjacency matrix for the tree.

	struct smp_rigtree_edge *edges; //!< List of edges in the tree; edge \c i goes from vertex \c e[2*i] to \c e[2*i+1].
	double *edge_thetas; //!< Current rotation angle for each rotatable bond (i.e. edge).
	double *edge_grads; //!< List of energy derivatives for each rotatable bond (i.e. edge); length \c nnodes-1.

	size_t natoms; //!< Total number of atoms in this tree.
	size_t *atoms; //!< Indices of atoms belonging to the tree in original mol_atom_group; length is \p natoms.
	size_t *node_label_for_atom; //!< ID of nodes for each atom.

	struct mol_quaternion tree_rotation; //!< Whole body rotation parameter.
	struct mol_vector3 tree_translation; //!< Whole body translation vector.
	struct mol_vector3 tree_center; //!< Whole body center of rotation.
	struct mol_vector3 tree_rotation_grads; //!< Gradients of the whole-tree rotation (in exp-form).
	struct mol_vector3 tree_translation_grads; //!< Gradients of the whole-tree translation.

	size_t *_edge_index; //!< List of edges in order of BFS tree traversal.
};

struct smp_rigforest {
	size_t nfree_atoms; //!< Number of free atoms.
	size_t nfree_trees; //!< Number of free rigid trees (movable root).
	size_t nanch_trees; //!< Number of anchored rigid (fixed root). Used for flexible sidechains.

	size_t *free_atoms; //!< Indices of free atoms; length \ref nfree_atoms.
	struct mol_vector3 *free_atoms_coordinates; //!< Coordinates of free atoms; length \ref nfree_atoms.
	struct mol_vector3 *free_atoms_gradients; //!< Derivatives of energy function for free atoms; length \ref nfree_atoms.

	struct smp_rigtree** free_trees; //!< Array of pointer to free rigtrees; length \ref nfree_trees.
	struct smp_rigtree** anch_trees; //!< Array of pointers to anchored rigtrees; length \ref nanch_trees.

	struct mol_vector3 *original_coordinates; //!< Original coordinate of the atoms.
};


/**
 * Allocate and initialize new \ref smp_rigforest.
 *
 * \param filename Name of file containing rigforest structure.
 * \param ag Atomgroup.
 * \return Newly allocated and initialized rigforest.
 */
struct smp_rigforest *smp_rigforest_create(
	const char *filename,
	struct mol_atom_group *ag);

/**
 * Initialize existing \ref smp_rigforest structure.
 *
 * See \ref smp_rigforest_create for description of most parameters.
 * \param rf Pointer to \ref smp_rigforest.
 */
void smp_rigforest_init(
	struct smp_rigforest *rf,
	const char *filename,
	struct mol_atom_group *ag);

/**
 * This function resets rigforest configuration to the one defined in \p filename.
 * This function won't check or reallocate data, so you should not use it to load different forest!
 *
 * See \ref smp_rigforest_create for description of most parameters.
 * \param rf Pointer to already allocated and initialized \ref smp_rigforest.
 */
void smp_rigforest_reload(
	struct smp_rigforest *rf,
	const char *filename,
	struct mol_atom_group *ag);

/**
 * Uninitialize structure and free all memory pointers stored inside it. It does not free \p rf itself.
 */
void smp_rigforest_destroy(struct smp_rigforest *rf);

/**
 * Uninitialize rigforest, freeing all allocated memory, including \p rf itself.
 */
void smp_rigforest_free(struct smp_rigforest *rf);

/**
 * Calculate the number of dimensions in rigforest.
 * The dimensions are following (the order here corresponds to the order used for coordinates during minimization):
 * - for each free atom (\c numf):
 * - - its 3D coordinates (\c 3),
 * - for each rigtree (\c numt):
 * - - angles of rotation for each dihedral in tree (\c v-1),
 * - - exp rotation of the whole tree (\c 3),
 * - - translation of the whole tree (\c 3),
 * - for each torsion (\c numtor):
 * - * angles of rotation for each dihedral in tree (\c v-1).
 */
size_t smp_rigforest_ndim(const struct smp_rigforest *rf);


/**
 * Copy atom coordinates from \c rf->orig to \c ag->coords.
 */
void smp_rigforest_copy_coords_to_atomgroup(struct mol_atom_group *ag, const struct smp_rigforest *rf);

/**
 * Copy atom coordinates from \c ag->coords to \c rf->orig.
 * NOTE: This function does not update center-of-mass in rigtree. Be sure to call \ref smp_rigforest_update_center.
 */
void smp_rigforest_copy_coords_from_atomgroup(struct smp_rigforest *rf, const struct mol_atom_group *ag);

/**
 * This helper function resets tree to the coordinates specified in \p array. Necessary for minimization.
 * See \ref smp_rigforest_ndim for description of \p array content.
 * Both \p rf and \p ag are updated.
 * NOTE: This function does not update center-of-mass in rigtree. Be sure to call \ref smp_rigforest_update_center.
 */
void smp_rigforest_update_from_coordinate_array(struct smp_rigforest *rf, struct mol_atom_group *ag, const double *array);

/**
 * Recalculate center-of-mass of every tree using atom coordinates from \p ag.
 */
void smp_rigforest_update_center(struct smp_rigforest *rf, const struct mol_atom_group *ag);

/**
 * Change atom coordinates in \p ag so they correspond to states of trees in \p rf.
 * NOTE: This function does not update center-of-mass in rigtree. Be sure to call \ref smp_rigforest_update_center.
 */
void smp_rigforest_move(struct mol_atom_group *ag, const struct smp_rigforest *rf);

/**
 * Update \p ag according to \p rf params, and calculate gradient for rigforest variables.
 * See \ref smp_rigforest_ndim for description of variables for \p gradient.
 *
 * \param gradient Pointer to an array where gradients will be placed.
 * \param rf Rigforest.
 * \param ag Atomgroup. Atom coordinates are updated to match \p rf configuration.
 */
void smp_rigforest_derive(double *restrict gradient, const struct smp_rigforest *rf, struct mol_atom_group *ag);

/**
 * Perform rigforest minimization with gradient descend. Only trees (incl. torsions) and free atoms are moved.
 *
 * \param maxIt Max. number of iterations.
 * \param tol Tolerance: when to stop minimizing.
 * \param rf Rigforest to minimize.
 * \param prms Parameters passed to \p egfun.
 * \param egfun Energy function.
 */
void smp_rigforest_minimize(int maxIt, double tol, struct smp_rigforest *rf, void *prms, lbfgs_evaluate_t egfun);

/** @}*/
#endif // _SMP_RIGFOREST_H_
