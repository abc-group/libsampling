#ifndef _SMP_HBOND_POLYNOMIALS_H_
#define _SMP_HBOND_POLYNOMIALS_H_

#include <math.h>

// Only non-positive b-spline fading function is supported

static const double poly_out = 0.0; ///< Polynomial value outside of clip interval [xmin xmax]

// Macroses for polynomials generation
#define CREATE_POLY_3(name, xmin, xmax, c2, c1, c0) \
	static void _poly_##name(double x, double *val, double *der) \
	{ \
		if ((x<=xmin)||(x>=xmax)) { \
			(*val) = poly_out; \
			(*der) = 0.0; \
			return; \
		} \
		(*val) = (*der) = c2; \
		(*val) = (*val)*x+c1; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c0; \
	}

#define CREATE_POLY_5(name, xmin, xmax, c4, c3, c2, c1, c0) \
	static void _poly_##name(double x, double *val, double *der) \
	{ \
		if ((x<=xmin)||(x>=xmax)) { \
			(*val) = poly_out; \
			(*der) = 0.0; \
			return; \
		} \
		(*val) = (*der) = c4; \
		(*val) = (*val)*x+c3; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c2; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c1; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c0; \
	}

#define CREATE_POLY_7(name, xmin, xmax, c6, c5, c4, c3, c2, c1, c0) \
	static void _poly_##name(double x, double *val, double *der) \
	{ \
		if ((x<=xmin)||(x>=xmax)) { \
			(*val) = poly_out; \
			(*der) = 0.0; \
			return; \
		} \
		(*val) = (*der) = c6; \
		(*val) = (*val)*x+c5; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c4; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c3; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c2; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c1; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c0; \
	}

#define CREATE_POLY_8(name, xmin, xmax, c7, c6, c5, c4, c3, c2, c1, c0) \
	static void _poly_##name(double x, double *restrict val, double *restrict der) \
	{ \
		if ((x<=xmin)||(x>=xmax)) { \
			(*val) = poly_out; \
			(*der) = 0.0; \
			return; \
		} \
		(*val) = (*der) = c7; \
		(*val) = (*val)*x+c6; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c5; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c4; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c3; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c2; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c1; \
		(*der) = (*der)*x+(*val); \
		(*val) = (*val)*x+c0; \
	}

// Defining polynomials for hbond score calculation

CREATE_POLY_8(AH_BBHelix, 1.78218633, 2.6757,
	12.93768086, -221.0155722, 1604.391304, -6409.335773, 15200.86425, -21375.00216, 16475.98811, -5361.55644)

CREATE_POLY_8(AH_BBOther, 1.6971523, 2.679339,
	13.58980244, -224.0452428, 1568.933094, -6044.257847, 13820.1498, -18730.96076, 13912.92238, -4361.995425)

CREATE_POLY_5(AH_SP2, 1.6941, 2.5,
	10.98727738, -100.2401419, 340.9733405, -511.6111233, 285.0061262)

CREATE_POLY_5(AH_SP3, 1.755, 2.521385,
	7.011735538, -68.99968829, 251.820931, -403.3593133, 238.7378958)

CREATE_POLY_8(xD_BBHelix, 0.3746, 1.04,
	223.5268153, -757.7254095, 1019.593508, -689.2232431, 240.1436064, -37.84119583, 0.85868904, 0.278181985)

CREATE_POLY_8(xD_BBOther, 0.76, 1.09,
	111.9877946, -380.3066184, 514.7650204, -352.4092342, 124.6219703, -19.94401946, 0.149314979, 0.635771774)

CREATE_POLY_3(xD_SP2short, 0.7071, 1.01,
	-0.562582503, -0.746682668, 0.809265171)

CREATE_POLY_3(xD_SP2long, 0.0, 1.01,
	0.094962885, -0.254313172, 0.0)

CREATE_POLY_3(xD_SP3short, 0.61566, 1.01,
	-0.100140144, -1.139139041, 0.739279186)

CREATE_POLY_3(xD_SP3long, 0.0, 1.01,
	0.089380221, -0.207503776, 0.0)

CREATE_POLY_8(xH_BBHelix, 0.156, 1.03,
	54.80664331, -196.8196655, 295.9418886, -232.105602, 96.99124565, -20.60918361, 1.573169816, 0.000745458)

CREATE_POLY_8(xH_BBOther, 0.61566, 1.07,
	43.94483847, -144.3836033, 193.5865176, -132.4469355, 47.28137288, -8.945888012, -0.227035135, 0.791902995)

CREATE_POLY_3(xH_SP2short, 0.0, 1.08,
	1.720984644, -1.855254573, 0.0)

CREATE_POLY_3(xH_SP2long, 0.0, 1.01,
	0.439598249, -0.444673076, 0.0)

CREATE_POLY_3(xH_SP3, 0.0, 1.06,
	1.761487842, -1.876959406, 0.0)

CREATE_POLY_7(xH_Ring, 0.7608, 1.089,
	37.744316, -117.731674, 143.0759275, -86.2258835, 26.7448175, -4.4699705, 0.6458455)

#undef CREATE_POLY_3
#undef CREATE_POLY_5
#undef CREATE_POLY_7
#undef CREATE_POLY_8

#endif // _SMP_HBOND_POLYNOMIALS_H_
