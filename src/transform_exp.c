#include <math.h>
#include "transform_exp.h"
#include "utils.h"


static const double NEAR_ZERO_ANGLE_THRESHOLD = 1e-7;


/**
 * Return the skew term S of the rotation matrix corresponding to the exponential coordinates \p v.
 */
void _skew(struct mol_matrix3 *s, const struct mol_vector3 v)
{
	s->m11 = 0;
	s->m12 = - v.Z;
	s->m13 = v.Y;
	s->m21 = v.Z;
	s->m22 = 0;
	s->m23 = - v.X;
	s->m31 = - v.Y;
	s->m32 = v.X;
	s->m33 = 0;
}


/**
 * Return the squared skew term S^2 of the rotation matrix corresponding to the exponential coordinates \p v.
 *
 */
void _skew_squared(struct mol_matrix3 *s, const struct mol_vector3 v)
{
	s->m11 = - v.Z * v.Z - v.Y * v.Y;
	s->m12 = v.X * v.Y;
	s->m13 = v.X * v.Z;
	s->m21 = s->m12;
	s->m22 = - v.Z * v.Z - v.X * v.X;
	s->m23 = v.Y * v.Z;
	s->m31 = s->m13;
	s->m32 = s->m23;
	s->m33 = - v.X * v.X - v.Y * v.Y;
}


/**
 * Return the derivative of the skew term of rotation matrix w.r.t. axis components.
 * Since components of the skew matrix linearly depend on axis components, we don't need axis itself.
 * So, this function just sets three matrices to the predefined values.
 *
 * \param ds_over_dv The pointer to the array of three \ref mol_matrix3 where the result should be written.
 */
static void _partial_skew(struct mol_matrix3 *ds_over_dv)
{
	smp_matrix_set_scalar(&ds_over_dv[0], 0);
	smp_matrix_set_scalar(&ds_over_dv[1], 0);
	smp_matrix_set_scalar(&ds_over_dv[2], 0);
	// ds / dx
	ds_over_dv[0].m23 = -1;
	ds_over_dv[0].m32 = 1;
	// ds / dy
	ds_over_dv[1].m13 = 1;
	ds_over_dv[1].m31 = -1;
	// ds / dz
	ds_over_dv[2].m12 = -1;
	ds_over_dv[2].m21 = 1;
}


/**
 * Compute the derivative of the square of the skew term of rotation matrix.
 * Unlike \ref _partial_skew, we need the exponential coordinates here.
 */
static void _partial_skew_squared(struct mol_matrix3 *ds2_over_dv, const struct mol_vector3 v)
{

	// dS^2 / dx
	ds2_over_dv[0].m11 = 0;
	ds2_over_dv[0].m12 = v.Y;
	ds2_over_dv[0].m13 = v.Z;
	ds2_over_dv[0].m21 = v.Y;
	ds2_over_dv[0].m22 = -2 * v.X;
	ds2_over_dv[0].m23 = 0;
	ds2_over_dv[0].m31 = v.Z;
	ds2_over_dv[0].m32 = 0;
	ds2_over_dv[0].m33 = -2 * v.X;
	// dS^2 / dy
	ds2_over_dv[1].m11 = -2 * v.Y;
	ds2_over_dv[1].m12 = v.X;
	ds2_over_dv[1].m13 = 0;
	ds2_over_dv[1].m21 = v.X;
	ds2_over_dv[1].m22 = 0;
	ds2_over_dv[1].m23 = v.Z;
	ds2_over_dv[1].m31 = 0;
	ds2_over_dv[1].m32 = v.Z;
	ds2_over_dv[1].m33 = -2 * v.Y;
	// dS^2 / dz
	ds2_over_dv[2].m11 = -2 * v.Z;
	ds2_over_dv[2].m12 = 0;
	ds2_over_dv[2].m13 = v.X;
	ds2_over_dv[2].m21 = 0;
	ds2_over_dv[2].m22 = -2 * v.Z;
	ds2_over_dv[2].m23 = v.Y;
	ds2_over_dv[2].m31 = v.X;
	ds2_over_dv[2].m32 = v.Y;
	ds2_over_dv[2].m33 = 0;
}


void smp_rmatr_from_exp(struct mol_matrix3 *r, const struct mol_vector3 v)
{
	// Below we're using notation explained in the overall module description at the top of the header file.
	// R == S^2 * (1 - cos(theta)) + S * sin(theta) + I
	struct mol_matrix3 m1, m2;
	const double theta_sq = MOL_VEC_SQ_NORM(v);
	const double theta = sqrt(theta_sq);
	double c1, c2;

	if (theta > NEAR_ZERO_ANGLE_THRESHOLD) {
		c1 = sin(theta) / theta;
		c2 = (1 - cos(theta)) / theta_sq;
	} else {
		// Use Taylor expansion
		c1 = 1 - theta_sq/ 6;
		c2 = 0.5 - theta_sq / 24;
	}

	// m1 = S(v) = S * theta
	_skew(&m1, v);
	// m1 = S(v) * sin(theta) / theta = S * sin(theta)
	smp_matrix_mult_scalar(&m1, c1);
	// m2 = S(v)^2 = S(w)^2 * theta^2 = (A(w) - I) * theta^2
	_skew_squared(&m2, v);
	// m2 = (A - I) * (1 - cos(theta))
	smp_matrix_mult_scalar(&m2, c2);
	// r = (A - I) * (1 - cos(theta)) + S * sin(theta)
	smp_matrix_sum(r, m1, m2);
	// r += I
	r->m11 += 1;
	r->m22 += 1;
	r->m33 += 1;
}


void smp_rmatr_derive_wrt_theta(
		struct mol_matrix3 *dr_over_dt,
		const struct mol_vector3 axis,
		const double theta)
{
	// dR / dtheta == S^2 * sin(theta) + S * cos(theta)
	const double l = sqrt(MOL_VEC_SQ_NORM(axis));
	if (l == 0) {
		smp_matrix_set_scalar(dr_over_dt, 0);
		return;
	}

	struct mol_vector3 w; // Unit axis
	MOL_VEC_DIV_SCALAR(w, axis, l);

	struct mol_matrix3 m1, m2;

	// m1 = S(w)
	_skew(&m1, w);
	// m1 = S * cos(theta)
	smp_matrix_mult_scalar(&m1, cos(theta));
	// m2 = S^2
	_skew_squared(&m2, w);
	// m2 = S^2 * sin(theta)
	smp_matrix_mult_scalar(&m2, sin(theta));
	smp_matrix_sum(dr_over_dt, m1, m2);
}


void smp_rmatr_derive_wrt_exp(struct mol_matrix3 *dr_over_dv, struct mol_vector3 v)
{
	// dR / dv = d(S^2(w) * (1 - cos(theta)) + S(w) * sin(theta) + I) / dv =
	//         = d(S^2(w)) / dv * (1 - cos(theta)) +
	//         + S^2(w) * d(1 - cos(theta)) / dv +
	//         + d(S(w)) / dv * sin(theta)
	//         + S(w) * d(sin(theta)) / dv =
	//
	//     [substituting S(w) = S(v) / theta]
	//         = d(S^2(v) / theta^2) / dv * (1 - cos(theta)) +
	//         + S^2(v) / theta^2 * d(1 - cos(theta)) / dv +
	//         + d(S(v) / theta) / dv * sin(theta) +
	//         + S(v) / theta * d(sin(theta)) / dv =
	//
	//         = {d(S^2(v)) / dv * 1 / theta^2 - S^2(v) * 2 / theta^3 * d(theta) / dv} * (1-cos(theta)) +
	//          + S^2(v) / theta^2 * sin(theta) * d(theta) / dv +
	//          + {d(S(v)) / dv - S(v) / theta^2 * d(theta) / dv} * sin(theta) +
	//          + S(v) * cos(theta) * d(theta) / dv =
	//
	//     [since theta == |v|, we can substitute  d(theta) / dv = v / theta (= w)]
	//         = {d(S^2(v)) / dv * 1 / theta^2 - S^2(v) * 2 * v / theta^4} * (1-cos(theta)) +
	//          + S^2(v) / theta^3 * sin(theta) * v +
	//          + {d(S(v)) / dv - S(v) / theta^3 * v} * sin(theta) +
	//          + S(v) * cos(theta) / theta * v =
	//
	//     [finally, we group together the terms starting with d(S^2(v))/dv, d(S(v))/dv, S^2(v), S(v)]
	//         = d(S^2(v))/dv * (1 - cos(theta)) / theta^2 +                        [Term 1]
	//         + dS(v)/dv * sin(theta) / theta +                                    [Term 2]
	//         + S^2(v) * (v/theta^3 * sin(theta) - 2v/theta^4 * (1-cos(theta))) +  [Term 3]
	//         + S(v) * (v/theta^2 * cos(theta) - v/theta^3 * sin(theta)            [Term 4]
	// And so we go:
	double theta_sq = MOL_VEC_SQ_NORM(v);
	double theta = sqrt(theta_sq);

	double prefactor_1, prefactor_2;
	struct mol_vector3 prefactor_3, prefactor_4;

	if (theta >= NEAR_ZERO_ANGLE_THRESHOLD) {
		prefactor_1 = (1 - cos(theta)) / theta_sq;
		prefactor_2 = sin(theta) / theta;
		// Calculate prefactor for Term 3: v * (sin(theta) - 2*(1-cos(theta))/theta) / theta^3
		double theta_3 = theta * theta * theta;
		double tmp1 = sin(theta) - 2 * (1 - cos(theta)) / theta;
		MOL_VEC_MULT_SCALAR(prefactor_3, v, tmp1 / theta_3);
		// Calculate prefactor for Term 4: v * (cos(theta) - sin(theta)/theta) / theta^2
		double tmp2 = cos(theta) - sin(theta) / theta;
		MOL_VEC_MULT_SCALAR(prefactor_4, v, tmp2 / theta_sq);
	} else {
		// Use Taylor series
		prefactor_1 = 0.5 - theta_sq / 24;
		prefactor_2 = 1 - theta_sq / 6;
		MOL_VEC_MULT_SCALAR(prefactor_3, v, -1. / 12);
		MOL_VEC_MULT_SCALAR(prefactor_4, v, -1. / 3);
	}

	struct mol_matrix3 ds[3];
	struct mol_matrix3 ds2[3];

	// dS(v)^2 / dv = dA(w)/dv - I * d(theta^2) / dv
	_partial_skew_squared(ds2, v);
	// ds2 = dS^2(v)/dv * (1-cos(theta)) / theta^2, we get [Term 1]
	smp_matrix3_mult_scalar(ds2, prefactor_1);

	// dS(v) / dv
	_partial_skew(ds);
	// ds = dS(v)/dv * sin(theta) / theta, we get [Term 2]
	smp_matrix3_mult_scalar(ds, prefactor_2);

	struct mol_matrix3 s[3];
	struct mol_matrix3 s2[3];

	// S(v) == S(w) * theta
	_skew(&s[0], v);
	// S(v)^2 == (A(w)-I) * theta^2
	_skew_squared(&s2[0], v);
	// Make several copies of S(v) and S(v)^2 for future use.
	for (int i = 1; i < 3; ++i) {
		s[i] = s[0];
		s2[i] = s2[0];
	}

	// S^2(v) * prefactor_3, we get [Term 3]
	smp_matrix_mult_scalar(&s2[0], prefactor_3.X);
	smp_matrix_mult_scalar(&s2[1], prefactor_3.Y);
	smp_matrix_mult_scalar(&s2[2], prefactor_3.Z);
	// S(v) * prefactor_4, we get [Term 4]
	smp_matrix_mult_scalar(&s[0], prefactor_4.X);
	smp_matrix_mult_scalar(&s[1], prefactor_4.Y);
	smp_matrix_mult_scalar(&s[2], prefactor_4.Z);

	for (int i = 0; i < 3; ++i) {
		smp_matrix_sum(&dr_over_dv[i], ds2[i], ds[i]);  // [Term 1] + [Term 2]
		smp_matrix_sum(&dr_over_dv[i], dr_over_dv[i], s2[i]); // + [Term 3]
		smp_matrix_sum(&dr_over_dv[i], dr_over_dv[i], s[i]); // + [Term 4]
	}
}


struct mol_quaternion smp_quaternion_from_exp(const struct mol_vector3 v)
{
	double angle = sqrt(MOL_VEC_SQ_NORM(v));
	struct mol_vector3 w = {.X = 0, .Y = 0, .Z = 1}; // Unit axis
	// If angle == 0, initialize to some arbitrary vector
	if (angle != 0) {
		MOL_VEC_DIV_SCALAR(w, v, angle);
	}
	struct mol_quaternion q;
	mol_quaternion_from_axis_angle(&q, &w, angle);
	return q;
}


struct mol_vector3 smp_quaternion_to_exp(const struct mol_quaternion q)
{
	double angle;
	struct mol_vector3 v;
	mol_quaternion_to_axis_angle(&v, &angle, &q);
	MOL_VEC_MULT_SCALAR(v, v, angle);
	return v;
}
