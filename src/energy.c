#include "energy.h"
#include "hybridization.h"
#include <math.h>
#include <mol2/pwpot.h>

static const double VDW_CUTOFF_LINE_SCALE = 0.6; //!< unitless
static const double VDW_CUTOFF_LINE_SCALE_2 = 0.6*0.6; //!< unitless, VDW_CUTOFF_LINE_SCALE^2
static const double VDW_CUTOFF_LINE_SCALE_6 = 0.6*0.6*0.6*0.6*0.6*0.6; //!< unitless, VDW_CUTOFF_LINE_SCALE^6
static const double VDW_CUTOFF_LINE_SCALE_MIN_6 = 1.0/(0.6*0.6*0.6*0.6*0.6*0.6); //!< unitless, VDW_CUTOFF_LINE_SCALE^(-6)
static const double VDW_CUTOFF_LINE_SCALE_MIN_12 = (1.0/(0.6*0.6*0.6*0.6*0.6*0.6*0.6*0.6*0.6*0.6*0.6*0.6)); //!< unitless, VDW_CUTOFF_LINE_SCALE^(-12)

static const double COUL_CCELEC = 332.0716; //!< (kcal/mol) * Angstrom / e^2, where e is elementary charge
static const double COUL_MIN_CUTOFF_SQ = 9.0; //!< Angstrom^2
static const double COUL_SHORT_RANGE_SQ = 25.0; //!< Angstrom^2

// Calculate linearized Van der Waals energy for pair of atoms
static void _vdweng_rep_line(
	struct mol_atom_group *ag,
	double *energy_rep,
	double *energy_atr,
	const double weight_rep,
	const double weight_atr,
	const double ff_cutoff2,
	const int atom_i,
	const int atom_j,
	const double eps_ij,
	const double radius_ij);

// Calculate linearized term of Van der Waals energy and gradient multiplier in short-range regions
static double _vdweng_rep_line_short(
	double *gradient_mult,
	const double eij,
	const double rij,
	const double rc2,
	const double d2,
	const double weight);

// Calculate "classical" Van der Waals energy and gradient multiplier
static double _vdweng_rep_line_long(
	double *gradient_mult,
	const double eij,
	const double rij,
	const double rc2,
	const double d2,
	const double weight);

// Calculate Coulomb energy and gradient multiplier for pair
static void _coul_eng_rdie_for_pair(double *energy, double *gradient_mult, const double cutoff, const double charge,
	const double d2,
	const double weight);


// Return the third degree of fraction with a input numerator and denominator
static double _pow3(const double nominator, const double denominator);

void smp_energy_dars(
		struct mol_atom_group *ag,
		double *energy,
		const struct mol_prms *atomprm,
		const struct nblist *nblst,
		const double weight)
{
	fprintf(stderr, "WARNING: smp_energy_dars is deprecated. Use mol_pwpot_eng instead.\n");
	mol_pwpot_eng(ag, energy, atomprm, nblst, weight);
}


void smp_energy_vdw_rep_line(
		struct mol_atom_group *ag,
		double *energy_rep,
		double *energy_atr,
		const struct nblist *nblst,
		const double weight_rep,
		const double weight_atr)
{
	const double ff_cutoff2 = (nblst->nbcof) * (nblst->nbcof);

	for (int i = 0; i < nblst->nfat; i++) {
		const int atom_i = nblst->ifat[i];
		for (int j = 0; j < nblst->nsat[i]; j++) {
			const int atom_j = nblst->isat[i][j];

			const double eps_ij = ag->eps[atom_i] * ag->eps[atom_j];
			const double radius_ij = ag->vdw_radius[atom_i] + ag->vdw_radius[atom_j];

			struct mol_vector3 dist;
			MOL_VEC_SUB(dist, ag->coords[atom_i], ag->coords[atom_j]);
			const double dist2 = MOL_VEC_SQ_NORM(dist);

			if (dist2 < ff_cutoff2) {
				_vdweng_rep_line(ag, energy_rep, energy_atr, weight_rep, weight_atr,
					ff_cutoff2, atom_i, atom_j, eps_ij, radius_ij);
			}
		}
	}
}


void smp_energy_vdw03_rep_line(
		struct mol_atom_group *ag,
		double *energy_rep,
		double *energy_atr,
		const struct nblist *nblst,
		const int n03,
		const int *list03,
		const double weight_rep,
		const double weight_atr)
{
	const double ff_cutoff2 = (nblst->nbcof) * (nblst->nbcof);

	for (int i = 0; i < n03; i++) {
		const int atom_i = list03[2*i];
		const int atom_j = list03[2*i+1];
		const double eps_ij = ag->eps03[atom_i] * ag->eps03[atom_j];
		const double radius_ij = ag->vdw_radius03[atom_i] + ag->vdw_radius03[atom_j];

		if (atom_i == atom_j)
			continue;

		_vdweng_rep_line(ag, energy_rep, energy_atr, weight_rep, weight_atr,
			ff_cutoff2, atom_i, atom_j, eps_ij, radius_ij);
	}
}

void smp_energy_vdw_rep_line_ligand(
		struct mol_atom_group *ag,
		double *energy,
		const struct nblist *nblst,
		const int ligand_atom_first,
		const int ligand_atom_last,
		const double weight_atr,
		const double weight_rep)
{
	(*energy) = 0;

	const double ff_cutoff2 = (nblst->nbcof) * (nblst->nbcof);

	for (int i = 0; i < nblst->nfat; i++) {
		const int atom_i = nblst->ifat[i];
		const bool atom_i_in_ligand = (atom_i >= ligand_atom_first && atom_i <= ligand_atom_last);
		for (int j = 0; j < nblst->nsat[i]; j++) {
			const int atom_j = nblst->isat[i][j];
			// Look for interactions only between a moving ligand and the rest of the system
			const bool atom_j_in_ligand = (atom_j >= ligand_atom_first && atom_j <= ligand_atom_last);
			if ((atom_i_in_ligand && !atom_j_in_ligand) || (!atom_i_in_ligand && atom_j_in_ligand)) {
				double e_r = 0, e_a = 0;

				const double eps_ij = ag->eps[atom_i] * ag->eps[atom_j];
				const double radius_ij = ag->vdw_radius[atom_i] + ag->vdw_radius[atom_j];

				struct mol_vector3 dist;
				MOL_VEC_SUB(dist, ag->coords[atom_i], ag->coords[atom_j]);
				const double dist2 = MOL_VEC_SQ_NORM(dist);

				if (dist2 < ff_cutoff2)
					_vdweng_rep_line(ag, &e_r, &e_a, weight_rep, weight_atr,
						ff_cutoff2, atom_i, atom_j, eps_ij, radius_ij);

				*energy += e_a + e_r;
			}
		}
	}
}

void smp_energy_coul_rdie_shift(
		struct mol_atom_group *ag,
		double *energy_short,
		double *energy_long,
		const struct nblist *nblst,
		const double weight_short,
		const double weight_long)
{
	for (int i = 0; i < nblst->nfat; i++) {
		const int atom_i = nblst->ifat[i];
		struct mol_vector3 ivec;
		MOL_VEC_COPY(ivec, ag->coords[atom_i]);

		for (int j = 0; j < nblst->nsat[i]; j++) {
			const int atom_j = nblst->isat[i][j];
			const double charge = COUL_CCELEC * (ag->charge[atom_i]) * (ag->charge[atom_j]);
			struct mol_vector3 dist;
			MOL_VEC_SUB(dist, ivec, ag->coords[atom_j]);
			const double dist2 = MOL_VEC_SQ_NORM(dist);
			double dven_r = 0;
			double energy;
			const double ff_cutoff = nblst->nbcof;

			if (dist2 < COUL_MIN_CUTOFF_SQ) {
				_coul_eng_rdie_for_pair(&energy, &dven_r, ff_cutoff, charge, COUL_MIN_CUTOFF_SQ,
					weight_short);
				(*energy_short) += energy;
				// FIXME: in this case energy is constant, but gradients are not zero. Issue #14
			} else {
				if (dist2 < COUL_SHORT_RANGE_SQ) {
					_coul_eng_rdie_for_pair(&energy, &dven_r, ff_cutoff, charge, dist2, weight_short);
					(*energy_short) += energy;
				} else if (dist2 < (ff_cutoff*ff_cutoff)) {
					_coul_eng_rdie_for_pair(&energy, &dven_r, ff_cutoff, charge, dist2, weight_long);
					(*energy_long) += energy;
				}
				struct mol_vector3 g;
				MOL_VEC_MULT_SCALAR(g, dist, dven_r);
				MOL_VEC_ADD(ag->gradients[atom_i], ag->gradients[atom_i], g);
				MOL_VEC_SUB(ag->gradients[atom_j], ag->gradients[atom_j], g);
			}
		}
	}
}

void smp_energy_restraints_fixedpoint(
		struct mol_atom_group *ag,
		double *energy,
		const struct mol_index_list atom_list,
		const struct mol_vector3 *reference_coords,
		const double weight)
{
	for (size_t i = 0; i < atom_list.size; i++) {
		const size_t iatom = atom_list.members[i];
		struct mol_vector3 dist;
		MOL_VEC_SUB(dist, ag->coords[iatom], reference_coords[i]);

		double r2 = MOL_VEC_SQ_NORM(dist);

		(*energy) += weight * r2;

		struct mol_vector3 grad;
		MOL_VEC_MULT_SCALAR(grad, dist, -2*weight);
		MOL_VEC_ADD(ag->gradients[iatom], ag->gradients[iatom], grad);
	}
}


void _vdweng_rep_line(
		struct mol_atom_group *ag,
		double *energy_rep,
		double *energy_atr,
		const double weight_rep,
		const double weight_atr,
		const double ff_cutoff2,
		const int atom_i,
		const int atom_j,
		const double eps_ij,
		const double radius_ij)
{
	const double cutoff_line_2 = VDW_CUTOFF_LINE_SCALE_2 * radius_ij * radius_ij;
	double dven_r;

	struct mol_vector3 dist;
	MOL_VEC_SUB(dist, ag->coords[atom_i], ag->coords[atom_j]);
	const double dist2 = MOL_VEC_SQ_NORM(dist);

	if (dist2 < cutoff_line_2) {
		(*energy_rep) += _vdweng_rep_line_short(&dven_r, eps_ij, radius_ij,
			ff_cutoff2, dist2, weight_rep);
	} else {
		const double cutoff_rep_2 = radius_ij * radius_ij;

		// The only difference here is which weight (atr or rep) we use, and what energy we add to
		if (dist2 < cutoff_rep_2) {
			(*energy_rep) += _vdweng_rep_line_long(&dven_r, eps_ij, radius_ij,
				ff_cutoff2, dist2, weight_rep);
		} else {
			(*energy_atr) += _vdweng_rep_line_long(&dven_r, eps_ij, radius_ij,
				ff_cutoff2, dist2, weight_atr);
		}
	}
	struct mol_vector3 g;
	MOL_VEC_MULT_SCALAR(g, dist, dven_r);
	MOL_VEC_ADD(ag->gradients[atom_i], ag->gradients[atom_i], g);
	MOL_VEC_SUB(ag->gradients[atom_j], ag->gradients[atom_j], g);
}


double _vdweng_rep_line_short(
		double *gradient_mult,
		const double eij,
		const double rij,
		const double rc2,
		const double d2,
		const double weight)
{
	const double rij2 = rij * rij;
	const double r_r6 = _pow3(rij2, rc2);
	const double r_r12 = r_r6 * r_r6;
	const double d = sqrt(d2);
	const double a = VDW_CUTOFF_LINE_SCALE_MIN_12 - 2 * VDW_CUTOFF_LINE_SCALE_MIN_6 + r_r6 * (4.0 - 2 * VDW_CUTOFF_LINE_SCALE_6 * r_r6) + r_r12 * (2 * VDW_CUTOFF_LINE_SCALE_6 * r_r6 - 3.0);
	const double b = 12.0 * (-VDW_CUTOFF_LINE_SCALE_MIN_12 + VDW_CUTOFF_LINE_SCALE_MIN_6 + VDW_CUTOFF_LINE_SCALE_6 * r_r6 * (r_r12 - r_r6)) / (VDW_CUTOFF_LINE_SCALE * rij);

	(*gradient_mult) = -weight * eij * b / d;
	return weight * eij * (a + (d - VDW_CUTOFF_LINE_SCALE * rij) * b);
}


double _vdweng_rep_line_long(
		double *gradient_mult,
		const double eij,
		const double rij,
		const double rc2,
		const double d2,
		const double weight)
{
	const double rij2 = rij * rij;
	const double r_r6 = _pow3(rij2, rc2);
	const double r_r12 = r_r6 * r_r6;
	const double r_d6 = _pow3(rij2, d2);
	const double r_d12 = r_d6 * r_d6;
	const double dr6 = _pow3(d2, rc2);

	(*gradient_mult) = -weight * eij * 12 * (-r_d12 + r_d6 + dr6 * (r_r12 - r_r6)) / d2;
	return weight * eij * (r_d12 - 2 * r_d6 + r_r6 * (4.0 - 2 * dr6) + r_r12 * (2 * dr6 - 3.0));
}


void _coul_eng_rdie_for_pair(
		double *energy,
		double *gradient_mult,
		const double cutoff,
		const double charge,
		const double d2,
		const double weight)
{
	double d = sqrt(d2);
	double esh = (1.0 - d/cutoff)*(1.0 - d/cutoff);
	(*energy) = weight*charge*esh/d2;
	double desh = 2.0 / (d2*d) * (1.0/d*(1.0-d/cutoff)*(1.0-d/cutoff) + 1.0/cutoff*(1.0-d/cutoff));
	(*gradient_mult) = weight*charge*desh;
}


double _pow3(const double nominator, const double denominator)
{
	double p = nominator / denominator;
	return p * p * p;
}
