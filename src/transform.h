#ifndef _SMP_TRANSFORM_H_
#define _SMP_TRANSFORM_H_

#include <mol2/vector.h>
#include <mol2/matrix.h>
#include <mol2/quaternion.h>

#define SMP_VEC_FROM_ARRAY(DST_VEC, ARRAY, IDX) do  \
	{                                           \
		(DST_VEC).X = (ARRAY)[3*(IDX) + 0]; \
		(DST_VEC).Y = (ARRAY)[3*(IDX) + 1]; \
		(DST_VEC).Z = (ARRAY)[3*(IDX) + 2]; \
	} while(0)

#define SMP_VEC_TO_ARRAY(DST_ARRAY, DST_IDX, VEC) do    \
	{                                               \
		(DST_ARRAY)[3*(DST_IDX) + 0] = (VEC).X; \
		(DST_ARRAY)[3*(DST_IDX) + 1] = (VEC).Y; \
		(DST_ARRAY)[3*(DST_IDX) + 2] = (VEC).Z; \
	} while(0)

#define SMP_QUATERNION_MULT_SCALAR(DST, U, C) do \
	{                                        \
		(DST).W = (U).W * (C);           \
		MOL_VEC_MULT_SCALAR(DST, U, C);  \
	} while(0)

#define SMP_QUATERNION_ADD(DST, U, V) do \
	{                                \
		(DST).W = (U).W + (V).W; \
		MOL_VEC_ADD(DST, U, V);  \
	} while(0)

/**
 * This function rotates vector \p v according to quaternion \p q. All according to Wikipedia.
 * \param v[in,out] Vector to rotate.
 * \param q[in] Rotation quaternion.
 */
void smp_vector3_rotate_by_quaternion(struct mol_vector3 *v, const struct mol_quaternion *q);

/**
 * Normalize vector \p v. Return its original length.
 * \param v Vector to normalize (inplace).
 * \return Length of \p v before normalization.
 */
double smp_vector3_normalize(struct mol_vector3 *v);

/**
 * Multiply two matrices.
 * \return Matrix product \p ma * \p mb.
 */
struct mol_matrix3 smp_matrix3_mul(const struct mol_matrix3 *ma, const struct mol_matrix3 *mb);

/**
 * Initialize matrix \p m as 3*3 identity matrix.
 */
void smp_matrix3_eye(struct mol_matrix3 *m);

#endif //_SMP_TRANSFORM_H_
