#include <mol2/quaternion.h>
#include <mol2/transform.h>
#include <math.h>
#include "utils.h"

void smp_str_trim_and_copy(char *dst, const char *src, const size_t n)
{
	while (*src == ' ') src++;
	size_t j;
	for (j = 0; j < n-1 && src[j] != ' ' && src[j] != '\0'; j++)
		dst[j] = src[j];
	dst[j] = '\0';
}


void smp_scale_vdw_radius(struct mol_atom_group *ag, const double scale)
{
	for (size_t i = 0; i < ag->natoms; i++){
		ag->vdw_radius[i] *= scale;
	}
}

void smp_matrix_mult_scalar(struct mol_matrix3 *Z, const double c)
{
	Z->m11 = Z->m11 * c;
	Z->m12 = Z->m12 * c;
	Z->m13 = Z->m13 * c;
	Z->m21 = Z->m21 * c;
	Z->m22 = Z->m22 * c;
	Z->m23 = Z->m23 * c;
	Z->m31 = Z->m31 * c;
	Z->m32 = Z->m32 * c;
	Z->m33 = Z->m33 * c;
}

void smp_matrix3_mult_scalar(struct mol_matrix3 *Z, const double c)
{
	for (int i = 0; i < 3; ++i)
		smp_matrix_mult_scalar(&Z[i], c);
}


void smp_matrix_sum(struct mol_matrix3 *Z, const struct mol_matrix3 X, const struct mol_matrix3 Y)
{
	Z->m11 = X.m11 + Y.m11;
	Z->m12 = X.m12 + Y.m12;
	Z->m13 = X.m13 + Y.m13;
	Z->m21 = X.m21 + Y.m21;
	Z->m22 = X.m22 + Y.m22;
	Z->m23 = X.m23 + Y.m23;
	Z->m31 = X.m31 + Y.m31;
	Z->m32 = X.m32 + Y.m32;
	Z->m33 = X.m33 + Y.m33;
}

void smp_matrix_set_scalar(struct mol_matrix3 *Z, const double c)
{
	Z->m11 = c; Z->m12 = c; Z->m13 = c;
	Z->m21 = c; Z->m22 = c; Z->m23 = c;
	Z->m31 = c; Z->m32 = c; Z->m33 = c;
}

void smp_vdw_radius_to_array(double *array, const struct mol_atom_group *ag)
{
	for (size_t i = 0; i < ag->natoms; i++){
		array[i] = ag->vdw_radius[i];
	}
}


void smp_vdw_radius_from_array(struct mol_atom_group *ag, const double *array)
{
	for (size_t i = 0; i < ag->natoms; i++){
		ag->vdw_radius[i] = array[i];
	}
}


void smp_atom_group_init_backbone_by_name(struct mol_atom_group *ag)
{
	char type[5];
	if (ag->backbone == NULL)
		ag->backbone = calloc(ag->natoms, sizeof(bool));
	for (size_t i = 0; i < ag->natoms; i++) {
		smp_str_trim_and_copy(type, ag->atom_name[i], 4);
		if (!strcmp(type, "C") || !strcmp(type, "CA") || !strcmp(type, "N") ||
		    !strcmp(type, "O") || !strcmp(type, "H"))
			ag->backbone[i] = true;
		else
			ag->backbone[i] = false;
	}
}


void smp_atom_list_translate(
		struct mol_atom_group *ag,
		const struct mol_vector3 *t,
		const struct mol_index_list atom_list)
{
	for (size_t j = 0; j < atom_list.size; j++) {
		const size_t i = atom_list.members[j];
		MOL_VEC_ADD(ag->coords[i], ag->coords[i], *t);
	}
}

void smp_atom_list_rotate(
		struct mol_atom_group *ag,
		const struct mol_quaternion *q,
		const struct mol_vector3 *center,
		const struct mol_index_list atom_list)
{

	struct mol_matrix3 rot_matrix;
	mol_matrix3_from_quaternion(&rot_matrix, *q);

	for (size_t j = 0; j < atom_list.size; j++) {
		const size_t i = atom_list.members[j];
		struct mol_vector3 tmp;
		MOL_VEC_SUB(tmp, ag->coords[i], *center);
		mol_vector3_rotate(&tmp, &rot_matrix);
		MOL_VEC_ADD(ag->coords[i], tmp, *center);
	}
}

struct mol_vector3 smp_atom_list_centroid(
		const struct mol_atom_group *ag,
		const struct mol_index_list atom_list)
{
	struct mol_vector3 ret = { 0 };
	for (size_t j = 0; j < atom_list.size; j++) {
		const size_t i = atom_list.members[j];
		MOL_VEC_ADD(ret, ret, ag->coords[i]);
	}
	MOL_VEC_DIV_SCALAR(ret, ret, atom_list.size);
	return ret;
}

static int _cmp_size_t(const void *s1, const void *s2)
{
	const size_t v1 = *((const size_t *)s1);
	const size_t v2 = *((const size_t *)s2);
	if (v1 < v2)
		return -1;
	else if (v1 > v2)
		return 1;
	else
		return 0;
}

void smp_create_interface_residues_list(
		struct mol_index_list *interface_residues,
		const struct mol_atom_group* ag,
		const size_t first_ligand_atom,
		const size_t last_ligand_atom,
		const double cutoff,
		const bool alloc)
{
	const double cutoff_squared = cutoff*cutoff;

	size_t first_ligand_residue = SIZE_MAX;
	size_t last_ligand_residue = SIZE_MAX;

	/* First, we find first and last residues of the ligand based on its first and last atom
	 * This loop runs in O(nresidues). As atoms and residues are typically sorted, it can be changed to run in
	 * O(log(nresidues)), but it's probably not worth the effort.
	 * */
	for (size_t resid_i = 0; resid_i < ag->nresidues; resid_i++) { // Loop over residues
		const size_t resid_i_end = ag->residue_list[resid_i]->atom_end;
		if (resid_i_end >= first_ligand_atom) { // This residue is part of ligand
			first_ligand_residue = resid_i;
			last_ligand_residue = ag->nresidues - 1; // Temporary, unless we found a better upper bound
			for (size_t resid_j = resid_i; resid_j < ag->nresidues; resid_j++) {
				const size_t resid_j_start = ag->residue_list[resid_j]->atom_start;
				if (resid_j_start > last_ligand_atom) {
					last_ligand_residue = resid_j - 1;
					break;
				}
			}
			break;
		}
	}

	if (alloc) {
		interface_residues->members = calloc(ag->nresidues, sizeof(size_t));
	}

	if (first_ligand_residue == SIZE_MAX || last_ligand_residue == SIZE_MAX) {
		PRINTF_ERROR("Unable to determine indexes of ligand residues!");
		exit(EXIT_FAILURE);
	}

	size_t count = 0;

	for (size_t resid_i = 0; resid_i < ag->nresidues; resid_i++) { // Loop over residues
		if (resid_i >= first_ligand_residue && resid_i <= last_ligand_residue)
			continue;
		const size_t resid_i_start = ag->residue_list[resid_i]->atom_start;
		const size_t resid_i_end = ag->residue_list[resid_i]->atom_end;
		for (size_t atom_i = resid_i_start; atom_i <= resid_i_end; atom_i++) {
			for (size_t resid_j = first_ligand_residue; resid_j <= last_ligand_residue; resid_j++) {
				const size_t resid_j_start = ag->residue_list[resid_j]->atom_start;
				const size_t resid_j_end = ag->residue_list[resid_j]->atom_end;
				for (size_t atom_j = resid_j_start; atom_j <= resid_j_end; atom_j++) {
					const double r2 = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[atom_i], ag->coords[atom_j]);
					if (r2 <= cutoff_squared) {
						bool already_have_resid_i = false;
						bool already_have_resid_j = false;
						for (size_t k = 0; k < count; k++) {
							if (interface_residues->members[k] == resid_i)
								already_have_resid_i = true;
							if (interface_residues->members[k] == resid_j)
								already_have_resid_j = true;
						}
						if (!already_have_resid_i)
							interface_residues->members[count++] = resid_i;
						if (!already_have_resid_j)
							interface_residues->members[count++] = resid_j;
					}
				}
			}
		}
	}

	interface_residues->size = count;
	// Sort the list to keep it tidy UwU
	qsort(interface_residues->members, count, sizeof(size_t), _cmp_size_t);

	if (alloc) {
		interface_residues->members = realloc(interface_residues->members,
			sizeof(size_t) * interface_residues->size);
	}
}

// To avoid dealing with dynamic buffer allocation in this function we set this (arbitrary) boundary
#define MAX_ELEM_LENGTH 10
void smp_get_atom_list_by_element(
		struct mol_index_list *atom_list,
		const struct mol_atom_group *ag,
		const char *element,
		const bool alloc)
{
	if (strlen(element) > MAX_ELEM_LENGTH) {
		PRINTF_ERROR("Elements with names longer than %d are not supported. Please, change the value of MAX_ELEM_LENGTH in %s and rebuild libsampling.", MAX_ELEM_LENGTH, __FILE__);
		exit(EXIT_FAILURE);
	}

	if (alloc)
		atom_list->members = calloc(ag->natoms, sizeof(size_t));
	size_t count = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		char cur_element[MAX_ELEM_LENGTH+1];
		smp_str_trim_and_copy(cur_element, ag->element[i], MAX_ELEM_LENGTH);
		if (!strcmp(element, cur_element)) {
			atom_list->members[count++] = i;
		}
	}
	atom_list->size = count;
	if (alloc)
		atom_list->members = realloc(atom_list->members, count * sizeof(size_t));
}
#undef MAX_ELEM_LENGTH

void smp_freeze_all_unfreeze_from_list(struct mol_atom_group *ag, const struct mol_index_list atom_list)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		ag->fixed[i] = true;
	}

	for (size_t i = 0; i < atom_list.size; i++) {
		const size_t j = atom_list.members[i];
		ag->fixed[j] = false;
	}
	mol_fixed_update_active_lists(ag);
}

void smp_coords_to_array(
		struct mol_vector3 *dst,
		const struct mol_atom_group *ag,
		const struct mol_index_list *atom_list)
{
	if (atom_list == NULL) {
		for (size_t i = 0; i < ag->natoms; i++) {
			MOL_VEC_COPY(dst[i], ag->coords[i]);
		}
	} else {
		for (size_t j = 0; j < atom_list->size; j++) {
			size_t i = atom_list->members[j];
			MOL_VEC_COPY(dst[j], ag->coords[i]);
		}
	}
}

void smp_coords_from_array(
		struct mol_atom_group *ag,
		const struct mol_vector3 *src,
		const struct mol_index_list *atom_list)
{
	if (atom_list == NULL) {
		for (size_t i = 0; i < ag->natoms; i++) {
			MOL_VEC_COPY(ag->coords[i], src[i]);
		}
	} else {
		for (size_t j = 0; j < atom_list->size; j++) {
			size_t i = atom_list->members[j];
			MOL_VEC_COPY(ag->coords[i], src[j]);
		}
	}
}

bool smp_atom_group_has_nan_coordinates(const struct mol_atom_group *ag)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		if (!isfinite(ag->coords[i].X) ||
		    !isfinite(ag->coords[i].Y) ||
		    !isfinite(ag->coords[i].Z)) {
			return true;
		}
	}
	return false;
}
