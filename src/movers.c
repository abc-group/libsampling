#include "movers.h"
#include "utils.h"
#include "energy.h"
#include "prng.h"
#include "rigforest.h"
#include <math.h>

// Warning: currently this function assumes that \p list is sorted and has no gaps!
// It imposes these restriction of smp_movers_slide_into_contact_backbone
static bool _is_residue_in_list(const struct mol_atom_group *ag, const size_t res_num, const struct mol_index_list list)
{
	const size_t first_list_atom = list.members[0];
	const size_t last_list_atom = list.members[list.size-1];
	size_t first_residue_atom = ag->residue_list[res_num]->atom_start;
	size_t last_residue_atom = ag->residue_list[res_num]->atom_end;

	return (first_residue_atom >= first_list_atom && last_residue_atom <= last_list_atom);
}

static void _interface_centers(
		const struct mol_atom_group *ag,
		const struct mol_index_list lig_atoms,
		const struct mol_index_list interface_residues,
		struct mol_vector3 *rec_center,
		struct mol_vector3 *lig_center)
{
	size_t num_of_atoms_rec = 0;
	size_t num_of_atoms_lig = 0;
	MOL_VEC_SET_SCALAR(*rec_center, 0);
	MOL_VEC_SET_SCALAR(*lig_center, 0);

	for (size_t i = 0; i < interface_residues.size; i++) {
		const size_t res_num = interface_residues.members[i];
		size_t first_atom = ag->residue_list[res_num]->atom_start;
		size_t last_atom = ag->residue_list[res_num]->atom_end;

		if (!_is_residue_in_list(ag, res_num, lig_atoms)) { // Receptor residue
			for (size_t iatom = first_atom; iatom <= last_atom; iatom++) {
				MOL_VEC_ADD(*rec_center, *rec_center, ag->coords[iatom]);
				num_of_atoms_rec++;
			}
		} else { // Ligand residue
			for (size_t iatom = first_atom; iatom <= last_atom; iatom++) {
				MOL_VEC_ADD(*lig_center, *lig_center, ag->coords[iatom]);
				num_of_atoms_lig++;
			}

		}
	}

	if (num_of_atoms_lig == 0) {
		PRINTF_ERROR("Interface contains no ligand atoms!");
		exit(EXIT_FAILURE);
	}
	if (num_of_atoms_rec == 0) {
		PRINTF_ERROR("Interface contains no receptor atoms!");
		exit(EXIT_FAILURE);
	}
	MOL_VEC_DIV_SCALAR(*rec_center, *rec_center, num_of_atoms_rec);
	MOL_VEC_DIV_SCALAR(*lig_center, *lig_center, num_of_atoms_lig);
}

static void _turn_off_vdw_on_selected_side_chains(struct mol_atom_group *ag, const struct mol_index_list residues)
{
	for (size_t i = 0; i < residues.size; i++) {
		const size_t ires = residues.members[i];
		const bool is_ala = ! strcmp(ag->residue_name[ires], "ALA");
		const bool is_gly = ! strcmp(ag->residue_name[ires], "GLY");
		const bool is_pro = ! strcmp(ag->residue_name[ires], "PRO");
		if (!(is_ala || is_gly || is_pro)) {
			size_t first_atom = ag->residue_list[ires]->atom_start;
			size_t last_atom = ag->residue_list[ires]->atom_end;
			for (size_t iatom = first_atom; iatom <= last_atom; iatom++) {
				if (mol_is_sidechain(ag, iatom, NULL))
					ag->vdw_radius[iatom] = 0;
			}
		}
	}
}


unsigned int smp_movers_slide_into_contact_backbone(
		struct mol_atom_group *ag,
		struct agsetup *ags,
		struct acesetup *acs,
		struct mol_vector3 *t,
		const struct mol_index_list lig_atom_list,
		const struct mol_index_list interface_residue_list,
		const double step,
		const double energy_threshold_pull,
		const double energy_threshold_push)
{
	static const double en_weight_atr = 1.0, en_weight_rep = 1.0;
	struct mol_vector3 center_rec, center_lig;

	if (lig_atom_list.size == 0) {
		MOL_VEC_SET_SCALAR(*t, 0);
		return 0;
	}

	const size_t ligand_atom_first = lig_atom_list.members[0];
	const size_t ligand_atom_last = lig_atom_list.members[lig_atom_list.size - 1];

	_interface_centers(ag, lig_atom_list, interface_residue_list,
	                   &center_rec, &center_lig);

	MOL_VEC_SUB(*t, center_lig, center_rec);

	// Normalize and scale the translation vector
	const double r = step / sqrt(MOL_VEC_SQ_NORM(*t));
	MOL_VEC_MULT_SCALAR(*t, *t, r);

	check_clusterupdate(ag, ags);

	double *original_vdw_radii = calloc(ag->natoms, sizeof(double));
	smp_vdw_radius_to_array(original_vdw_radii, ag);
	_turn_off_vdw_on_selected_side_chains(ag, interface_residue_list);

	double eng;
	smp_energy_vdw_rep_line_ligand(ag, &eng, ags->nblst, ligand_atom_first, ligand_atom_last, en_weight_atr, en_weight_rep);

	unsigned int step_count = 0;
	if (eng > energy_threshold_pull) { // Pull them apart
		while (eng > energy_threshold_pull) {
			smp_atom_list_translate(ag, t, lig_atom_list);
			check_clusterupdate(ag, ags);
			smp_energy_vdw_rep_line_ligand(ag, &eng, ags->nblst, ligand_atom_first, ligand_atom_last, en_weight_atr, en_weight_rep);
			step_count++;
		}
	} else if (eng == 0) { // Push if backbones don't see each other
		MOL_VEC_MULT_SCALAR(*t, *t, -1); // Change the direction of translation vector
		while (eng < energy_threshold_push) {
			smp_atom_list_translate(ag, t, lig_atom_list);
			check_clusterupdate(ag, ags);
			smp_energy_vdw_rep_line_ligand(ag, &eng, ags->nblst, ligand_atom_first, ligand_atom_last, en_weight_atr, en_weight_rep);
			step_count++;
		}
	}

	bool nblist_updated = check_clusterupdate(ag, ags);
	if (nblist_updated && acs != NULL) {
		ace_updatenblst(ags, acs);
	}

	// Calculate the full translational vector
	MOL_VEC_MULT_SCALAR(*t, *t, step_count);

	smp_vdw_radius_from_array(ag, original_vdw_radii);
	free(original_vdw_radii);
	return step_count;
}


void smp_movers_randomly_rotate_translate_selected(
		struct mol_atom_group *ag,
		struct smp_prng *prng,
		const struct mol_index_list atom_list,
		const double delta_translation,
		const double delta_rotation)
{
	// Calculate rotation center
	struct mol_vector3 ligand_center = smp_atom_list_centroid(ag, atom_list);

	// Convert delta_rotation to units of pi, and clamp at 180 degrees
	double delta_rotation_pi = MIN(delta_rotation / M_PI, 1.0);

	// Generate random small rotation quaternion
	struct mol_quaternion random_q;
	smp_prng_quaternion_small(prng, &random_q, delta_rotation_pi);

	// Generate random translation vector uniformly distributed in [-delta/2;delta/2]^3
	struct mol_vector3 random_vector = {
		.X = smp_prng_uniform(prng), .Y = smp_prng_uniform(prng), .Z = smp_prng_uniform(prng)
	};
	struct mol_vector3 t;
	MOL_VEC_MULT_SCALAR(t, random_vector, delta_translation);
	MOL_VEC_SUB_SCALAR(t, t, 0.5*delta_translation);

	smp_atom_list_rotate(ag, &random_q, &ligand_center, atom_list);
	smp_atom_list_translate(ag, &t, atom_list);
}


void smp_movers_randomly_rotate_dihedrals(
		struct mol_atom_group *ag,
		struct smp_rigforest *rigforest,
		struct smp_prng *prng,
		const double delta)
{
	// Generate random perturbation for each edge of the tree
	for (size_t i = 0; i < rigforest->nfree_trees; i++) {
		for (size_t j = 0; j < rigforest->free_trees[i]->nnodes-1; j++) {
			const double delta_theta = delta * smp_prng_uniform(prng) - 0.5 * delta;
			rigforest->free_trees[i]->edge_thetas[j] = delta_theta;
		}
	}
	for (size_t i = 0; i < rigforest->nanch_trees; i++) {
		for (size_t j = 0; j < rigforest->anch_trees[i]->nnodes-1; j++) {
			const double delta_theta = delta * smp_prng_uniform(prng) - 0.5 * delta;
			rigforest->anch_trees[i]->edge_thetas[j] = delta_theta;
		}
	}
	smp_rigforest_move(ag, rigforest);
}
